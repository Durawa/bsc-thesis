from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from . import models
from django.contrib.auth.models import User
from datetime import datetime
from .utilities import get_id, get_ids, generate_password
from django.contrib.auth.hashers import make_password
from .decorators import forbid_permission


def get_admin():
    return get_object_or_404(User, pk=1)

"""
def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authentication completed successfully.')
                else:
                    return HttpResponse('This account is locked.')
            else:
                return HttpResponse('Inaccurate authentication data.')
    else:
        form = LoginForm()
        return render(request, 'recruitment_application/login.html', {'form': form})
"""


@login_required
def dashboard(request):
    return render(request,
                  'recruitment_application/dashboard.html',
                  {'section': 'dashboard'})


def create_user(request, json: dict):
    required_keys = ['username',
                     'first_name', 'last_name', 'email']
    for key in required_keys:
        if key not in json:
            return Response({"error": f"No {key} in json."},
                            status=status.HTTP_400_BAD_REQUEST)
    user = User()
    """
    user.username = request.data['username']
    user.password = request.data['password']
    . . .
    """
    for key in required_keys:
        if key == 'password':
            continue
        else:
            setattr(user, key, json[key])
    password = generate_password(3, 3, 3, 1)
    user.password = make_password(password)
    user.save()
    models.send_mail_to_user(request, user, password)
    return user


class UserList(APIView):
    def get(self, request):
        if hasattr(request.user, 'hremployee'):
            return Response({'id': request.user.hremployee.id, 'type': 'hremployee'})
        elif hasattr(request.user, 'recruiter'):
            return Response({'id': request.user.recruiter.id, 'type': 'recruiter'})
        elif hasattr(request.user, 'projectmanager'):
            return Response({'id': request.user.projectmanager.id, 'type': 'projectmanager'})
        elif hasattr(request.user, 'candidate'):
            return Response({'id': request.user.candidate.id, 'type': 'candidate'})
        raise RuntimeError('User with no type')


class AddressList(APIView):
    def get(self, request, id=None, format=None):
        if id is None:
            all_addresses = models.Address.objects.all()
            dictionary = {'addresses': [address.dict()
                                        for address in all_addresses]}
            return Response(dictionary)
        else:
            address = get_object_or_404(models.Address, pk=id)
            return Response(address.dict())

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['street', 'house_nr',
                         'flat_nr', 'postal_code', 'city', 'country']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)
        address = models.Address()
        return self.save_address(request, address)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['street', 'house_nr',
                         'flat_nr', 'postal_code', 'city', 'country', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)
        address = get_object_or_404(models.Address, pk=request.data['id'])
        return self.save_address(request, address)

    def save_address(self, request, address):
        address.street = request.data['street']
        address.house_nr = request.data['house_nr']
        address.flat_nr = request.data['flat_nr']
        address.postal_code = request.data['postal_code']
        address.city = request.data['city']
        address.country = request.data['country']
        address.created_by = request.user

        try:
            address.save()
        except Exception as e:
            return Response({"error": str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(address.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        address = get_object_or_404(models.Address, pk=request.data['id'])
        address.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class HREmployeeList(APIView):
    def get(self, request, id=None, format=None):
        if id is None:
            all_hremployees = models.HREmployee.objects.all()
            dictionary = {'hremployees': [
                hremployee.dict() for hremployee in all_hremployees]}
            return Response(dictionary)
        else:
            hremployee = get_object_or_404(models.HREmployee, pk=id)
            return Response(hremployee.dict())

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['birth_date', 'birth_place', 'telephone']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        hremployee = models.HREmployee()
        hremployee.user = create_user(request, request.data)
        return self.save_hremployee(request, hremployee, required_keys)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['birth_date', 'birth_place', 'telephone', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        hremployee = get_object_or_404(models.HREmployee, pk=request.data['id'])
        return self.save_hremployee(request, hremployee, required_keys)

    def save_hremployee(self, request, hremployee, required_keys):
        try:
            hremployee.address = get_object_or_404(
                models.Address, pk=get_id(request.data['address']))
        except Exception as e:
            hremployee.user.delete()
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        hremployee.created_by = get_admin()  # request.user
        for key in required_keys:
            setattr(hremployee, key, request.data[key])

        try:
            hremployee.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(hremployee.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        hremployee = get_object_or_404(models.HREmployee, pk=request.data['id'])
        hremployee.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class RecruiterList(APIView):
    def get(self, request, **kwargs):
        if 'id' in kwargs:
            recruiter = get_object_or_404(models.Recruiter, pk=kwargs['id'])
            return Response(recruiter.dict())
        elif 'recruitment_process_id' in kwargs:
            recruitment_process = get_object_or_404(models.RecruitmentProcess, pk=kwargs['recruitment_process_id'])
            recruiters = recruitment_process.recruiters.all()
            dictionary = {'recruiters': [recruiter.dict()
                                         for recruiter in recruiters]}
            return Response(dictionary)
        else:
            all_recruiters = models.Recruiter.objects.all()
            dictionary = {'recruiters': [recruiter.dict()
                                         for recruiter in all_recruiters]}
            return Response(dictionary)

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['birth_date', 'birth_place', 'telephone']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)
        recruiter = models.Recruiter()
        recruiter.user = create_user(request, request.data)
        return self.save_recruiter(request, recruiter, required_keys)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['birth_date', 'birth_place', 'telephone', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)
        recruiter = get_object_or_404(models.Recruiter, pk=request.data['id'])
        return self.save_recruiter(request, recruiter, required_keys)

    def save_recruiter(self, request, recruiter, required_keys):
        try:
            recruiter.address = get_object_or_404(
                models.Address, pk=get_id(request.data['address']))
        except Exception as e:
            recruiter.user.delete()
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        recruiter.created_by = get_admin()
        for key in required_keys:
            setattr(recruiter, key, request.data[key])

        try:
            recruiter.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(recruiter.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        recruiter = get_object_or_404(models.Recruiter, pk=request.data['id'])
        recruiter.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProjectManagerList(APIView):
    def get(self, request, id=None, format=None):
        if id is None:
            all_project_managers = models.ProjectManager.objects.all()
            dictionary = {'project_managers': [
                manager.dict() for manager in all_project_managers]}
            return Response(dictionary)
        else:
            manager = get_object_or_404(models.ProjectManager, pk=id)
            return Response(manager.dict())

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['birth_date', 'birth_place', 'telephone']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        manager = models.ProjectManager()
        manager.user = create_user(request, request.data)
        return self.save_manager(request, manager, required_keys)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['birth_date', 'birth_place', 'telephone', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        manager = get_object_or_404(models.ProjectManager, pk=request.data['id'])
        return self.save_manager(request, manager, required_keys)

    def save_manager(self, request, manager, required_keys):
        try:
            manager.address = get_object_or_404(
                models.Address, pk=get_id(request.data['address']))
        except Exception as e:
            manager.user.delete()
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        manager.created_by = get_admin()
        for key in required_keys:
            setattr(manager, key, request.data[key])

        try:
            manager.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(manager.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        manager = get_object_or_404(models.ProjectManager, pk=request.data['id'])
        manager.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CandidateList(APIView):
    def get(self, request, **kwargs):
        if 'id' in kwargs:
            candidate = get_object_or_404(models.Candidate, pk=kwargs['id'])
            return Response(candidate.dict())
        elif 'recruitment_process_id' in kwargs:
            recruitment_process = get_object_or_404(models.RecruitmentProcess, pk=kwargs['recruitment_process_id'])
            candidates = recruitment_process.candidates.all()
            dictionary = {'candidates': [candidate.dict() for candidate in candidates]}
            return Response(dictionary)
        else:
            all_candidates = models.Candidate.objects.all()
            dictionary = {'candidates': [candidate.dict()
                                         for candidate in all_candidates]}
            return Response(dictionary)

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['birth_date', 'birth_place', 'telephone']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        candidate = models.Candidate()
        candidate.user = create_user(request, request.data)
        return self.save_candidate(request, candidate, required_keys)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['birth_date', 'birth_place', 'telephone', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        candidate = get_object_or_404(models.Candidate, pk=request.data['id'])
        return self.save_candidate(request, candidate, required_keys)

    def save_candidate(self, request, candidate, required_keys):
        try:
            candidate.address = get_object_or_404(
                models.Address, pk=get_id(request.data['address']))
        except Exception as e:
            candidate.user.delete()
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        candidate.created_by = request.user

        birth_date_datetime: datetime = datetime.strptime(request.data['birth_date'], '%Y-%m-%d')
        candidate.birth_date = birth_date_datetime.date()
        candidate.birth_place = request.data['birth_place']
        candidate.telephone = request.data['telephone']

        try:
            candidate.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(candidate.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        candidate = get_object_or_404(models.Candidate, pk=request.data['id'])
        candidate.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class JobExperienceStageList(APIView):
    def get(self, request, id=None, format=None):
        if id is None:
            all_job_experience_stages = models.JobExperienceStage.objects.all()
            dictionary = {'job_experience_stages': [stage.dict() for stage in all_job_experience_stages]}
            return Response(dictionary)
        else:
            stage = get_object_or_404(models.JobExperienceStage, pk=id)
            return Response(stage.dict())

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['name', 'min_years', 'max_years']
        for key in required_keys:
            if key not in request.data:
                return Response({"error:" f"No {key} in json."}, status.HTTP_400_BAD_REQUEST)

        stage = models.JobExperienceStage()
        return self.save_stage(request, stage, required_keys)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['name', 'min_years', 'max_years', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error:" f"No {key} in json."}, status.HTTP_400_BAD_REQUEST)

        stage = get_object_or_404(models.JobExperienceStage, pk=request.data['id'])
        return self.save_stage(request, stage, required_keys)

    def save_stage(self, request, stage, required_keys):
        stage.created_by = get_admin()
        for key in required_keys:
            setattr(stage, key, request.data[key])
        try:
            stage.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(stage.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        stage = get_object_or_404(models.JobExperienceStage, pk=request.data['id'])
        stage.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProgrammingLanguageList(APIView):
    def get(self, request, id=None, format=None):
        if id is None:
            all_programming_languages = models.ProgrammingLanguage.objects.all()
            dictionary = {'programming_languages': [language.dict() for language in all_programming_languages]}
            return Response(dictionary)
        else:
            language = get_object_or_404(models.ProgrammingLanguage, pk=id)
            return Response(language.dict())

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['name']
        for key in required_keys:
            if key not in request.data:
                return Response({"error:" f"No {key} in json."}, status.HTTP_400_BAD_REQUEST)

        language = models.ProgrammingLanguage()
        return self.save_language(language, request)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['name', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error:" f"No {key} in json."}, status.HTTP_400_BAD_REQUEST)

        language = get_object_or_404(models.ProgrammingLanguage, pk=request.data['id'])
        return self.save_language(language, request)

    def save_language(self, language, request):
        language.name = request.data['name']
        language.created_by = get_admin()
        try:
            language.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(language.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        language = get_object_or_404(models.ProgrammingLanguage, pk=request.data['id'])
        language.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class JobPositionTypeList(APIView):
    def get(self, request, id=None, format=None):
        if id is None:
            all_job_position_types = models.JobPositionType.objects.all()
            dictionary = {'job_position_types': [position_type.dict() for position_type in all_job_position_types]}
            return Response(dictionary)
        else:
            position_type = get_object_or_404(models.JobPositionType, pk=id)
            return Response(position_type.dict())

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['name']
        for key in required_keys:
            if key not in request.data:
                return Response({"error:" f"No {key} in json."}, status.HTTP_400_BAD_REQUEST)

        position_type = models.JobPositionType()
        return self.save_position_type(request, position_type)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['name', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error:" f"No {key} in json."}, status.HTTP_400_BAD_REQUEST)

        position_type = get_object_or_404(models.JobPositionType, pk=request.data['id'])
        return self.save_position_type(request, position_type)

    def save_position_type(self, request, position_type):
        position_type.name = request.data['name']
        position_type.created_by = get_admin()
        try:
            position_type.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(position_type.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        position_type = get_object_or_404(models.JobPositionType, pk=request.data['id'])
        position_type.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class JobPositionList(APIView):
    def get(self, request, id=None, format=None):
        if id is None:
            all_job_positions = models.JobPosition.objects.all()
            dictionary = {'job_positions': [position.dict() for position in all_job_positions]}
            return Response(dictionary)
        else:
            position = get_object_or_404(models.JobPosition, pk=id)
            return Response(position.dict())

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['experience_stage', 'language', 'position_type']
        for key in required_keys:
            if key not in request.data:
                return Response({"error:" f"No {key} in json."}, status.HTTP_400_BAD_REQUEST)

        position = models.JobPosition()
        return self.save_position(request, position)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['experience_stage', 'language', 'position_type', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error:" f"No {key} in json."}, status.HTTP_400_BAD_REQUEST)

        position = get_object_or_404(models.JobPosition, pk=request.data['id'])
        return self.save_position(request, position)

    def save_position(self, request, position):
        position.experience_stage = get_object_or_404(models.JobExperienceStage,
                                                      pk=get_id(request.data['experience_stage']))
        position.language = get_object_or_404(models.ProgrammingLanguage, pk=get_id(request.data['language']))
        position.position_type = get_object_or_404(models.JobPositionType, pk=get_id(request.data['position_type']))
        position.created_by = get_admin()

        try:
            position.save()
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(position.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        position = get_object_or_404(models.JobPosition, pk=request.data['id'])
        position.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class RecruitmentProcessList(APIView):
    def get(self, request, **kwargs):
        if 'id' in kwargs:
            process = get_object_or_404(models.RecruitmentProcess, pk=kwargs['id'])
            return Response(process.dict())
        elif 'hr_employee_id' in kwargs:
            hremployee = get_object_or_404(models.HREmployee, pk=kwargs['hr_employee_id'])
            recruitment_processes = hremployee.recruitment_processes.filter(is_open=True)
            dictionary = {'recruitment_processes': [process.dict() for process in recruitment_processes]}
            return Response(dictionary)
        elif 'recruiter_id' in kwargs:
            recruiter = get_object_or_404(models.Recruiter, pk=kwargs['recruiter_id'])
            recruitment_processes = recruiter.recruitment_processes.filter(is_open=True)
            dictionary = {'recruitment_processes': [process.dict() for process in recruitment_processes]}
            return Response(dictionary)
        elif 'candidate_id' in kwargs:
            candidate = get_object_or_404(models.Candidate, pk=kwargs['candidate_id'])
            recruitment_processes = candidate.recruitment_processes.filter(is_open=True)
            dictionary = {'recruitment_processes': [process.dict() for process in recruitment_processes]}
            return Response(dictionary)
        else:
            all_recruitment_processes = models.RecruitmentProcess.objects.all()
            dictionary = {'recruitment_processes': [process.dict() for process in all_recruitment_processes]}
            return Response(dictionary)

    @forbid_permission("Candidate", "Recruiter")
    def post(self, request, format=None):
        process = models.RecruitmentProcess()
        return self.save_process(request, process)

    @forbid_permission("Candidate", "Recruiter")
    def put(self, request):
        required_keys = ['is_at_test_stage', 'is_open', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        process = get_object_or_404(models.RecruitmentProcess, pk=request.data['id'])
        return self.save_process(request, process)

    def save_process(self, request, process):
        process.project_manager = get_object_or_404(models.ProjectManager, pk=1)
        process.job_position = get_object_or_404(models.JobPosition, pk=get_id(request.data['job_position']))
        process.hr_employee = request.user.hremployee

        if 'recruiters' in request.data:
            recruiters_pkeys = get_ids(request.data['recruiters'])
            recruiters_list = []
            for recruiter_pk in recruiters_pkeys:
                recruiters_list.append(get_object_or_404(models.Recruiter, pk=recruiter_pk))

        if 'candidates' in request.data:
            candidates_pkeys = get_ids(request.data['candidates'])
            candidates_list = []
            for candidate_pk in candidates_pkeys:
                candidates_list.append(get_object_or_404(models.Candidate, pk=candidate_pk))

        if 'is_at_test_stage' in request.data:
            process.is_at_test_stage = request.data['is_at_test_stage']
        else:
            process.is_at_test_stage = True

        if 'is_open' in request.data:
            process.is_open = request.data['is_open']
        else:
            process.is_open = True

        accepted_formats = ['%Y-%m-%d', '%d.%m.%Y']

        for accepted_format in accepted_formats:
            try:
                date_obj = datetime.strptime(request.data['deadline'], accepted_format)
                break
            except ValueError:
                pass

        process.deadline = date_obj

        process.created_by = get_admin()
        try:
            process.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        if 'recruiters' in request.data:
            process.recruiters.clear()
            process.recruiters.add(*recruiters_list)

        # delete all bindings of deleted candidates
        if 'candidates' in request.data:
            all_candidates = process.candidates.all()
            candidates_to_remove = [candidate for candidate in all_candidates if candidate not in candidates_list]
            for candidate in candidates_to_remove:
                models.RecruiterCandidateBinding.objects.filter(candidate=candidate, process=process).delete()

        if 'candidates' in request.data:
            # sanity bindings cleaning
            process_bindings = models.RecruiterCandidateBinding.objects.filter(process=process)
            bindings_to_delete = [b for b in process_bindings if b.candidate not in candidates_list]
            for binding in bindings_to_delete:
                binding.delete()

        if 'candidates' in request.data:
            process.candidates.clear()
            process.candidates.add(*candidates_list)

        return Response(process.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        process = get_object_or_404(models.RecruitmentProcess, pk=request.data['id'])
        process.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class RecruiterCandidateBindingList(APIView):
    def get(self, request, **kwargs):
        if 'id' in kwargs:
            binding = get_object_or_404(models.RecruiterCandidateBinding, pk=kwargs['id'])
            return Response(binding.dict())
        elif 'recruiter_id' in kwargs and 'process_id' in kwargs:
            return self.get_for_recruiter_and_process(request, **kwargs)
        elif 'candidate_id' in kwargs and 'process_id' in kwargs:
            candidate = get_object_or_404(models.Candidate, pk=kwargs['candidate_id'])
            process = get_object_or_404(models.RecruitmentProcess, pk=kwargs['process_id'])
            bindings = models.RecruiterCandidateBinding.objects.filter(process=process, candidate=candidate)
            dictionary = {'recruiter_candidate_bindings': [b.dict() for b in bindings]}
            return Response(dictionary)
        else:
            return self.get_all(request, **kwargs)

    @staticmethod
    @forbid_permission("Candidate")
    def get_for_recruiter_and_process(request, **kwargs):
        recruiter = get_object_or_404(models.Recruiter, pk=kwargs['recruiter_id'])
        process = get_object_or_404(models.RecruitmentProcess, pk=kwargs['process_id'])
        bindings = models.RecruiterCandidateBinding.objects.filter(process=process, recruiter=recruiter)
        dictionary = {'recruiter_candidate_bindings': [b.dict() for b in bindings]}
        return Response(dictionary)

    @staticmethod
    @forbid_permission("Candidate")
    def get_all(request, **kwargs):
        all_recruiter_candidate_bindings = models.RecruiterCandidateBinding.objects.all()
        dictionary = {
            'recruiter_candidate_bindings': [binding.dict() for binding in all_recruiter_candidate_bindings]}
        return Response(dictionary)

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['process', 'recruiter', 'candidate']
        for key in required_keys:
            if key not in request.data:
                return Response({"error:" f"No {key} in json."}, status.HTTP_400_BAD_REQUEST)

        binding = models.RecruiterCandidateBinding()
        return self.save_binding(request, binding)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['process', 'recruiter', 'candidate', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error:" f"No {key} in json."}, status.HTTP_400_BAD_REQUEST)

        binding = get_object_or_404(models.RecruiterCandidateBinding, pk=request.data['id'])
        return self.save_binding(request, binding)

    def save_binding(self, request, binding):
        binding.process = get_object_or_404(models.RecruitmentProcess, pk=get_id(request.data['process']))
        binding.recruiter = get_object_or_404(models.Recruiter, pk=get_id(request.data['recruiter']))
        binding.candidate = get_object_or_404(models.Candidate, pk=get_id(request.data['candidate']))
        binding.created_by = get_admin()

        try:
            binding.save()
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(request.data)

    @forbid_permission("Candidate")
    def delete(self, request):
        if 'id' in request.data:
            binding = get_object_or_404(models.RecruiterCandidateBinding, pk=request.data['id'])
            binding.delete()
            return Response(status=status.HTTP_200_OK)
        elif 'candidate' in request.data and 'recruiter' in request.data and 'process' in request.data:
            candidate = get_object_or_404(models.Candidate, pk=request.data['candidate'])
            recruiter = get_object_or_404(models.Recruiter, pk=request.data['recruiter'])
            process = get_object_or_404(models.RecruitmentProcess, pk=request.data['process'])
            models.RecruiterCandidateBinding.objects.filter(process=process,
                                                            candidate=candidate,
                                                            recruiter=recruiter).delete()
            return Response(status=status.HTTP_200_OK)


class ClosedTestQuestionList(APIView):
    def get(self, request, **kwargs):
        if 'id' in kwargs:
            question = get_object_or_404(models.ClosedTestQuestion, pk=kwargs['id'])
            return Response(question.dict())
        elif 'experience_id' in kwargs and 'language_id' in kwargs and 'position_type_id' in kwargs:
            experience_stage = models.JobExperienceStage.objects.get(pk=kwargs['experience_id'])
            language = models.ProgrammingLanguage.objects.get(pk=kwargs['language_id'])
            position_type = models.JobPositionType.objects.get(pk=kwargs['position_type_id'])
            closed_test_questions = models.ClosedTestQuestion.objects.filter(experience_stage=experience_stage,
                                                                             language=language,
                                                                             position_type=position_type)
            dictionary = {'closed_test_questions': [question.dict() for question in closed_test_questions]}
            return Response(dictionary)
        else:
            all_closed_test_questions = models.ClosedTestQuestion.objects.all()
            dictionary = {'closed_test_questions': [question.dict() for question in all_closed_test_questions]}
            return Response(dictionary)

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['question_text', 'answer_a', 'answer_b', 'answer_c', 'answer_d', 'correct_answer']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        question = models.ClosedTestQuestion()
        return self.save_question(request, question, required_keys)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['question_text', 'answer_a', 'answer_b', 'answer_c', 'answer_d', 'correct_answer', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        question = get_object_or_404(models.ClosedTestQuestion, pk=request.data['id'])
        return self.update_question(request, question, required_keys)

    def save_question(self, request, question, required_keys):
        try:
            question = self.save_specifics(request, question, required_keys)
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        self.add_points_for_question(request, question)
        return Response(question.dict())

    def update_question(self, request, question, required_keys):
        try:
            question = self.save_specifics(request, question, required_keys)
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(question.dict())

    @staticmethod
    def save_specifics(request, question, required_keys):
        question.experience_stage = get_object_or_404(models.JobExperienceStage,
                                                      pk=get_id(request.data['experience_stage']))
        question.language = get_object_or_404(models.ProgrammingLanguage, pk=get_id(request.data['language']))
        question.position_type = get_object_or_404(models.JobPositionType, pk=get_id(request.data['position_type']))
        question.created_by = get_admin()

        for key in required_keys:
            setattr(question, key, request.data[key])

        question.save()
        return question

    @staticmethod
    def add_points_for_question(request, question):
        points = models.ClosedQuestionPoints()
        points.question = question
        points.experience_stage = question.experience_stage
        points.points = 2
        points.created_by = get_admin()
        points.save()

        experience_stages = models.JobExperienceStage.objects.all().order_by('max_years')
        if question.experience_stage != experience_stages[0]:
            previous_experience_stage = None
            for index, experience_stage in enumerate(experience_stages):
                if question.experience_stage == experience_stage:
                    previous_experience_stage = experience_stages[index-1]
            if previous_experience_stage is None:
                raise RuntimeError('Could not find previous experience stage')

            points2 = models.ClosedQuestionPoints()
            points2.question = question
            points2.experience_stage = previous_experience_stage
            points2.points = 4
            points2.created_by = get_admin()
            points2.save()

    @forbid_permission("Candidate")
    def delete(self, request):
        question = get_object_or_404(models.ClosedTestQuestion, pk=request.data['id'])
        question.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ClosedQuestionPointsList(APIView):
    def get(self, request, id=None, format=None):
        if id is None:
            all_closed_question_points = models.ClosedQuestionPoints.objects.all()
            dictionary = {
                'closed_questions_points': [question_points.dict() for question_points in all_closed_question_points]}
            return Response(dictionary)
        else:
            question_points = get_object_or_404(models.ClosedQuestionPoints, pk=id)
            return Response(question_points.dict())

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['question', 'experience_stage', 'points']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        question_points = models.ClosedQuestionPoints()
        return self.save_question_points(request, question_points)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['question', 'experience_stage', 'points', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        question_points = get_object_or_404(models.ClosedQuestionPoints, pk=request.data['id'])
        return self.save_question_points(request, question_points)

    def save_question_points(self, request, question_points):
        question_points.question = get_object_or_404(models.ClosedTestQuestion, pk=get_id(request.data['question']))
        question_points.experience_stage = get_object_or_404(models.JobExperienceStage,
                                                             pk=get_id(request.data['experience_stage']))
        question_points.points = request.data['points']
        question_points.created_by = get_admin()

        try:
            question_points.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(question_points.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        question_points = get_object_or_404(models.ClosedQuestionPoints, pk=request.data['id'])
        question_points.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class OpenTestQuestionList(APIView):
    def get(self, request, **kwargs):
        if 'id' in kwargs:
            question = get_object_or_404(models.OpenTestQuestion, pk=kwargs['id'])
            return Response(question.dict())
        elif 'experience_id' in kwargs and 'language_id' in kwargs and 'position_type_id' in kwargs:
            experience_stage = models.JobExperienceStage.objects.get(pk=kwargs['experience_id'])
            language = models.ProgrammingLanguage.objects.get(pk=kwargs['language_id'])
            position_type = models.JobPositionType.objects.get(pk=kwargs['position_type_id'])
            closed_test_questions = models.OpenTestQuestion.objects.filter(experience_stage=experience_stage,
                                                                             language=language,
                                                                             position_type=position_type)
            dictionary = {'open_test_questions': [question.dict() for question in closed_test_questions]}
            return Response(dictionary)
        else:
            all_open_test_questions = models.OpenTestQuestion.objects.all()
            dictionary = {'open_test_questions': [question.dict() for question in all_open_test_questions]}
            return Response(dictionary)

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['question_text', 'experience_stage', 'language', 'position_type']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        question = models.OpenTestQuestion()
        return self.save_question(request, question)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['question_text', 'experience_stage', 'language', 'position_type', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        question = get_object_or_404(models.OpenTestQuestion, pk=request.data['id'])
        return self.update_question(request, question)

    def save_question(self, request, question):
        try:
            question = self.save_specifics(request, question)
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        self.add_points_for_question(request, question)
        return Response(question.dict())

    def update_question(self, request, question):
        try:
            question = self.save_specifics(request, question)
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(question.dict())

    @staticmethod
    def save_specifics(request, question):
        question.question_text = request.data['question_text']
        question.experience_stage = get_object_or_404(models.JobExperienceStage,
                                                      pk=get_id(request.data['experience_stage']))
        question.language = get_object_or_404(models.ProgrammingLanguage, pk=get_id(request.data['language']))
        question.position_type = get_object_or_404(models.JobPositionType, pk=get_id(request.data['position_type']))
        question.created_by = get_admin()
        question.save()
        return question

    @staticmethod
    def add_points_for_question(request, question):
        points = models.OpenQuestionPoints()
        points.question = question
        points.experience_stage = question.experience_stage
        points.points = 4
        points.created_by = get_admin()
        points.save()

        experience_stages = models.JobExperienceStage.objects.all().order_by('max_years')
        if question.experience_stage != experience_stages[0]:
            previous_experience_stage = None
            for index, experience_stage in enumerate(experience_stages):
                if question.experience_stage == experience_stage:
                    previous_experience_stage = experience_stages[index - 1]
            if previous_experience_stage is None:
                raise RuntimeError('Could not find previous experience stage')

            points2 = models.OpenQuestionPoints()
            points2.question = question
            points2.experience_stage = previous_experience_stage
            points2.points = 8
            points2.created_by = get_admin()
            points2.save()

    @forbid_permission("Candidate")
    def delete(self, request):
        question = get_object_or_404(models.OpenTestQuestion, pk=request.data['id'])
        question.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class OpenQuestionPointsList(APIView):
    def get(self, request, **kwargs):
        if 'id' in kwargs:
            question_points = get_object_or_404(models.OpenQuestionPoints, pk=kwargs['id'])
            return Response(question_points.dict())
        elif 'question_id' in kwargs and 'experience_id' in kwargs:
            question = get_object_or_404(models.OpenTestQuestion, pk=kwargs['question_id'])
            experience = get_object_or_404(models.JobExperienceStage, pk=kwargs['experience_id'])
            points = models.OpenQuestionPoints.objects.get(question=question, experience_stage=experience)
            return Response(points.dict())
        else:
            all_open_question_points = models.OpenQuestionPoints.objects.all()
            dictionary = {
                'open_questions_points': [question_points.dict() for question_points in all_open_question_points]}
            return Response(dictionary)

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['question', 'experience_stage', 'points']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        question_points = models.OpenQuestionPoints()
        return self.save_questions_points(request, question_points)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['question', 'experience_stage', 'points', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        question_points = get_object_or_404(models.OpenQuestionPoints, pk=request.data['id'])
        return self.save_questions_points(request, question_points)

    def save_questions_points(self, request, question_points):
        question_points.question = get_object_or_404(models.ClosedTestQuestion, pk=get_id(request.data['question']))
        question_points.experience_stage = get_object_or_404(models.JobExperienceStage,
                                                             pk=get_id(request.data['experience_stage']))
        question_points.points = request.data['points']
        question_points.created_by = get_admin()

        try:
            question_points.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(question_points.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        question_points = get_object_or_404(models.OpenQuestionPoints, pk=request.data['id'])
        question_points.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TestList(APIView):
    def get(self, request, **kwargs):
        if 'id' in kwargs:
            test = get_object_or_404(models.Test, pk=kwargs['id'])
            return Response({'test': test.dict()})
        elif 'process_id' in kwargs and 'closed_count' in kwargs and 'open_count' in kwargs and 'recruiter_id' in kwargs:
            process = get_object_or_404(models.RecruitmentProcess, pk=kwargs['process_id'])
            recruiter = get_object_or_404(models.Recruiter, pk=kwargs['recruiter_id'])
            test = models.Test.generate(process, kwargs['closed_count'], kwargs['open_count'], recruiter)
            return Response({'test': test.dict()})
        elif 'process_id' in kwargs and 'recruiter_id' in kwargs:
            process = get_object_or_404(models.RecruitmentProcess, pk=kwargs['process_id'])
            recruiter = get_object_or_404(models.Recruiter, pk=kwargs['recruiter_id'])
            test = models.Test.get_test(process=process, recruiter=recruiter)
            if test is None:
                return Response({})
            return Response({'test': test.dict()})
        elif 'process_id' in kwargs and 'candidate_id' in kwargs:
            process = get_object_or_404(models.RecruitmentProcess, pk=kwargs['process_id'])
            candidate = get_object_or_404(models.Candidate, pk=kwargs['candidate_id'])
            test = models.Test.get_test(process=process, candidate=candidate)
            if test is None:
                return Response({})
            return Response({'test': test.dict()})
        else:
            return self.get_all(request, **kwargs)

    @staticmethod
    @forbid_permission('Candidate')
    def get_all(request, **kwargs):
        all_tests = models.Test.objects.all()
        dictionary = {'tests': [test.dict() for test in all_tests]}
        return Response(dictionary)

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        test = models.Test()
        return self.save_test(request, test)

    @forbid_permission("Candidate")
    def put(self, request):
        test = get_object_or_404(models.Test, pk=request.data['id'])
        return self.save_test(request, test)

    def save_test(self, request, test):
        test.process = get_object_or_404(models.RecruitmentProcess, pk=get_id(request.data['process']))
        test.recruiter = get_object_or_404(models.Recruiter, pk=get_id(request.data['recruiter']))
        if 'candidate' in request.data:
            test.candidate = get_object_or_404(models.Candidate, pk=get_id(request.data['candidate']))

        date_obj = datetime.strptime(request.data['date'], '%Y-%m-%d').date()
        test.date = date_obj

        closed_test_questions_pkeys = get_ids(request.data['closed_test_questions'])
        closed_test_questions_list = []
        for closed_test_question_pk in closed_test_questions_pkeys:
            closed_test_questions_list.append(get_object_or_404(models.ClosedTestQuestion, pk=closed_test_question_pk))

        open_test_questions_pkeys = get_ids(request.data['open_test_questions'])
        open_test_questions_list = []
        for open_test_question_pk in open_test_questions_pkeys:
            open_test_questions_list.append(get_object_or_404(models.OpenTestQuestion, pk=open_test_question_pk))
        test.created_by = get_admin()
        try:
            test.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        test.closed_test_questions.clear()
        test.closed_test_questions.add(*closed_test_questions_list)
        test.open_test_questions.clear()
        test.open_test_questions.add(*open_test_questions_list)
        return Response(test.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        test = get_object_or_404(models.Test, pk=request.data['id'])
        test.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class WrittenTestList(APIView):
    def get(self, request, **kwargs):
        if 'id' in kwargs:
            written_test = get_object_or_404(models.WrittenTest, pk=kwargs['id'])
            return Response(written_test.dict())
        elif 'process_id' in kwargs and 'candidate_id' in kwargs:
            process = get_object_or_404(models.RecruitmentProcess, pk=kwargs['process_id'])
            candidate = get_object_or_404(models.Candidate, pk=kwargs['candidate_id'])
            try:
                test = models.WrittenTest.objects.get(process=process, candidate=candidate)
            except Exception:
                return Response({'test': None})
            return Response({'test': test.dict()})
        else:
            all_written_tests = models.WrittenTest.objects.all()
            dictionary = {'written_tests': [written_test.dict() for written_test in all_written_tests]}
            return Response(dictionary)

    def post(self, request, format=None):
        written_test = models.WrittenTest()
        return self.save_written_test(request, written_test)

    @forbid_permission('Candidate', 'HREmployee')
    def put(self, request):
        required_keys = ['id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        written_test = get_object_or_404(models.WrittenTest, pk=request.data['id'])
        return self.check_written_test(request, written_test)

    def save_written_test(self, request, written_test):
        written_test = self.get_basic_data_and_save(request, written_test, 'id')
        self.save_closed_questions(written_test, request.data['closed_test_questions'])
        self.save_open_questions(written_test, request.data['open_test_questions'])
        return Response(written_test.dict())

    def check_written_test(self, request, written_test):
        written_test = self.get_basic_data_and_save(request, written_test, 'test')
        self.update_open_questions(written_test, request.data['open_test_questions'])
        return Response(written_test.dict())

    @staticmethod
    def get_basic_data_and_save(request, written_test, test_key):
        written_test.process = get_object_or_404(models.RecruitmentProcess, pk=get_id(request.data['process']))
        written_test.test = get_object_or_404(models.Test, pk=get_id(request.data[test_key]))
        written_test.candidate = get_object_or_404(models.Candidate, pk=get_id(request.data['candidate']))
        written_test.created_by = get_admin()
        try:
            written_test.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return written_test

    @staticmethod
    def save_closed_questions(test, closed_questions):
        for closed_question in closed_questions:
            answered_close = models.AnsweredClosedQuestion()
            answered_close.process = test.process
            answered_close.candidate = test.candidate
            answered_close.question = get_object_or_404(models.ClosedTestQuestion, pk=closed_question['id'])
            answered_close.answer = closed_question['given_answer']
            answered_close.test = test
            answered_close.save()

    @staticmethod
    def save_open_questions(test, open_questions):
        for open_question in open_questions:
            answered_open = models.AnsweredOpenQuestion()
            answered_open.process = test.process
            answered_open.candidate = test.candidate
            answered_open.question = get_object_or_404(models.OpenTestQuestion, pk=open_question['id'])
            answered_open.answer = open_question['given_answer']
            answered_open.test = test
            answered_open.save()

    @staticmethod
    def update_open_questions(written_test, open_questions):
        for open_question in written_test.answered_open.all():
            data = None
            for index, open_question_data in enumerate(open_questions):
                if 'id' not in open_question_data:
                    raise RuntimeError('No id in question data')
                if open_question_data['id'] == open_question.id:
                    data = open_question_data
                    del open_questions[index]
                    break
            if data is None:
                raise RuntimeError('Could not find matching question to question data')
            open_question.checked = True
            if 'given_points' not in data:
                raise RuntimeError(f'No points assigned to question with id {str(open_question.id)}')
            if not isinstance(data['given_points'], int):
                raise RuntimeError(f'Given points is not a number')
            open_question.given_points = data['given_points']
            open_question.save()

    @forbid_permission("Candidate")
    def delete(self, request):
        written_test = get_object_or_404(models.WrittenTest, pk=request.data['id'])
        written_test.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class RecruitmentInterviewList(APIView):
    def get(self, request, **kwargs):
        if 'id' in kwargs:
            interview = get_object_or_404(models.RecruitmentInterview, pk=id)
            return Response(interview.dict())
        elif 'process_id' in kwargs and 'candidate_id' in kwargs:
            process = get_object_or_404(models.RecruitmentProcess, pk=kwargs['process_id'])
            candidate = get_object_or_404(models.Candidate, pk=kwargs['candidate_id'])
            try:
                interview = models.RecruitmentInterview.objects.get(process=process, candidate=candidate)
            except Exception:
                return Response({'interview': None})
            return Response({'interview': interview.dict()})
        else:
            all_recruitment_interviews = models.RecruitmentInterview.objects.all()
            dictionary = {'recruitment_interviews': [interview.dict() for interview in all_recruitment_interviews]}
            return Response(dictionary)

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        interview = models.RecruitmentInterview()
        return self.save_interview(request, interview)

    @forbid_permission("Candidate")
    def put(self, request):
        interview = get_object_or_404(models.RecruitmentInterview, pk=request.data['id'])
        return self.save_interview(request, interview)

    def save_interview(self, request, interview):
        interview.process = get_object_or_404(models.RecruitmentProcess, pk=get_id(request.data['process']))
        interview.candidate = get_object_or_404(models.Candidate, pk=get_id(request.data['candidate']))

        date_obj = timezone.make_aware(datetime.strptime(request.data['date'], '%Y-%m-%d %H:%M'))
        interview.date = date_obj

        interview.created_by = get_admin()

        try:
            interview.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(interview.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        interview = get_object_or_404(models.RecruitmentInterview, pk=request.data['id'])
        interview.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class InterviewNotesList(APIView):
    def get(self, request, id=None, format=None):
        if id is None:
            all_interview_notes = models.InterviewNotes.objects.all()
            dictionary = {'interview_notes': [interview_notes.dict() for interview_notes in all_interview_notes]}
            return Response(dictionary)
        else:
            interview_notes = get_object_or_404(models.InterviewNotes, pk=id)
            return Response(interview_notes.dict())

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['notes']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        interview_notes = models.InterviewNotes()
        return self.save_interview_notes(request, interview_notes)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['notes', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        interview_notes = get_object_or_404(models.InterviewNotes, pk=request.data['id'])
        return self.save_interview_notes(request, interview_notes)

    def save_interview_notes(self, request, interview_notes):
        interview_notes.interview = get_object_or_404(models.InterviewNotes, pk=get_id(request.data['interview_notes']))
        interview_notes.notes = request.data['notes']
        interview_notes.created_by = get_admin()

        try:
            interview_notes.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(interview_notes.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        interview_notes = get_object_or_404(models.InterviewNotes, pk=request.data['id'])
        interview_notes.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class JobOfferList(APIView):
    def get(self, request, id=None, format=None):
        if id is None:
            all_job_offers = models.JobOffer.objects.all()
            dictionary = {'job_offers': [offer.dict() for offer in all_job_offers]}
            return Response(dictionary)
        else:
            offer = get_object_or_404(models.JobOffer, pk=id)
            return Response(offer.dict())

    @forbid_permission("Candidate")
    def post(self, request, format=None):
        required_keys = ['salary', 'is_accepted']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        offer = models.JobOffer()
        return self.save_offer(request, offer, required_keys)

    @forbid_permission("Candidate")
    def put(self, request):
        required_keys = ['salary', 'is_accepted', 'id']
        for key in required_keys:
            if key not in request.data:
                return Response({"error": f"No {key} in json."},
                                status=status.HTTP_400_BAD_REQUEST)

        offer = get_object_or_404(models.JobOffer, pk=request.data['id'])
        return self.save_offer(request, offer, required_keys)

    def save_offer(self, request, offer, required_keys):
        offer.process = get_object_or_404(models.RecruitmentProcess, pk=get_id(request.data['process']))
        offer.candidate = get_object_or_404(models.Candidate, pk=get_id(request.date['candidate']))
        offer.created_by = get_admin()
        for key in required_keys:
            setattr(offer, key, request.data[key])

        try:
            offer.save()
        except Exception as e:
            return Response({'error': str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(offer.dict())

    @forbid_permission("Candidate")
    def delete(self, request):
        offer = get_object_or_404(models.JobOffer, pk=request.data['id'])
        offer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
