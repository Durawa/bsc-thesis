import random


def get_id(arg):
    if isinstance(arg, int):
        return arg
    elif isinstance(arg, dict):
        if "id" in arg:
            return arg["id"]
        raise RuntimeError("Id not found in dict")
    raise RuntimeError("Arg is not int or dict")


def get_ids(arg: list):
    if len(arg) == 0:
        return arg
    if isinstance(arg[0], dict):
        if "id" in arg[0]:
            return [el["id"] for el in arg]
        raise RuntimeError("No id in dict")
    elif isinstance(arg[0], int):
        return arg
    raise RuntimeError("Arg has wrong types")


def generate_password(capitals, lower, numbers, specials):
    random_capitals = []
    for i in range(capitals):
        random_capitals.append(chr(random.randint(65, 90)))
    random_lower = []
    for i in range(lower):
        random_lower.append(chr(random.randint(97, 122)))
    random_numbers = []
    for i in range(numbers):
        random_numbers.append(str(random.randint(0, 9)))
    SPECIAL_SIGNS = ('!', '@', '#', '?', '&', '%', '_')
    random_specials = []
    for i in range(specials):
        random_specials.append(SPECIAL_SIGNS[random.randrange(0, len(SPECIAL_SIGNS)-1)])

    random_signs = random_capitals + random_lower + random_numbers + random_specials
    random.shuffle(random_signs)
    return ''.join(random_signs)

