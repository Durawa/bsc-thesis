from django import template

register = template.Library()

@register.filter
def get_user_type(user):
    if hasattr(user, 'hremployee'):
        return 'hr_employee'
    elif hasattr(user, 'recruiter'):
        return 'recruiter'
    elif hasattr(user, 'projectmanager'):
        return 'project_manager'
    elif hasattr(user, 'candidate'):
        return 'candidate'
    else:
        raise RuntimeError("User with no type")