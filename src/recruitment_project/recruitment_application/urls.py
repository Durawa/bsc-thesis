from django.urls import path
from . import views
from django.contrib.auth import views as auth_views


app_name='recruitment_application'

urlpatterns = [
    #path('login/', views.user_login, name='login'),
    #path('login/', auth_views.LoginView.as_view(), name='login'),
    #path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('', views.dashboard, name='dashboard'),
    
    path('User/get/', views.UserList.as_view()),

    path('Address/get/', views.AddressList.as_view(), name='AddressGetAll'),
    path('Address/get/<int:id>/', views.AddressList.as_view(), name='AddressGetSpecific'),
    path('Address/post/', views.AddressList.as_view(), name='AddressPost'),
    path('Address/put/', views.AddressList.as_view(), name='AddressPut'),
    path('Address/delete/', views.AddressList.as_view(), name='AddressDelete'),

    path('HREmployee/get/', views.HREmployeeList.as_view()),
    path('HREmployee/get/<int:id>/', views.HREmployeeList.as_view()),
    path('HREmployee/post/', views.HREmployeeList.as_view()),
    path('HREmployee/put/', views.HREmployeeList.as_view()),
    path('HREmployee/delete/', views.HREmployeeList.as_view()),

    path('Recruiter/get/', views.RecruiterList.as_view()),
    path('Recruiter/get/<int:id>/', views.RecruiterList.as_view()),
    path('Recruiter/get/RecruitmentProcess/<int:recruitment_process_id>/', views.RecruiterList.as_view()),
    path('Recruiter/post/', views.RecruiterList.as_view()),
    path('Recruiter/put/', views.RecruiterList.as_view()),
    path('Recruiter/delete/', views.RecruiterList.as_view()),

    path('ProjectManager/get/', views.ProjectManagerList.as_view()),
    path('ProjectManager/get/<int:id>/', views.ProjectManagerList.as_view()),
    path('ProjectManager/post/', views.ProjectManagerList.as_view()),
    path('ProjectManager/put/', views.ProjectManagerList.as_view()),
    path('ProjectManager/delete/', views.ProjectManagerList.as_view()),

    path('Candidate/get/', views.CandidateList.as_view(), name='CandidateGetAll'),
    path('Candidate/get/<int:id>/', views.CandidateList.as_view(), name='CandidateGetSpecific'),
    path('Candidate/get/RecruitmentProcess/<int:recruitment_process_id>/', views.CandidateList.as_view()),
    path('Candidate/post/', views.CandidateList.as_view(), name='CandidatePost'),
    path('Candidate/put/', views.CandidateList.as_view(), name='CandidatePut'),
    path('Candidate/delete/', views.CandidateList.as_view(), name='CandidateDelete'),

    path('JobExperienceStage/get/', views.JobExperienceStageList.as_view()),
    path('JobExperienceStage/get/<int:id>/', views.JobExperienceStageList.as_view()),
    path('JobExperienceStage/post/', views.JobExperienceStageList.as_view()),
    path('JobExperienceStage/put/', views.JobExperienceStageList.as_view()),
    path('JobExperienceStage/delete/', views.JobExperienceStageList.as_view()),

    path('ProgrammingLanguage/get/', views.ProgrammingLanguageList.as_view()),
    path('ProgrammingLanguage/get/<int:id>/', views.ProgrammingLanguageList.as_view()),
    path('ProgrammingLanguage/post/', views.ProgrammingLanguageList.as_view()),
    path('ProgrammingLanguage/put/', views.ProgrammingLanguageList.as_view()),
    path('ProgrammingLanguage/delete/', views.ProgrammingLanguageList.as_view()),

    path('JobPositionType/get/', views.JobPositionTypeList.as_view()),
    path('JobPositionType/get/<int:id>/', views.JobPositionTypeList.as_view()),
    path('JobPositionType/post/', views.JobPositionTypeList.as_view()),
    path('JobPositionType/put/', views.JobPositionTypeList.as_view()),
    path('JobPositionType/delete/', views.JobPositionTypeList.as_view()),

    path('JobPosition/get/', views.JobPositionList.as_view()),
    path('JobPosition/get/<int:id>/', views.JobPositionList.as_view()),
    path('JobPosition/post/', views.JobPositionList.as_view()),
    path('JobPosition/put/', views.JobPositionList.as_view()),
    path('JobPosition/delete/', views.JobPositionList.as_view()),

    path('RecruitmentProcess/get/', views.RecruitmentProcessList.as_view()),
    path('RecruitmentProcess/get/<int:id>/', views.RecruitmentProcessList.as_view()),
    path('RecruitmentProcess/get/HREmployee/<int:hr_employee_id>/', views.RecruitmentProcessList.as_view()),
    path('RecruitmentProcess/get/Recruiter/<int:recruiter_id>/', views.RecruitmentProcessList.as_view()),
    path('RecruitmentProcess/get/Candidate/<int:candidate_id>/', views.RecruitmentProcessList.as_view()),
    path('RecruitmentProcess/post/', views.RecruitmentProcessList.as_view()),
    path('RecruitmentProcess/put/', views.RecruitmentProcessList.as_view()),
    path('RecruitmentProcess/delete/', views.RecruitmentProcessList.as_view()),

    path('RecruiterCandidateBinding/get/', views.RecruiterCandidateBindingList.as_view()),
    path('RecruiterCandidateBinding/get/<int:id>/', views.RecruiterCandidateBindingList.as_view()),
    path('RecruiterCandidateBinding/get/Recruiter/<int:recruiter_id>/RecruitmentProcess/<int:process_id>/', views.RecruiterCandidateBindingList.as_view()),
    path('RecruiterCandidateBinding/get/Candidate/<int:candidate_id>/RecruitmentProcess/<int:process_id>/', views.RecruiterCandidateBindingList.as_view()),
    path('RecruiterCandidateBinding/post/', views.RecruiterCandidateBindingList.as_view()),
    path('RecruiterCandidateBinding/put/', views.RecruiterCandidateBindingList.as_view()),
    path('RecruiterCandidateBinding/delete/', views.RecruiterCandidateBindingList.as_view()),

    path('ClosedTestQuestion/get/', views.ClosedTestQuestionList.as_view()),
    path('ClosedTestQuestion/get/<int:id>/', views.ClosedTestQuestionList.as_view()),
    path('ClosedTestQuestion/get/JobExperienceStage/<int:experience_id>/ProgrammingLanguage/<int:language_id>/JobPositionType/<int:position_type_id>/', views.ClosedTestQuestionList.as_view()),
    path('ClosedTestQuestion/post/', views.ClosedTestQuestionList.as_view()),
    path('ClosedTestQuestion/put/', views.ClosedTestQuestionList.as_view()),
    path('ClosedTestQuestion/delete/', views.ClosedTestQuestionList.as_view()),

    path('ClosedQuestionPoints/get/', views.ClosedQuestionPointsList.as_view()),
    path('ClosedQuestionPoints/get/<int:id>/', views.ClosedQuestionPointsList.as_view()),
    path('ClosedQuestionPoints/post/', views.ClosedQuestionPointsList.as_view()),
    path('ClosedQuestionPoints/put/', views.ClosedQuestionPointsList.as_view()),
    path('ClosedQuestionPoints/delete/', views.ClosedQuestionPointsList.as_view()),

    path('OpenTestQuestion/get/', views.OpenTestQuestionList.as_view()),
    path('OpenTestQuestion/get/<int:id>/', views.OpenTestQuestionList.as_view()),
    path('OpenTestQuestion/get/JobExperienceStage/<int:experience_id>/ProgrammingLanguage/<int:language_id>/JobPositionType/<int:position_type_id>/', views.OpenTestQuestionList.as_view()),
    path('OpenTestQuestion/post/', views.OpenTestQuestionList.as_view()),
    path('OpenTestQuestion/put/', views.OpenTestQuestionList.as_view()),
    path('OpenTestQuestion/delete/', views.OpenTestQuestionList.as_view()),

    path('OpenQuestionPoints/get/', views.OpenQuestionPointsList.as_view()),
    path('OpenQuestionPoints/get/<int:id>/', views.OpenQuestionPointsList.as_view()),
    path('OpenQuestionPoints/get/OpenTestQuestion/<int:question_id>/JobExperienceStage/<int:experience_id>/', views.OpenQuestionPointsList.as_view()),
    path('OpenQuestionPoints/post/', views.OpenQuestionPointsList.as_view()),
    path('OpenQuestionPoints/put/', views.OpenQuestionPointsList.as_view()),
    path('OpenQuestionPoints/delete/', views.OpenQuestionPointsList.as_view()),

    path('Test/get/', views.TestList.as_view()),
    path('Test/get/<int:id>/', views.TestList.as_view()),
    path('Test/get/RecruitmentProcess/<int:process_id>/Recruiter/<int:recruiter_id>/', views.TestList.as_view()),
    path('Test/get/RecruitmentProcess/<int:process_id>/Candidate/<int:candidate_id>/', views.TestList.as_view()),
    path('Test/generate/RecruitmentProcess/<int:process_id>/<int:closed_count>/<int:open_count>/Recruiter/<int:recruiter_id>/', views.TestList.as_view()),
    path('Test/post/', views.TestList.as_view()),
    path('Test/put/', views.TestList.as_view()),
    path('Test/delete/', views.TestList.as_view()),

    path('WrittenTest/get/', views.WrittenTestList.as_view()),
    path('WrittenTest/get/<int:id>/', views.WrittenTestList.as_view()),
    path('WrittenTest/get/RecruitmentProcess/<int:process_id>/Candidate/<int:candidate_id>/', views.WrittenTestList.as_view()),
    path('WrittenTest/post/', views.WrittenTestList.as_view()),
    path('WrittenTest/put/', views.WrittenTestList.as_view()),
    path('WrittenTest/delete/', views.WrittenTestList.as_view()),

    path('RecruitmentInterview/get/', views.RecruitmentInterviewList.as_view()),
    path('RecruitmentInterview/get/<int:id>/', views.RecruitmentInterviewList.as_view()),
    path('RecruitmentInterview/get/RecruitmentProcess/<int:process_id>/Candidate/<int:candidate_id>/', views.RecruitmentInterviewList.as_view()),
    path('RecruitmentInterview/post/', views.RecruitmentInterviewList.as_view()),
    path('RecruitmentInterview/put/', views.RecruitmentInterviewList.as_view()),
    path('RecruitmentInterview/delete/', views.RecruitmentInterviewList.as_view()),

    path('InterviewNotes/get/', views.InterviewNotesList.as_view()),
    path('InterviewNotes/get/<int:id>/', views.InterviewNotesList.as_view()),
    path('InterviewNotes/post/', views.InterviewNotesList.as_view()),
    path('InterviewNotes/put/', views.InterviewNotesList.as_view()),
    path('InterviewNotes/delete/', views.InterviewNotesList.as_view()),

    path('JobOffer/get/', views.JobOfferList.as_view()),
    path('JobOffer/get/<int:id>/', views.JobOfferList.as_view()),
    path('JobOffer/post/', views.JobOfferList.as_view()),
    path('JobOffer/put/', views.JobOfferList.as_view()),
    path('JobOffer/delete/', views.JobOfferList.as_view()),

]
