from django.core.exceptions import ValidationError

def validate_postal_code(code: str):
    if len(code) > 6:
        raise ValidationError(f"Postal code {code} longer than 6.")
    for c in code:
        if c.isalpha():
            raise ValidationError(f"Non-numeric character in postal code {code}")