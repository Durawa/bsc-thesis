from django.core.exceptions import PermissionDenied

user_types_map = {
    'Candidate': 'candidate',
    'HREmployee': 'hremployee',
    'Recruiter': 'recruiter',
    'ProjectManager': 'projectmanager'
}


def forbid_permission(*user_types):
    def decorator(func):
        def wrapper(*args, **kwargs):
            request = None
            for arg in args:
                if hasattr(arg, 'user'):
                    request = arg
            if request is None:
                raise RuntimeError('Could not find request with user')
            for user_type in user_types:
                if user_type not in user_types_map:
                    raise RuntimeError(f'{user_type} is not a valid user type')
                if hasattr(request.user, user_types_map[user_type]):
                    raise PermissionDenied(f'The view is inaccessible for {user_type} user type.')
            return func(*args, **kwargs)
        return wrapper
    return decorator
