from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from . import models
from django.core.mail import send_mail
from django.db import transaction


@receiver(post_save, sender=models.Test)
def on_test_save(sender, instance, **kwargs):
    def send_mail_on_test_save(instance):
        test = instance
        bindings = test.process.recruiter_bindings.filter(recruiter=test.recruiter)
        candidates = [binding.candidate for binding in bindings]
        for candidate in candidates:
            send_mail(
                subject=f'Test na stanowisko {str(test.process.job_position)}',
                message=f'Szanowna Pani/Szanowny Panie {candidate.first_name} {candidate.last_name},\n\n'
                        f'Informujemy, że do Twojego konta w procesie rekrutacyjnym na stanowisko \n'
                        f'{str(test.process.job_position)} został przypisany test, który odbędzie się dnia \n'
                        f'{test.date.strftime("%d.%m.%Y")}r. Test będzie dostępny przez cały dzień.\n\n'
                        f'Prośby o zmianę terminu testu prosimy kierować na adres {test.recruiter.email}.\n\n'
                        f'Z poważaniem,\n'
                        f'{test.recruiter.first_name} {test.recruiter.last_name}',
                from_email=f'{test.recruiter.email}',
                recipient_list=[candidate.email],
                fail_silently=False
            )
    transaction.on_commit(lambda: send_mail_on_test_save(instance))


@receiver(post_delete, sender=models.Test)
def on_test_delete(sender, instance, **kwargs):
    def send_mail_on_test_delete(instance):
        test = instance
        bindings = test.process.recruiter_bindings.filter(recruiter=test.recruiter)
        candidates = [binding.candidate for binding in bindings]
        for candidate in candidates:
            send_mail(
                subject=f'Odwołanie testu na stanowisko {str(test.process.job_position)}',
                message=f'Szanowna Pani/Szanowny Panie {candidate.first_name} {candidate.last_name},\n\n'
                        f'Informujemy, że test w procesie rekrutacyjnym na stanowisko \n'
                        f'{str(test.process.job_position)}, który miał się'
                        f' odbyć dnia {test.date.strftime("%d.%m.%Y")}r. został odwołany.\n'
                        f'Nowa data testu zostanie wkrótce ustalona przez nasz zespół.\n\n'
                        f'Z poważaniem,\n'
                        f'{test.recruiter.first_name} {test.recruiter.last_name}',
                from_email=f'{test.recruiter.email}',
                recipient_list=[candidate.email],
                fail_silently=False
            )
    transaction.on_commit(lambda: send_mail_on_test_delete(instance))


@receiver(post_save, sender=models.RecruiterCandidateBinding)
def on_recruiter_candidate_binding_save(sender, instance, **kwargs):
    def send_mail_on_recruiter_candidate_binding_save(instance):
        binding = instance
        send_mail(
            subject=f'Informacja o przypisaniu do procesu rekrutacyjnego',
            message=f'Szanowna Pani/Szanowny Panie {binding.candidate.first_name} {binding.candidate.last_name},\n\n'
                    f'Informujemy, że Twoje konto zostało przypisane do procesu rekrutacyjnego na stanowisko {str(binding.process.job_position)}.\n'
                    f'Twoim rekruterem jest {binding.recruiter.first_name} {binding.recruiter.last_name}.\n'
                    f'Prosimy oczekiwać na wyznaczenie terminu testu rekrutacyjnego.\n'
                    f'Pytania dotyczące testu rekrutacyjnego prosimy kierować na skrzynkę mailową: {binding.recruiter.email}.\n\n'
                    f'Z poważaniem,\n'
                    f'{binding.process.hr_employee.first_name} {binding.process.hr_employee.last_name}',
            from_email=f'{binding.process.hr_employee.email}',
            recipient_list=[binding.candidate.email],
            fail_silently=False
        )
    transaction.on_commit(lambda: send_mail_on_recruiter_candidate_binding_save(instance))


@receiver(post_delete, sender=models.RecruiterCandidateBinding)
def on_recruiter_candidate_binding_delete(sender, instance, **kwargs):
    def send_mail_on_recruiter_candidate_binding_delete(instance):
        binding = instance
        send_mail(
            subject=f'Informacja o odpięciu od rekrutera',
            message=f'Szanowna Pani/Szanowny Panie {binding.candidate.first_name} {binding.candidate.last_name},\n\n'
                    f'Informujemy, że Twoje konto zostało odpięte od rekrutera {binding.recruiter.first_name} {binding.recruiter.last_name}\n'
                    f'w procesie rekrutacyjnym na stanowisko {str(binding.process.job_position)}.\n'
                    f'Prosimy oczekiwać na wyznaczenie nowego rekrutera.\n'
                    f'Wszelkie pytania prosimy kierować na skrzynkę mailową: {binding.process.hr_employee.email}.\n\n'
                    f'Z poważaniem,\n'
                    f'{binding.process.hr_employee.first_name} {binding.process.hr_employee.last_name}',
            from_email=f'{binding.process.hr_employee.email}',
            recipient_list=[binding.candidate.email],
            fail_silently=False
        )
    transaction.on_commit(lambda: send_mail_on_recruiter_candidate_binding_delete(instance))


@receiver(post_save, sender=models.WrittenTest)
def on_written_test_save(sender, instance, **kwargs):
    def send_mail_on_written_test_save(instance):
        test = instance
        if test.is_fully_checked:
            closed_points = test.closed_points()
            max_closed = test.max_closed_points()

            open_points = test.open_points()
            max_open = test.max_open_points()

            overall_points = closed_points + open_points
            overall_max_points = max_closed + max_open
            overall_percent = int((float(overall_points) / float(overall_max_points)) * 100.0)

            binding = models.RecruiterCandidateBinding.objects.get(process=test.process, candidate=test.candidate)
            recruiter = binding.recruiter

            send_mail(
                subject=f'Wyniki testu rekrutacyjnego',
                message=f'Szanowna Pani/Szanowny Panie {test.candidate.first_name} {test.candidate.last_name},\n\n'
                        f'Informujemy, że wyniki Twojego testu rekrutacyjnego są następujące:\n'
                        f'Uzyskana ilość punktów: {str(overall_points)}/{str(overall_max_points)}\n'
                        f'Procentowa ilość punktów: {str(overall_percent)}%\n\n'
                        f'Z poważaniem,\n'
                        f'{recruiter.first_name} {recruiter.last_name}',
                from_email=f'{recruiter.email}',
                recipient_list=[test.candidate.email],
                fail_silently=False
            )
    transaction.on_commit(lambda: send_mail_on_written_test_save(instance))


@receiver(post_save, sender=models.RecruitmentInterview)
def on_interview_save(sender, instance, **kwargs):
    def send_mail_on_interview_save(instance):
        interview = instance
        binding = models.RecruiterCandidateBinding.objects.get(process=interview.process, candidate=interview.candidate)
        send_mail(
            subject=f'Spotkanie rekrutacyjne na stanowisko {str(interview.process.job_position)}',
            message=f'Szanowna Pani/Szanowny Panie {interview.candidate.first_name} {interview.candidate.last_name},\n\n'
                    f'Zapraszamy Cię na spotkanie rekrutacyjne na stanowisko {str(interview.process.job_position)}, \n'
                    f'które odbędzie się dnia {interview.date.strftime("%d.%m.%Y")}r. '
                    f'o godzinie {interview.date.strftime("%H:%M")}.\n\n'
                    f'Z poważaniem,\n'
                    f'{binding.recruiter.first_name} {binding.recruiter.last_name}',
            from_email=f'{binding.recruiter.email}',
            recipient_list=[interview.candidate.email],
            fail_silently=False
        )
    transaction.on_commit(lambda: send_mail_on_interview_save(instance))


@receiver(post_delete, sender=models.RecruitmentInterview)
def on_interview_delete(sender, instance, **kwargs):
    def send_mail_on_interview_delete(instance):
        interview = instance
        binding = models.RecruiterCandidateBinding.objects.get(process=interview.process, candidate=interview.candidate)
        send_mail(
            subject=f'Odwołanie spotkania rekrutacyjnego na stanowisko {str(interview.process.job_position)}',
            message=f'Szanowna Pani/Szanowny Panie {interview.candidate.first_name} {interview.candidate.last_name},\n\n'
                    f'Informujemy, że spotkanie rekrutacyjne na stanowisko {str(interview.process.job_position)}, \n'
                    f'które miało odbyć się dnia {interview.date.strftime("%d.%m.%Y")}r. '
                    f'o godzinie {interview.date.strftime("%H:%M")} zostało odwołane.\n\n'
                    f'Z poważaniem,\n'
                    f'{binding.recruiter.first_name} {binding.recruiter.last_name}',
            from_email=f'{binding.recruiter.email}',
            recipient_list=[interview.candidate.email],
            fail_silently=False
        )
    transaction.on_commit(lambda: send_mail_on_interview_delete(instance))
