import pytest
import json
from rest_framework.test import APIClient
from . import models


@pytest.fixture
def api_cli():
    return APIClient()


@pytest.fixture
def test_user():
    return models.User.objects.create_user(username='test_user', password='test_password')


@pytest.fixture
def address1(test_user):
    return models.Address.objects.create(
        id=1,
        street="Broniewskiego",
        house_nr="1",
        flat_nr="2",
        postal_code="75-234",
        city="Koszalin",
        country="Polska",
        created_by=test_user
    )


@pytest.fixture
def address2(test_user):
    return models.Address.objects.create(
        id=2,
        street="Jana Pawła II",
        house_nr="3",
        flat_nr="4",
        postal_code="75-100",
        city="Koszalin",
        country="Polska",
        created_by=test_user
    )


@pytest.fixture
def test_user1():
    return models.User.objects.create_user(username='test_user1', password='test_password1')


@pytest.fixture
def test_user2():
    return models.User.objects.create_user(username='test_user2', password='test_password2')


@pytest.fixture
def test_user3():
    return models.User.objects.create_user(username='test_user3', password='test_password3')


"""
W tym fixture ustawiam sobie baze danych
"""


@pytest.fixture
def setup_db(test_user, address1, address2, test_user1, test_user2, test_user3):
    models.Candidate.objects.create(
        id=1,
        user=test_user1,
        birth_date="1990-08-02",
        birth_place="Gdańsk",
        telephone="560452100",
        address=address1,
        created_by=test_user
    )

    models.Candidate.objects.create(
        id=2,
        user=test_user2,
        birth_date="1985-05-14",
        birth_place="Kołobrzeg",
        telephone="500220841",
        address=address2,
        created_by=test_user
    )

    models.Candidate.objects.create(
        id=3,
        user=test_user3,
        birth_date="1978-03-29",
        birth_place="Koszalin",
        telephone="660543320",
        address=address2,
        created_by=test_user
    )


