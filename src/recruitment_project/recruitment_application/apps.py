from django.apps import AppConfig
from django.db.models.signals import post_save


class RecruitmentApplicationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'recruitment_application'

    def ready(self):
        from . import events



