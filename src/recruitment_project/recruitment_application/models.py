import datetime
import math

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from . import validators
import random
from django.utils import timezone
from django.core.mail import send_mail
from rest_framework.reverse import reverse


def user_to_dict(user):
    return {
        'username': user.username,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'email': user.email
    }


def send_mail_to_user(request, user, password):
    send_mail(
        subject=f'Twoje konto zostało utworzone',
        message=f'Szanowna Pani/Szanowny Panie {user.first_name} {user.last_name},\n\n'
                f'Informujemy, że Twoje konto zostało utworzone.\n'
                f'Teraz możesz zalogować się za pomocą następujących danych:\n\n'
                f'Login: {user.username}\n'
                f'Hasło: {password}\n\n'
                f'Kliknij w poniższy link:\n'
                f'{reverse("login", request=request)}\n\n'
                f'Z poważaniem,\n'
                f'{request.user.first_name} {request.user.last_name}',
        from_email=f'{request.user.email}',
        recipient_list=[user.email],
        fail_silently=False
    )


class Address(models.Model):
    street = models.CharField(max_length=100)
    house_nr = models.CharField(max_length=10)
    flat_nr = models.CharField(max_length=10, null=True)
    postal_code = models.CharField(max_length=10, validators=[validators.validate_postal_code])
    city = models.CharField(max_length=100)
    country = models.CharField(max_length=30)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        slash = "/" if len(self.flat_nr) > 0 else ""
        return f"ul.{self.street} {self.house_nr}{slash}{self.flat_nr} {self.postal_code} {self.city} {self.country}"

    def dict(self):
        return {
            'id': self.id,
            'street': self.street,
            'house_nr': self.house_nr,
            'flat_nr': self.flat_nr,
            'postal_code': self.postal_code,
            'city': self.city,
            'country': self.country,
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }


class HREmployee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField()
    birth_place = models.CharField(max_length=100)
    telephone = models.CharField(max_length=20)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='created_HRemployees')

    @property
    def first_name(self):
        return self.user.first_name

    @property
    def last_name(self):
        return self.user.last_name

    @property
    def email(self):
        return self.user.email

    def dict(self):
        return {
            'id': self.id,
            'user': user_to_dict(self.user),
            'birth_date': self.birth_date.strftime('%d.%m.%Y'),
            'birth_place': self.birth_place,
            'telephone': self.telephone,
            'address': self.address.dict(),
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Recruiter(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField()
    birth_place = models.CharField(max_length=100)
    telephone = models.CharField(max_length=20)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='created_Recruiters')

    @property
    def first_name(self):
        return self.user.first_name

    @property
    def last_name(self):
        return self.user.last_name

    @property
    def email(self):
        return self.user.email

    def dict(self):
        return {
            'id': self.id,
            'user': user_to_dict(self.user),
            'birth_date': self.birth_date.strftime('%d.%m.%Y'),
            'birth_place': self.birth_place,
            'telephone': self.telephone,
            'address': self.address.dict(),
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class ProjectManager(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField(null=True)
    birth_place = models.CharField(max_length=100)
    telephone = models.CharField(max_length=20)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='created_ProjectMangers')

    @property
    def first_name(self):
        return self.user.first_name

    @property
    def last_name(self):
        return self.user.last_name

    @property
    def email(self):
        return self.user.email

    def dict(self):
        return {
            'id': self.id,
            'user': user_to_dict(self.user),
            'birth_date': self.birth_date.strftime('%d.%m.%Y'),
            'birth_place': self.birth_place,
            'telephone': self.telephone,
            'address': self.address.dict(),
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Candidate(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField()
    birth_place = models.CharField(max_length=100)
    telephone = models.CharField(max_length=20)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='created_Candidates')

    @property
    def first_name(self):
        return self.user.first_name

    @property
    def last_name(self):
        return self.user.last_name

    @property
    def email(self):
        return self.user.email

    def dict(self):
        return {
            'id': self.id,
            'user': user_to_dict(self.user),
            'birth_date': self.birth_date.strftime('%d.%m.%Y'),
            'birth_place': self.birth_place,
            'telephone': self.telephone,
            'address': self.address.dict(),
            'address_str': str(self.address),
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class JobExperienceStage(models.Model):
    name = models.CharField(max_length=30, unique=True)
    min_years = models.IntegerField()
    max_years = models.IntegerField()
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'min_years': self.min_years,
            'max_years': self.max_years,
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return self.name


class ProgrammingLanguage(models.Model):
    name = models.CharField(max_length=20, unique=True)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return self.name


class JobPositionType(models.Model):
    name = models.CharField(max_length=30, unique=True)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return self.name


class JobPosition(models.Model):
    experience_stage = models.ForeignKey(JobExperienceStage, on_delete=models.CASCADE)
    language = models.ForeignKey(ProgrammingLanguage, on_delete=models.CASCADE)
    position_type = models.ForeignKey(JobPositionType, on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('experience_stage', 'language', 'position_type')

    def dict(self):
        return {
            'id': self.id,
            'experience_stage': self.experience_stage.dict(),
            'language': self.language.dict(),
            'position_type': self.position_type.dict(),
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return f'{self.experience_stage.name} {self.language.name} {self.position_type.name}'


class RecruitmentProcess(models.Model):
    project_manager = models.ForeignKey(ProjectManager, on_delete=models.CASCADE)
    job_position = models.ForeignKey(JobPosition, on_delete=models.CASCADE)
    hr_employee = models.ForeignKey(HREmployee, on_delete=models.CASCADE, related_name='recruitment_processes')
    recruiters = models.ManyToManyField(Recruiter, related_name='recruitment_processes')
    candidates = models.ManyToManyField(Candidate, related_name='recruitment_processes')
    is_at_test_stage = models.BooleanField(default=True)
    is_open = models.BooleanField(default=True)
    deadline = models.DateField()
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def dict(self):
        candidates_list = []
        for candidate in self.candidates.all():
            candidates_list.append(candidate.dict())

        return {
            'id': self.id,
            'project_manager': self.project_manager.dict(),
            'job_position': self.job_position.dict(),
            'job_position_name': str(self.job_position),
            'hr_employee': self.hr_employee.dict(),
            'recruiters': [recruiter.dict() for recruiter in self.recruiters.all()],
            'candidates': candidates_list,
            'is_at_test_stage': self.is_at_test_stage,
            'is_open': self.is_open,
            'deadline': self.deadline.strftime('%d.%m.%Y'),
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return f'{self.job_position} ({str(self.deadline)}, {str(self.hr_employee)})'


class RecruiterCandidateBinding(models.Model):
    process = models.ForeignKey(RecruitmentProcess, on_delete=models.CASCADE, related_name="recruiter_bindings")
    recruiter = models.ForeignKey(Recruiter, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('process', 'recruiter', 'candidate')

    def dict(self):
        return {
            'id': self.id,
            'process': self.process.dict(),
            'recruiter': self.recruiter.dict(),
            'candidate': self.candidate.dict(),
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }


ANSWER_CHOICES = (
    ('a', 'A'),
    ('b', 'B'),
    ('c', 'C'),
    ('d', 'D')
)


class ClosedTestQuestion(models.Model):
    question_text = models.TextField()
    answer_a = models.CharField(max_length=500)
    answer_b = models.CharField(max_length=500)
    answer_c = models.CharField(max_length=500)
    answer_d = models.CharField(max_length=500)
    correct_answer = models.CharField(max_length=1, choices=ANSWER_CHOICES)
    experience_stage = models.ForeignKey(JobExperienceStage, on_delete=models.CASCADE)
    language = models.ForeignKey(ProgrammingLanguage, on_delete=models.CASCADE)
    position_type = models.ForeignKey(JobPositionType, on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('question_text', 'language', 'position_type')

    def dict(self):
        return {
            'id': self.id,
            'question_text': self.question_text,
            'answer_a': self.answer_a,
            'answer_b': self.answer_b,
            'answer_c': self.answer_c,
            'answer_d': self.answer_d,
            'correct_answer': self.correct_answer,
            'experience_stage': self.experience_stage.dict(),
            'language': self.language.dict(),
            'position_type': self.position_type.dict(),
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return f'{self.question_text} ({str(self.experience_stage)} {str(self.language)} {str(self.position_type)})'


class ClosedQuestionPoints(models.Model):
    question = models.ForeignKey(ClosedTestQuestion, on_delete=models.CASCADE)
    experience_stage = models.ForeignKey(JobExperienceStage, on_delete=models.CASCADE)
    points = models.IntegerField()
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('question', 'experience_stage')

    def dict(self):
        return {
            'id': self.id,
            'question': self.question.dict(),
            'experience_stage': self.experience_stage.dict(),
            'points': self.points,
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }


class OpenTestQuestion(models.Model):
    question_text = models.TextField()
    experience_stage = models.ForeignKey(JobExperienceStage, on_delete=models.CASCADE)
    language = models.ForeignKey(ProgrammingLanguage, on_delete=models.CASCADE)
    position_type = models.ForeignKey(JobPositionType, on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('question_text', 'language', 'position_type')

    def dict(self):
        return {
            'id': self.id,
            'question_text': self.question_text,
            'experience_stage': self.experience_stage.dict(),
            'language': self.language.dict(),
            'position_type': self.position_type.dict(),
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }


class OpenQuestionPoints(models.Model):
    question = models.ForeignKey(OpenTestQuestion, on_delete=models.CASCADE)
    experience_stage = models.ForeignKey(JobExperienceStage, on_delete=models.CASCADE)
    points = models.IntegerField()
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('question', 'experience_stage')

    def dict(self):
        return {
            'id': self.id,
            'question': self.question.dict(),
            'experience_stage': self.experience_stage.dict(),
            'max_points': self.points,
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }


class MockTest:
    def __init__(self):
        self.process = None
        self.recruiter = None
        self.closed_test_questions = []
        self.open_test_questions = []

    def dict(self):
        return {
            'process': self.process.dict(),
            'recruiter': self.recruiter.dict(),
            'closed_test_questions': [question.dict() for question in self.closed_test_questions],
            'open_test_questions': [question.dict() for question in self.open_test_questions]
        }


class Test(models.Model):
    process = models.ForeignKey(RecruitmentProcess, on_delete=models.CASCADE, related_name='tests')
    recruiter = models.ForeignKey(Recruiter, on_delete=models.CASCADE, related_name='tests')
    date = models.DateField()
    closed_test_questions = models.ManyToManyField(ClosedTestQuestion, related_name='tests')
    open_test_questions = models.ManyToManyField(OpenTestQuestion, related_name='tests')
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    @property
    def is_past(self):
        return self.date < datetime.date.today()

    class Meta:
        unique_together = ('process', 'recruiter')

    def dict(self):
        return {
            'id': self.id,
            'process': self.process.dict(),
            'recruiter': self.recruiter.dict(),
            'date': self.date.strftime('%d.%m.%Y'),
            'closed_test_questions': [question.dict() for question in self.closed_test_questions.all()],
            'open_test_questions': [question.dict() for question in self.open_test_questions.all()],
            'is_past': self.is_past,
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def clean(self):
       pass
       # if self.is_past:
       #     raise ValidationError('Cannot change past test')

    def save(self, *args, **kwargs):
        self.clean()
        super(Test, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.clean()
        super(Test, self).delete(*args, **kwargs)

    def __str__(self):
        return f'TEST ({str(self.process)}/{str(self.recruiter)})'

    @staticmethod
    def generate(process, closed_count, open_count, recruiter):
        closed_qs = list(ClosedTestQuestion.objects.filter(experience_stage=process.job_position.experience_stage,
                                                           language=process.job_position.language,
                                                           position_type=process.job_position.position_type))
        open_qs = list(OpenTestQuestion.objects.filter(experience_stage=process.job_position.experience_stage,
                                                       language=process.job_position.language,
                                                       position_type=process.job_position.position_type))
        random.shuffle(closed_qs)
        random.shuffle(open_qs)
        closed_qs = closed_qs[:closed_count]
        open_qs = open_qs[:open_count]

        test = MockTest()
        test.process = process
        test.recruiter = recruiter
        for q in closed_qs:
            test.closed_test_questions.append(q)
        for q in open_qs:
            test.open_test_questions.append(q)
        return test

    @classmethod
    def get_test(cls, **kwargs):
        if 'process' in kwargs and 'recruiter' in kwargs:
            tests = cls.objects.filter(process=kwargs['process'], recruiter=kwargs['recruiter'])
        elif 'process' in kwargs and 'candidate' in kwargs:
            try:
                binding = RecruiterCandidateBinding.objects.get(process=kwargs['process'],
                                                                candidate=kwargs['candidate'])
            except Exception:
                return None
            tests = cls.objects.filter(process=kwargs['process'], recruiter=binding.recruiter)
        if len(tests) > 1:
            raise RuntimeError('Database inconsistency! More than one test assigned to one recruiter in one process!')
        elif len(tests) == 0:
            return None
        else:
            return tests[0]


class WrittenTest(models.Model):
    process = models.ForeignKey(RecruitmentProcess, on_delete=models.CASCADE)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('candidate', 'test')

    def max_closed_points(self):
        questions = self.test.closed_test_questions.all()
        return sum([ClosedQuestionPoints.objects.get(question=q,
                                                     experience_stage=self.process.job_position.experience_stage).points
                    for q in
                    questions])

    def closed_points(self):
        answered_closed = self.answered_closed.all()
        closed_sum = sum([q.points for q in answered_closed])
        return closed_sum

    def max_open_points(self):
        questions = self.test.open_test_questions.all()
        return sum([OpenQuestionPoints.objects.get(question=q,
                                                   experience_stage=self.process.job_position.experience_stage).points
                    for q in
                    questions])

    def open_points(self):
        answered_open = self.answered_open.all()
        open_sum = sum([q.points for q in answered_open])
        return open_sum

    @property
    def points(self):
        return self.closed_points() + self.open_points()

    @property
    def points_percent(self):
        return int((self.points / (self.max_closed_points() + self.max_open_points())) * 100.0)

    @property
    def is_fully_checked(self):
        open_answers = self.answered_open.all()
        for answer in open_answers:
            if not answer.checked:
                return False
        return True

    def dict(self):
        closed_points = self.closed_points()
        max_closed = self.max_closed_points()
        percent = int((float(closed_points) / float(max_closed)) * 100.0)

        open_points = self.open_points()
        max_open = self.max_open_points()
        open_percent = int((float(open_points) / float(max_open)) * 100.0)

        overall_points = closed_points + open_points
        overall_max_points = max_closed + max_open
        overall_percent = int((float(overall_points) / float(overall_max_points)) * 100.0)

        return {
            'id': self.id,
            'process': self.process.dict(),
            'test': self.test.dict(),
            'candidate': self.candidate.dict(),
            'points': closed_points + open_points,
            'points_percent': overall_percent,
            'closed_points': closed_points,
            'max_closed_points': max_closed,
            'closed_points_percent': percent,
            'open_points_percent': open_percent,
            'open_points': open_points,
            'max_open_points': max_open,
            'is_fully_checked': self.is_fully_checked,
            'closed_test_questions': [q.dict() for q in self.answered_closed.all()],
            'open_test_questions': [q.dict() for q in self.answered_open.all()],
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }

    def __str__(self):
        return f'{self.test} - WRITTEN'


class AnsweredClosedQuestion(models.Model):
    process = models.ForeignKey(RecruitmentProcess, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    question = models.ForeignKey(ClosedTestQuestion, on_delete=models.CASCADE)
    answer = models.CharField(max_length=1, choices=ANSWER_CHOICES)
    test = models.ForeignKey(WrittenTest, on_delete=models.CASCADE, related_name='answered_closed')

    class Meta:
        unique_together = ('question', 'test')

    def is_correct(self):
        return self.answer == self.question.correct_answer

    @property
    def points(self):
        if not self.is_correct():
            return 0
        return ClosedQuestionPoints.objects.get(question=self.question,
                                                experience_stage=self.process.job_position.experience_stage).points

    def dict(self):
        d = {
            'id': self.id,
            'process': self.process.dict(),
            'candidate': self.candidate.dict(),
            'answer': self.answer,
            'points': self.points,
            'is_correct': self.is_correct()
        }
        q_dict = self.question.dict()
        del q_dict['id']
        del q_dict['creation_date']
        del q_dict['created_by']
        d.update(q_dict)
        return d


class AnsweredOpenQuestion(models.Model):
    process = models.ForeignKey(RecruitmentProcess, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    question = models.ForeignKey(OpenTestQuestion, on_delete=models.CASCADE)
    answer = models.TextField(null=True)
    checked = models.BooleanField(default=False)
    test = models.ForeignKey(WrittenTest, on_delete=models.CASCADE, related_name='answered_open')
    given_points = models.IntegerField(default=0)
    notes = models.TextField(null=True)

    def is_correct(self):
        if not self.checked:
            return None
        return self.given_points > 0

    @property
    def max_points(self):
        points = OpenQuestionPoints.objects.get(question=self.question,
                                                experience_stage=self.process.job_position.experience_stage)
        return points.points

    @property
    def points(self):
        return self.given_points

    @points.setter
    def points(self, value):
        self.points = value

    def dict(self):
        d = {
            'id': self.id,
            'process': self.process.dict(),
            'candidate': self.candidate.dict(),
            'answer': self.answer,
            'points': self.points,
            'max_points': self.max_points,
            'is_correct': self.is_correct(),
            'checked': self.checked,
            'notes': self.notes
        }
        q_dict = self.question.dict()
        del q_dict['id']
        del q_dict['creation_date']
        del q_dict['created_by']
        d.update(q_dict)
        return d

    def clean(self):
        max_points = OpenQuestionPoints.objects.get(question=self.question,
                                                    experience_stage=self.process.job_position.experience_stage)
        if self.given_points > max_points.points:
            raise ValidationError(f'Cannot assign more than {str(max_points.points)} for '
                                  f'question {str(self.question)} in process {str(self.process)}.')

    def save(self, *args, **kwargs):
        self.clean()
        super(AnsweredOpenQuestion, self).save(*args, **kwargs)


class RecruitmentInterview(models.Model):
    process = models.ForeignKey(RecruitmentProcess, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    date = models.DateTimeField()
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    @property
    def is_past(self):
            return self.date < timezone.now()

    class Meta:
        unique_together = ('process', 'candidate')

    def dict(self):
        return {
            'id': self.id,
            'process': self.process.dict(),
            'candidate': self.candidate.dict(),
            'date': self.date.strftime('%d.%m.%Y %H:%M'),
            'day': self.date.strftime('%d.%m.%Y'),
            'hour': self.date.strftime('%H:%M'),
            'is_past': self.is_past,
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }


class InterviewNotes(models.Model):
    interview = models.ForeignKey(RecruitmentInterview, on_delete=models.CASCADE)
    notes = models.TextField()
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def dict(self):
        return {
            'id': self.id,
            'interview': self.interview.dict(),
            'notes': self.notes,
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }


class JobOffer(models.Model):
    process = models.ForeignKey(RecruitmentProcess, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    salary = models.IntegerField()
    is_accepted = models.BooleanField(default=False)
    creation_date = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('process', 'candidate')

    def dict(self):
        return {
            'id': self.id,
            'process': self.process.dict(),
            'candidate': self.candidate.dict(),
            'salary': self.salary,
            'is_accepted': self.is_accepted,
            'creation_date': str(self.creation_date),
            'created_by': user_to_dict(self.created_by)
        }
