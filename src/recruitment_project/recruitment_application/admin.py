from django.contrib import admin
from . import models


@admin.register(models.Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ('street', 'house_nr', 'flat_nr', 'postal_code',
                    'city', 'country', 'creation_date', 'created_by')


@admin.register(models.HREmployee)
class HREmployeeAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'birth_date', 'birth_place', 'telephone',
                    'email', 'address', 'creation_date', 'created_by')


@admin.register(models.Recruiter)
class RecruiterAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'birth_date', 'birth_place', 'telephone',
                    'email', 'address', 'creation_date', 'created_by')


@admin.register(models.ProjectManager)
class ProjectManagerAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'birth_date', 'birth_place', 'telephone',
                    'email', 'address', 'creation_date', 'created_by')


@admin.register(models.Candidate)
class CandidateAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'birth_date', 'birth_place', 'telephone',
                    'email', 'address', 'creation_date', 'created_by')


@admin.register(models.JobExperienceStage)
class JobExperienceStageAdmin(admin.ModelAdmin):
    list_display = ('name', 'min_years', 'max_years',
                    'creation_date', 'created_by')


@admin.register(models.ProgrammingLanguage)
class ProgrammingLanguageAdmin(admin.ModelAdmin):
    list_display = ('name', 'creation_date', 'created_by')


@admin.register(models.JobPositionType)
class JobPositionTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'creation_date', 'created_by')


@admin.register(models.JobPosition)
class JobPositionAdmin(admin.ModelAdmin):
    list_display = ('experience_stage', 'language',
                    'position_type', 'creation_date', 'created_by')


@admin.register(models.RecruitmentProcess)
class RecruitmentProcessAdmin(admin.ModelAdmin):
    list_display = ('project_manager', 'job_position', 'hr_employee', 'is_at_test_stage',
                    'is_open', 'deadline', 'creation_date', 'created_by')


@admin.register(models.RecruiterCandidateBinding)
class RecruiterCandidateBindingAdmin(admin.ModelAdmin):
    list_display = ('process', 'recruiter', 'candidate',
                    'creation_date', 'created_by')


@admin.register(models.ClosedTestQuestion)
class ClosedTestQuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'answer_a', 'answer_b', 'answer_c', 'answer_d', 'correct_answer',
                    'experience_stage', 'language', 'position_type', 'creation_date', 'created_by')


@admin.register(models.ClosedQuestionPoints)
class ClosedQuestionPointsAdmin(admin.ModelAdmin):
    list_display = ('question', 'experience_stage',
                    'points', 'creation_date', 'created_by')


@admin.register(models.OpenTestQuestion)
class OpenTestQuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'experience_stage', 'language', 'position_type',
                    'creation_date', 'created_by')


@admin.register(models.OpenQuestionPoints)
class OpenQuestionPointsAdmin(admin.ModelAdmin):
    list_display = ('question', 'experience_stage',
                    'points', 'creation_date', 'created_by')


@admin.register(models.Test)
class TestAdmin(admin.ModelAdmin):
    list_display = ('process', 'recruiter',
                    'date', 'creation_date', 'created_by')


@admin.register(models.WrittenTest)
class WrittenTestAdmin(admin.ModelAdmin):
    list_display = ('process', 'test', 'candidate',
                    'points', 'creation_date', 'created_by')


@admin.register(models.RecruitmentInterview)
class RecruitmentInterviewAdmin(admin.ModelAdmin):
    list_display = ('process', 'candidate', 'date',
                    'creation_date', 'created_by')


@admin.register(models.InterviewNotes)
class InterviewNotesAdmin(admin.ModelAdmin):
    list_display = ('interview', 'notes', 'creation_date', 'created_by')


@admin.register(models.JobOffer)
class JobOfferAdmin(admin.ModelAdmin):
    list_display = ('process', 'candidate', 'salary',
                    'is_accepted', 'creation_date', 'created_by')


@admin.register(models.AnsweredClosedQuestion)
class AnsweredClosedQuestionAdmin(admin.ModelAdmin):
    list_display = ('process', 'candidate', 'question', 'answer', 'test')


@admin.register(models.AnsweredOpenQuestion)
class AnsweredOpenQuestionAdmin(admin.ModelAdmin):
    list_display = ('process', 'candidate', 'question', 'answer', 'checked', 'test', 'given_points')
