import pytest
import json
from rest_framework.test import APIClient
from django.urls import reverse

"""
Zamiast modułu requests!
Metody http zwracają obiekt Response
Odpowiedź w Response jest pod atrybutem content

example:
res = client.get(url)
j = json.loads(res.content)
"""


@pytest.mark.get
@pytest.mark.django_db  # django_db wszedzie gdzie accesuje baze danych
def test_address_get_all(api_cli, setup_db):
    url = reverse('recruitment_application:AddressGetAll')
    resp = api_cli.get(url)
    j = json.loads(resp.content)
    assert len(j['addresses']) == 2, 'Wrong number of addresses in db'


@pytest.mark.get
@pytest.mark.django_db
def test_address_get(api_cli, setup_db):
    id = 1
    url = reverse('recruitment_application:AddressGetSpecific', kwargs={'id': id})
    resp = api_cli.get(url)
    j = json.loads(resp.content)
    assert resp.status_code == 200, "Invalid response"
    assert j['id'] == id, "Invalid id"
    assert j['street'] == "Broniewskiego", "Invalid street"
    assert j['house_nr'] == "1", "Invalid house number"
    assert j['flat_nr'] == "2", "Invalid flat number"
    assert j['postal_code'] == "75-234", "Invalid postal code"
    assert j['city'] == "Koszalin", "Invalid city"
    assert j['country'] == "Polska", "Invalid country"


@pytest.mark.get
@pytest.mark.django_db
@pytest.mark.parametrize("id, house_nr, flat_nr, postal_code", [
    (1, "1", "2", "75-234"),
    (2, "3", "4", "75-100")
    ])
def test_address_get_2(api_cli, setup_db, id, house_nr, flat_nr, postal_code):
    url = reverse('recruitment_application:AddressGetSpecific', kwargs={'id': id})
    resp = api_cli.get(url)
    j = json.loads(resp.content)
    assert resp.status_code == 200, "Invalid response"
    assert j['id'] == id, "Invalid id"
    assert j['house_nr'] == house_nr, "Invalid house number"
    assert j['flat_nr'] == flat_nr, "Invalid flat number"
    assert j['postal_code'] == postal_code, "Invalid postal code"


@pytest.mark.post
@pytest.mark.django_db
def test_address_post(api_cli: APIClient, test_user):
    url = reverse('recruitment_application:AddressPost')
    data = {
        "street": "Chopina",
        "house_nr": "1c",
        "flat_nr": "2",
        "postal_code": "75-443",
        "city": "Koszalin",
        "country": "Polska"
    }
    api_cli.force_login(test_user)
    resp = api_cli.post(url, data)
    j = json.loads(resp.content)
    assert resp.status_code == 200, j['error']
    assert 'id' in j, "No id in response"
    for key in data.keys():
        assert j[key] == data[key], "Invalid response"


@pytest.mark.put
@pytest.mark.django_db
def test_address_put(api_cli: APIClient, setup_db, test_user):
    url = reverse('recruitment_application:AddressPut')
    data = {
        "id": 2,
        "street": "Jana Pawła II",
        "house_nr": "3",
        "flat_nr": "4",
        "postal_code": "70-100",
        "city": "Szczecin",
        "country": "Polska"
    }
    api_cli.force_login(test_user)
    resp = api_cli.put(url, data)
    j = json.loads(resp.content)
    print(resp.status_code)
    assert resp.status_code == 200, j['error']
    assert 'id' in j, 'No id in response'
    for key in data.keys():
        assert j[key] == data[key], 'Invalid response'


@pytest.mark.delete
@pytest.mark.django_db
def test_address_delete(api_cli: APIClient, setup_db):
    url = reverse('recruitment_application:AddressDelete')
    data = {'id': 1}
    resp = api_cli.delete(url, data)
    assert resp.status_code == 204, "invalid status code"

    resp = api_cli.get(reverse("recruitment_application:AddressGetAll"))
    j = json.loads(resp.content)
    assert len(j['addresses']) == 1, "delete operation did not work"


#############################################################################################


@pytest.mark.get
@pytest.mark.django_db
def test_candidate_get_all(api_cli, setup_db):
    url = reverse('recruitment_application:CandidateGetAll')
    resp = api_cli.get(url)
    j = json.loads(resp.content)
    assert len(j['candidates']) == 3, "Wrong number of candidates in db"


@pytest.mark.get
@pytest.mark.django_db
def test_candidate_get(api_cli, setup_db, test_user1, address1):
    id = 1
    url = reverse('recruitment_application:CandidateGetSpecific', kwargs={'id': id})
    resp = api_cli.get(url)
    j = json.loads(resp.content)
    assert resp.status_code == 200, "invalid response"
    assert j['id'] == id, "Invalid id"
    assert j['user']['username'] == test_user1.username
    assert j['birth_date'] == "02.08.1990", "Invalid birth date"
    assert j['birth_place'] == "Gdańsk", "Invalid birth plsce"
    assert j['telephone'] == "560452100", "Invalid telephone"
    assert j['address']['street'] == address1.street
    assert j['address']['house_nr'] == address1.house_nr
    assert j['address']['flat_nr'] == address1.flat_nr
    assert j['address']['postal_code'] == address1.postal_code
    assert j['address']['city'] == address1.city
    assert j['address']['country'] == address1.country


@pytest.mark.get
@pytest.mark.django_db
@pytest.mark.parametrize("id, birth_date, birth_place, telephone", [
    (1, "02.08.1990", "Gdańsk", "560452100"),
    (2, "14.05.1985", "Kołobrzeg", "500220841"),
    (3, "29.03.1978", "Koszalin", "660543320")
])
def test_candidate_get_2(api_cli, setup_db, id, birth_date, birth_place, telephone):
    url = reverse('recruitment_application:CandidateGetSpecific', kwargs={'id': id})
    resp = api_cli.get(url)
    j = json.loads(resp.content)
    assert resp.status_code == 200, "Invalid response"
    assert j['id'] == id, "Invalid id"
    assert j['birth_date'] == birth_date, "Invalid birth date"
    assert j['birth_place'] == birth_place, "Invalid birth place"
    assert j['telephone'] == telephone, "Invalid telephone"


@pytest.mark.post
@pytest.mark.django_db
def test_candidate_post(api_cli: APIClient, setup_db, test_user):
    url = reverse('recruitment_application:CandidatePost')
    data = {
        "username": "test_user4",
        "first_name": "Mikołaj",
        "last_name": "Sikora",
        "email": "m.sikora@gmail.com",
        "birth_date": "1992-06-10",
        "birth_place": "Szczecin",
        "telephone": "510981400",
        "address": {'id': 1}
    }
    api_cli.force_login(test_user)
    resp = api_cli.post(url, data, format='json')
    j = json.loads(resp.content)
    print(j)
    assert resp.status_code == 200, j['error']
    assert 'id' in j, "No id in response"
    for key in data.keys():
        assert j[key] == data[key], "Invalid response"



@pytest.mark.put
@pytest.mark.django_db
def test_candidate_put(api_cli: APIClient, setup_db, test_user):
    url = reverse('recruitment_application:CandidatePut')
    data = {
        "id": 1,
        "birth_date": "1990-02-28",
        "birth_place": "Warszawa",
        "telephone": "560452100",
        "address": {'id': 1}
    }
    api_cli.force_login(test_user)
    resp = api_cli.put(url, data, format='json')
    j = json.loads(resp.content)
    print(resp.status_code)
    assert resp.status_code == 200, j['error']
    assert 'id' in j, 'No id in response'
    for key in data.keys():
        if key not in ['address', 'birth_date']:
            assert j[key] == data[key], "Invalid response"
    assert j['birth_date'] == "28.02.1990", "Invalid date"

    resp = api_cli.get(reverse("recruitment_application:AddressGetSpecific", kwargs={"id": 1}))
    address = json.loads(resp.content)
    assert j['address'] == address, "Invalid address"


@pytest.mark.delete
@pytest.mark.django_db
@pytest.mark.parametrize("id", [1, 2, 3])
def test_candidate_delete(api_cli: APIClient, setup_db, id):
    url = reverse("recruitment_application:CandidateDelete")
    data = {'id': id}
    resp = api_cli.delete(url, data)
    assert resp.status_code == 204, "Invalid status code"













"""
# Create your tests here.
class AddressTestCase(TestCase):
    a = None
    def setUp(self):
        self.a = models.Address()
        self.a.street = 'Sezamkowa'
        self.a.house_nr = '1a'
        self.a.flat_nr = '2'
        self.a.postal_code = '75-449'
        self.a.city = 'Koszalin'
        self.a.country = 'Polska'

    def test_string_repr(self):
        self.assertEqual(
            first=str(self.a),
            second='ul.Sezamkowa 1a/2 75-449 Koszalin Polska'
        )


class GenPasswordTestCase(TestCase):
    def test_password_generator(self):
        pwd = utilities.generate_password(
            capitals=3,
            lower=3,
            numbers=3,
            specials=1
        )
        self.assertEqual(len(pwd), 10)


class ClosedQuestionsIntegrityTest(TestCase):
    def test_integrity(self):
        questions = models.ClosedTestQuestion.objects.all()
        for question in questions:
            print(str(question))
            try:
                models.ClosedQuestionPoints.objects.get(question=question,
                                                       experience_stage=question.experience_stage)
            except Exception:
                self.fail(f'No points for {str(question)} on level {str(question.experience_stage)}')

"""
