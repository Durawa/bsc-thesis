var path = require('path')
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

module.exports = {
    context: __dirname,
    entry: ['./node_src/js/index.jsx'],
    watch: true,
    devtool: 'source-map',
    output: {
        path: path.resolve('./static/js/'),
        filename: "[name].js",
    },
    plugins: [
        new BundleTracker({filename: './webpack-stats.json'}),
    ],
    module: {
        rules: [
            {test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/},
            {test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/, options: { presets: ['@babel/preset-env', '@babel/preset-react'] }},
            {test: /\.css$/i, use: ["style-loader", "css-loader"]}
        ]
    }
}