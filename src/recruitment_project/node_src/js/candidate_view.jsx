import React, { Component } from 'react'
import { Button, Card, Col, Container, Row } from 'react-bootstrap';
import { CandidateAPI, UserAPI } from './API/models_api.js';
import {
    find_element,
    get_processes_for_candidate,
    get_recruiter_for_candidate_from_process
} from './common_functions.jsx';
import { RecruitmentProcessView } from './components/recruitment_process_view.jsx';
import { CANDIDATE_KEY } from './constants.js';
import { WriteTestView } from './components/test_view.jsx';
import { MeetingDateView } from './components/meeting_date_view.jsx';

export class CandidateView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            processes: null,
            chosen_process: null,
            process_id: null,
            candidate_recruiter_id: null,
            recruiter: null,
            candidate: null
        }
        this.get_data()
    }

    get_data = async () => {
        const user = await UserAPI.http_get()
        const candidate = await CandidateAPI.http_get(user.id)
        const processes_obj = await get_processes_for_candidate(candidate.id)
        const recruiter_obj = await get_recruiter_for_candidate_from_process(candidate.id, processes_obj.process_id)
        this.setState(() => {
            return {
                ...processes_obj,
                ...recruiter_obj,
                candidate: candidate
            }
        })
    }

    on_process_changed = async (process, idx) => {
        const new_state = {
            process_id: process.id,
            chosen_process: idx
        }
        const user = await UserAPI.http_get()
        const candidate = await CandidateAPI.http_get(user.id)
        const recruiter_obj = await get_recruiter_for_candidate_from_process(candidate.id, this.state.process_id)
        this.setState(() => {
            return {
                ...new_state,
                ...recruiter_obj
            }
        })
    }

    render() {
        return (
            <Container>
                <Row className="mb-3">
                    <Col md>
                        <RecruitmentProcessView
                            mode={CANDIDATE_KEY}
                            processes={this.state.processes?.recruitment_processes}
                            chosen_process={this.state.chosen_process}
                            on_chosen_process_changed={this.on_process_changed.bind(this)}
                        />
                    </Col>
                    <Col md>
                       <MeetingDateView 
                            process={find_element(this.state.process_id, this.state.processes?.recruitment_processes)}
                            candidate={this.state.candidate}
                        />
                    </Col>
                </Row>
                <Row className="mb-3">
                    <Col md>
                        <WriteTestView candidate={this.state.candidate} process={this.state.process_id}/>
                    </Col>
                </Row>
            </Container>
        )
    }
}