
const languages = {
    PL: 'PL',
    EN: 'EN'
}

const translations = {
    PL: {

    },
    EN: {

    }
}

class Translator {
    selected_language = languages.PL

    selected_language() {
        return this.selected_language;
    }

    set_language(language) {
        if (!language in languages) {
            throw new Error(`Language ${language} is not a supported language.`)
        }
        this.selected_language = languages[language]
    }

    translate(string) {
        if (!string in translations[this.selected_language()])
            throw new Error(`${string} not found for language ${this.selected_language()}`)
        return translations[this.selected_language()][string]
    }
}

const global_translator = new Translator();

export function tr(string) {
    global_translator.translate(string)
}

export function set_language(language) {
    global_translator.set_language(language)
}