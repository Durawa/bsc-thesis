import React, { Component } from 'react';
import { Table, Card, Button, Tabs, Tab } from 'react-bootstrap';
import './style.css'
import {NewEditCandidateModal} from './new_edit_candidate_modal.jsx'
import { WrittenTestAPI } from '../API/models_api.js';
import {ModalTestView} from './test_view.jsx'


export class CandidatesView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        }
    }

    onModalHide = () => {
        this.setState(() => {
            return {
                show: false
            }
        })
    }

    on_add_clicked = () => {
        this.setState(() => {
            return {
                show: true
            }
        })
    }

    render() {
        let tabs = <Tabs>
                        <Tab eventKey="all" title="Wszyscy">
                        <CandidatesCard 
                            has_add_btn={true}
                            title={"Wszyscy kandydaci"}
                            on_add_clicked={this.on_add_clicked.bind(this)}
                            candidates={this.props.candidates_all}
                            chosen_candidate={this.props.chosen_candidate_all}
                            on_chosen_candidate_changed={this.props.on_chosen_candidate_all_changed?.bind(this)}
                            highlight={this.props.highlight}
                        />
                        </Tab>
                        <Tab eventKey="of_recruiter" title="Przypisani do rekrutera">
                        <CandidatesCard
                            has_add_btn={false}
                            title={"Kandydaci przypisani do rekrutera"}
                            candidates={this.props.candidates_recruiter}
                            chosen_candidate={this.props.chosen_candidate_recruiter}
                            on_chosen_candidate_changed={this.props.on_chosen_candidate_recruiter_changed?.bind(this)}
                            highlight={this.props.highlight}
                        />
                        </Tab>
                    </Tabs>
        if (this.props.enable_recruiters_view==false) {
            tabs = <CandidatesCard 
                        has_add_btn={false}
                        title={"Kandydaci"}
                        candidates={this.props.candidates_all}
                        chosen_candidate={this.props.chosen_candidate_all}
                        on_chosen_candidate_changed={this.props.on_chosen_candidate_all_changed?.bind(this)}
                        highlight={this.props.highlight}
                        show_tests={this.props.show_tests}
                        process={this.props.process}
                    />
        }

        return (
            <>
                <NewEditCandidateModal 
                    candidates={this.props.candidates_all}
                    allow_addition_of_existing={true}
                    title={"Nowy kandydat"} 
                    show={this.state.show} 
                    onHide={this.onModalHide.bind(this)}
                    onSave={this.props.on_candidate_created?.bind(this)}
                    onSaveExisting={this.props.on_candidate_add_existing?.bind(this)}
                />
                {tabs}
            </>
        )

    }
}

class CandidatesCard extends Component {

    render() {
        let add_btn = null;
        if (this.props.has_add_btn == true) {
            add_btn = <Button onClick={()=>this.props.on_add_clicked?.()} variant="outline-success" size="sm">Dodaj</Button>
        }
        return (
            <Card>
            <Card.Body>
                <div className="components_title_div">
                    <Card.Title>{this.props.title}</Card.Title>
                    {add_btn}
                </div>
                <CandidateTable 
                    candidates={this.props.candidates}
                    chosen_candidate={this.props.chosen_candidate}
                    on_chosen_candidate_changed={this.props.on_chosen_candidate_changed?.bind(this)}
                    highlight={this.props.highlight}
                    show_tests={this.props.show_tests}
                    process={this.props.process}
                />
            </Card.Body>
        </Card>
        )
    }
}

class CandidateTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tests: null,
            show_modal: false,
            test: null,
            candidate: null
        }
    }

    get_data = async () => {
        if (this.props.candidates) {
            let test_objects = {};
            for (let candidate of this.props.candidates) {
                if (candidate != undefined) {
                    const written_test = await WrittenTestAPI.getForProcessAndCandidate(this.props.process.id, candidate.id)
                    test_objects[candidate.id] = written_test
                }
            }
            this.setState(() => {
                return {
                    tests: {...test_objects}
                }
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.candidates != this.props.candidates || prevProps.process != this.props.process) {
            this.get_data()
        }
    }

    check_test = (candidate) => {
        this.setState(() => {
            return {
                candidate: candidate,
                mode: "check",
                test: this.state.tests[candidate.id].test,
                show_modal: true
            }
        })
    }

    show_test = (candidate) => {
        this.setState(() => {
            return {
                candidate: candidate,
                mode: "view",
                test: this.state.tests[candidate.id].test,
                show_modal: true
            }
        })
    }

    on_close_modal = () => {
        this.setState(() => {
            return {
                show_modal: false
            }
        })
    }

    post_action = () => {
        this.get_data()
    }

    render() {
        let headers = null;
        if (this.props.show_tests == true) {
            headers = <>
                        <th>Wynik</th>
                        <th></th>
                    </>
        }
        return (
            <>
                <ModalTestView
                    show={this.state.show_modal}
                    mode={this.state.mode}
                    on_close_clicked={this.on_close_modal?.bind(this)}
                    heading="Test"
                    test={this.state.test}
                    candidate={this.state.candidate}
                    cancel_text="Anuluj"
                    ok_text="Zatwierdź"
                    on_ok_clicked={this.on_ok_clicked?.bind(this)}
                    post_action={this.post_action?.bind(this)}
                />
                <Table hover size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Kandydat</th>
                        <th>Telefon</th>
                        <th>E-mail</th>
                        {headers}
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.candidates?.map((candidate, idx) => {
                            let chosenStyle = (idx == this.props.chosen_candidate) ? { backgroundColor: this.props.highlight, cursor: 'pointer' } : { cursor: 'pointer' };
                            let test_values = null;
                            let percent = null
                            let look_btn = null
                            if (this.props.show_tests) {
                                if (this.state.tests) {                                    
                                    if (candidate.id in this.state.tests && this.state.tests[candidate.id].test) {
                                        if (this.state.tests[candidate.id].test?.is_fully_checked) {
                                            percent = this.state.tests[candidate.id].test?.points_percent
                                            percent = `${percent}%`
                                            look_btn = <Button onClick={()=>this.show_test(candidate)} variant="outline-success" size="sm">Zobacz test</Button>
                                        } else {
                                            percent = "N/S"
                                            look_btn = <Button onClick={()=>this.check_test(candidate)} variant="outline-success" size="sm">Sprawdź test</Button>
                                        }
                                    } else if (candidate.id in this.state.tests) {
                                        percent = "N/D"
                                    }
                                }
                            }
                            test_values = <>
                                <td>{percent}</td>
                                <td>{look_btn}</td>
                            </>
                            return (
                                <tr key={idx} style={chosenStyle} onClick={() => {
                                    this.props.on_chosen_candidate_changed?.(candidate, idx);
                                }}>
                                    <td>{idx + 1}</td>
                                    <td>{candidate.user.first_name} {candidate.user.last_name}</td>
                                    <td>{candidate.telephone}</td>
                                    <td>{candidate.user.email}</td>
                                    {test_values}
                                </tr>
                            )
                        })
                    }
                </tbody>
            </Table>
        </>
        )
    }
}