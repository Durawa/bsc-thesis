import React, { Component } from 'react';
import { Table , Card } from 'react-bootstrap';
import {
    HR_EMPLOYEE_HIGHLIGHT
} from './../constants.js'

export class RecruitersView extends Component {
    render() {
        if (this.props.recruiters) {
            return (
                <Card>
                    <Card.Body>
                        <Card.Title>Rekruterzy</Card.Title>
                        <Table hover size="sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Rekruter</th>
                                    <th>Telefon</th>
                                    <th>E-mail</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.props.recruiters?.map((recruiter, idx) => {
                                        let chosenStyle = (idx == this.props.chosen_recruiter) ? { backgroundColor: HR_EMPLOYEE_HIGHLIGHT, cursor: 'pointer '} : { cursor: 'pointer' };
                                        return (
                                            <tr key={idx} style={chosenStyle} onClick={() => {
                                                this.props.on_chosen_recruiter_changed?.(recruiter, idx);
                                            }}>
                                                <td>{idx + 1}</td>
                                                <td>{recruiter.user.first_name} {recruiter.user.last_name}</td>
                                                <td>{recruiter.telephone}</td>
                                                <td>{recruiter.user.email}</td>
                                            </tr>
                                        )
                                    }) 
                                }
                            </tbody>
                        </Table>
                    </Card.Body>
                </Card>
            )
        }
        return null;
    }
}