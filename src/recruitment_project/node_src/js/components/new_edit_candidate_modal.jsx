import React, { Component } from 'react';
import { Modal, Form, Button, Container, Row, Col, Card } from 'react-bootstrap';
import {CandidateAPI} from './../API/models_api.js'
import {add_zero_padding} from './../common_functions.jsx'

export class NewEditCandidateModal extends Component {

    login = "";
    first_name = "";
    last_name = "";
    birth_date = "";
    birth_place = "";
    phone = "";
    email = "";
    street = "";
    house_nr = "";
    flat_nr = "";
    postal_code = "";
    city = "";
    country = "";

    constructor(props) {
        super(props)
        this.state = {
            all_candidates: null,
            add_existing_checked: false,
            selected_candidate: null,
            candidates: null
        }
        this.get_data()
    }

    get_data = async () => {
            const candidates = await CandidateAPI.http_get()
    
            this.setState(() => {
                return {
                    all_candidates: candidates.candidates,
                }
            })
    }

    get_new_candidate = () => {
        const candidate_info = {
            username: this.login,
            first_name: this.first_name,
            last_name: this.last_name,
            birth_date: this.birth_date,
            birth_place: this.birth_place,
            telephone: this.phone,
            email: this.email,
            street: this.street,
            house_nr: this.house_nr,
            flat_nr: this.flat_nr,
            postal_code: this.postal_code,
            city: this.city,
            country: this.country
        }
        let id_obj = {};
        if (this.props.candidate) {
            id_obj = {
                id: this.props.candidate.id,
                address: this.props.candidate.address.id
            }
        }
        return {
            ...candidate_info,
            ...id_obj
        }
    }

    clear = () => {
        this.login = "";
        this.first_name = "";
        this.last_name = "";
        this.birth_date = "";
        this.birth_place = "";
        this.phone = "";
        this.email = "";
        this.street = "";
        this.house_nr = "";
        this.flat_nr = "";
        this.postal_code = "";
        this.city = "";
        this.country = "";
    }

    on_change = (value) => {
        this.setState(() => {
            return {
                add_existing_checked: value
            }
        })
    }

    on_select = (selection) => {
        let selected = this.state.all_candidates?.find(el => el.id == selection)
        this.setState(() => {
            return {
                selected_candidate: selected
            }
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.candidates != this.props.candidates) {
            let candidates = []
            for (let candidate of this.state.all_candidates) {
                if (this.props.candidates?.find(el => el.id == candidate.id) == null) {
                    candidates.push(candidate)
                }
            }
            let selected_candidate = null;
            if (candidates.length > 0) {
                selected_candidate = candidates[0]
            }
            this.setState(() => {
                return {
                    candidates: candidates,
                    selected_candidate: selected_candidate
                }
            })
        }

        if (prevProps.candidate != this.props.candidate) {
            if (this.props.candidate) {
                this.login = this.props.candidate.user.username;
                this.first_name = this.props.candidate.user.first_name;
                this.last_name = this.props.candidate.user.last_name;
                
                const birth_date_date = new Date(Date.parse(this.props.candidate.birth_date))
                const birth_date_string = `${birth_date_date.getFullYear()}-${add_zero_padding((birth_date_date.getMonth()+1).toString())}-${add_zero_padding(birth_date_date.getDate().toString())}`

                this.birth_date = birth_date_string;
                this.birth_place = this.props.candidate.birth_place;
                this.phone = this.props.candidate.telephone;
                this.email = this.props.candidate.user.email;
                this.street = this.props.candidate.address.street;
                this.house_nr = this.props.candidate.address.house_nr;
                this.flat_nr = this.props.candidate.flat_nr;
                this.postal_code = this.props.candidate.address.postal_code;
                this.city = this.props.candidate.address.city;
                this.country = this.props.candidate.address.country;
            }
        }
    }

    render() {
        let check = null
        if (this.props.allow_addition_of_existing == true) {
            check = <Form.Check  size="sm" style={{ display: 'flex' }} checked={this.state.add_existing_checked} onChange={(event) => this.on_change(event.target.checked)} label="Dodaj istniejącego kandydata" type="switch" />
        }

        let form = <Form.Select  size="sm" aria-label="Candidate select" defaultValue={null} onChange={(e)=>this.on_select(e.target.value)}>
            {
                this.state.candidates?.map((candidate) => {

                    return (
                        <option value={candidate.id}>{candidate.user.first_name} {candidate.user.last_name}</option>
                    )

                })
            }
        </Form.Select>
        if (this.state.add_existing_checked == false) {
            form = <Form>
                <Row>
                    <Form.Group as={Col} className="mb-3" controlId="formLogin">
                        <Form.Label>Login</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.user.username} onChange={(event) => { this.login = event.target.value }}></Form.Control>
                    </Form.Group>
                </Row>

                <Row>
                    <Form.Group as={Col} className="mb-3" controlId="formName">
                        <Form.Label>Imię</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.user.first_name} onChange={(event) => { this.first_name = event.target.value }}></Form.Control>

                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="formLastName">
                        <Form.Label>Nazwisko</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.user.last_name} onChange={(event) => { this.last_name = event.target.value }}></Form.Control>

                    </Form.Group>
                </Row>

                <Row>
                    <Form.Group as={Col} className="mb-3" controlId="formBirthDate">
                        <Form.Label>Data urodzenia</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.birth_date} type="date" onChange={(event) => { this.birth_date = event.target.value }}></Form.Control>

                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="formBirthPlace">
                        <Form.Label>Miejsce urodzenia</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.birth_place} onChange={(event) => { this.birth_place = event.target.value }}></Form.Control>

                    </Form.Group>
                </Row>

                <Row>

                    <Form.Group as={Col} className="mb-3" controlId="formPhone">
                        <Form.Label>Telefon</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.telephone} onChange={(event) => { this.phone = event.target.value }}></Form.Control>

                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="formEmail">
                        <Form.Label>E-mail</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.user.email} type="email" onChange={(event) => { this.email = event.target.value }}></Form.Control>

                    </Form.Group>

                </Row>


                <Row>
                    <Form.Group as={Col} className="mb-3" controlId="formStreet">
                        <Form.Label>Ulica</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.address.street} onChange={(event) => { this.street = event.target.value }}></Form.Control>

                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="formHouse">
                        <Form.Label>Numer domu</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.address.house_nr}  onChange={(event) => { this.house_nr = event.target.value }}></Form.Control>

                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="formFlat">
                        <Form.Label>Numer lokalu</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.address.flat_nr}  onChange={(event) => { this.flat_nr = event.target.value }}></Form.Control>

                    </Form.Group>

                </Row>


                <Row>

                    <Form.Group as={Col} className="mb-3" controlId="formPostalCode">
                        <Form.Label>Kod pocztowy</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.address.postal_code}  onChange={(event) => { this.postal_code = event.target.value }}></Form.Control>

                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="formCity">
                        <Form.Label>Miasto</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.address.city}  onChange={(event) => { this.city = event.target.value }}></Form.Control>

                    </Form.Group>


                    <Form.Group as={Col} className="mb-3" controlId="formCountry">
                        <Form.Label>Kraj</Form.Label>
                        <Form.Control size="sm" defaultValue={this.props.candidate?.address.country}  onChange={(event) => { this.country = event.target.value }}></Form.Control>

                    </Form.Group>
                </Row>
            </Form>
        }

        return (
            <Modal show={this.props.show} onHide={() => { this.props.onHide?.(); this.clear() }} >
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form>
                        {check}
                    </Form>
                    {form}
                </Modal.Body >

                <Modal.Footer>
                    <Button variant="outline-secondary" size="sm" onClick={() => { this.props.onHide?.(); this.clear() }}>Anuluj</Button>
                    <Button variant="outline-success" size="sm" onClick={() => {
                        if (this.state.add_existing_checked == false) {
                            this.props.onSave?.(this.get_new_candidate());
                        } else {
                            this.props.onSaveExisting?.(this.state.selected_candidate)
                        }
                        this.props.onHide?.();
                        this.clear();
                    }}>Zapisz</Button>
                </Modal.Footer>
            </Modal >
        )
    }
}