import React, {Component} from 'react'
import {Card} from 'react-bootstrap'
import { RecruitmentInterviewAPI } from '../API/models_api.js'

export class MeetingDateView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            date: null,
            hour: null
        }
    }

    get_data = async () => {
        let meeting = await RecruitmentInterviewAPI.getForProcessAndCandidate(this.props.process.id, this.props.candidate.id);
        meeting = meeting.interview
        if (meeting) {
            this.setState(() => {
                return {
                    date: meeting.day,
                    hour: meeting.hour
                }
            })
        } else {
            this.setState(() => {
                return {
                    date: null,
                    hour: null
                }
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.candidate != this.props.candidate || prevProps.process != this.props.process) {
            this.get_data()
        }
    }

    render() {
        const style = {
            fontWeight: 'bold'
        }

        let info = <p>Nie umówiono jeszcze spotkania.</p>
        if (this.state.date && this.state.hour) {
            info = <p>
                    <span style={style}>Data spotkania:</span> {this.state.date}<br />
                    <span style={style}>Godzina spotkania:</span> {this.state.hour}<br />
                </p>
        }
        return (
            <Card>
                <Card.Header>Spotkanie rekrutacyjne</Card.Header>
                <Card.Body>
                    <Card.Title>Informacje</Card.Title>
                        {info}
                </Card.Body>
            </Card>
        )
    }
}