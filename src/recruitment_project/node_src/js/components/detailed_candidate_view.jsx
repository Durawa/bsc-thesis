import React, { Component } from 'react'
import { Card, Button } from 'react-bootstrap'
import {HR_EMPLOYEE_HIGHLIGHT} from './../constants.js'
import './style.css';
import {QuestionModal} from './question_modal.jsx'
import { CandidateAPI, RecruiterCandidateBindingAPI, RecruitmentInterviewAPI } from '../API/models_api.js';
import './style.css'
import { NewEditCandidateModal } from './new_edit_candidate_modal.jsx';
import { DateModal } from './date_modal.jsx';
import {convert_time} from './../common_functions.jsx'

export class DetailedCandidateView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show_modal: false,
            show_edition_modal: false,
            meeting: null,
            show_meeting_modal: false
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.candidate != prevProps.candidate){
            this.get_data()
        }
    }

    get_data = async  () => {
        if (this.props.candidate && this.props.process) {
            let interview = await RecruitmentInterviewAPI.getForProcessAndCandidate(this.props.process, this.props.candidate.id)
            interview = interview.interview
            this.setState(() => {
                return {
                    meeting: interview
                }
            })
        }
    }

    on_edit_modal_hide = () => {
        this.setState(() => {
            return {
                show_edition_modal: false
            }
        })
    }

    on_save_meeting = (date, hour) => {
        const datetime = `${date} ${convert_time(hour)}`
        RecruitmentInterviewAPI.http_post({
            candidate: this.props.candidate,
            date: datetime,
            process: this.props.process
        }, () => {
            this.get_data()
        }, (error) => {
            alert(error)
        })
    }

    cancel_meeting = () => {
        RecruitmentInterviewAPI.http_delete(this.state.meeting, () => {
            this.get_data()
        }, () => {alert('Wewnętrzny błąd serwera!')})
    }

    render() {
        const style = {
            fontWeight: 'bold'
        }

        let email = null;
        if (this.props.candidate?.user.email != '') {
            email = <li>
                        <span style={style}>E-mail</span><br/>{this.props.candidate?.user.email}
                    </li>
        }

        let assigned_info = null;
        let assign_recruiter_btn = <Button style={{float: 'right'}} variant="outline-success" size="sm" onClick={() => this.props.assign_to_recruiter?.(this.props.candidate)}>Przypnij do rekrutera</Button>
        if (this.props.recruiter != null) {
            assign_recruiter_btn = <Button style={{float: 'right'}} variant="outline-danger" size="sm" onClick={() => this.props.remove_from_recruiter?.(this.props.candidate)}>Odepnij od rekrutera</Button>
            assigned_info = <p>Przypięty do rekrutera <span style={{fontWeight: 'bold'}}>{this.props.recruiter.user.first_name} {this.props.recruiter.user.last_name}</span></p>
        }

        let footer_div = null;
        if (this.props.enable_assigning_to_and_removing_from_recruiter != false) {
            footer_div = <><hr/><div className="components_title_div"><p>Nieprzypięty do rekrutera</p>{assign_recruiter_btn}</div></>;
            if (this.props.recruiter != null) {
                footer_div = <><hr/><div className="components_title_div">
                    {assigned_info}
                    {assign_recruiter_btn}
                </div></>
            }
        } else if (this.props.enable_meetings != false) {
            let meeting_date = <p>Nie umówiono jeszcze spotkania.</p>;
            if (this.state.meeting?.date) {
                if (this.state.meeting?.is_past) {
                    meeting_date = <p>Spotkanie rekrutacyjne odbyło się dnia <span style={{fontWeight: 'bold'}}>{this.state.meeting.date}</span></p>
                } else {
                    meeting_date = <p>Spotkanie rekrutacyjne dnia <span style={{fontWeight: 'bold'}}>{this.state.meeting.date}</span></p>
                }
            }
            let btn = <Button onClick={()=>this.setState(() => {return {show_meeting_modal: true}})} style={{float: 'right'}} variant="outline-success" size="sm">Umów spotkanie</Button>
            if (this.state.meeting?.date) {
                if (this.state.meeting?.is_past) {
                    btn = null
                } else {
                    btn = <Button onClick={this.cancel_meeting} style={{float: 'right'}} variant="outline-danger" size="sm">Odwołaj spotkanie</Button>
                }
            }
            footer_div = <><hr/><div className="components_title_div">
                    {meeting_date}
                    {btn}
                </div></>
        }

        let edit_btn = null;
        if (this.props.enable_edition != false) {
            edit_btn = <Button onClick={()=>this.setState(() => {return {show_edition_modal: true}})} style={{marginRight: '0.5rem'}} variant="outline-success" size="sm">Edytuj</Button>
        }

        let delete_btn = null;
        if (this.props.enable_deletion != false) {
            delete_btn = <Button variant="outline-danger" size="sm" onClick={()=>this.setState(()=>{ return {show_modal: true}})}>Usuń</Button>
        }

        return (
            <>  <DateModal
                    show={this.state.show_meeting_modal}
                    title={"Wybierz datę spotkania"}
                    label={"Data spotkania"}
                    hour_label={"Godzina spotkania"}
                    enable_hour={true}
                    on_close={() => this.setState(() => {return {show_meeting_modal: false}})}
                    on_save={this.on_save_meeting.bind(this)}
                />
                <NewEditCandidateModal
                    candidates={null}
                    allow_addition_of_existing={false}
                    title={"Edycja kandydata"}
                    candidate={this.props.candidate}
                    show={this.state.show_edition_modal}
                    onHide={this.on_edit_modal_hide.bind(this)}
                    onSave={this.props.on_candidate_edited?.bind(this)}
                />
                <QuestionModal 
                    show={this.state.show_modal}
                    heading={"Usuwanie kandydata z procesu"}
                    text={"Czy na pewno chcesz usunąć wybranego kandydata z procesu?"}
                    cancel_text={"Anuluj"}
                    ok_text={"OK"}
                    on_close_clicked={() => this.setState(() => {return {show_modal: false}})}
                    on_ok_clicked={() => {
                        this.props.on_delete_clicked?.(this.props.candidate)
                        this.setState(() => {return {show_modal: false}})
                    }}
                />
                <Card>
                    <Card.Header style={{ backgroundColor: this.props.highlight }}>Szczegóły o kandydacie</Card.Header>
                    <Card.Body>
                        <div className="components_title_div">
                            <Card.Title>{this.props.candidate?.user.first_name} {this.props.candidate?.user.last_name}</Card.Title>
                            <div style={{display: 'flex'}}>
                                {edit_btn}
                                {delete_btn}
                            </div>
                        </div>
                        <hr/>
                        <Card.Text>
                            <div>
                            <h6>Dane personalne</h6>
                            <ul style={{listStyle: 'none', marginTop: '0.1rem'}}>
                                <li>
                                    <span style={style}>Adres</span><br/>{this.props.candidate?.address_str}
                                </li>
                                <li>
                                    <span style={style}>Telefon</span><br/>{this.props.candidate?.telephone}
                                </li>
                                {email}
                                <li>
                                    <span style={style}>Data urodzenia</span><br/>{this.props.candidate?.birth_date}
                                </li>
                                <li>
                                    <span style={style}>Miejsce urodzenia</span><br/>{this.props.candidate?.birth_place}
                                </li>
                            </ul>
                            </div>
                            {footer_div}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </>
        )
    }
}