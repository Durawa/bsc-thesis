import React, { Component } from 'react';
import { Table, Card, Button } from 'react-bootstrap';
import {
    HR_EMPLOYEE_KEY,
    RECRUITER_KEY,
    PROJECT_MANAGER_KEY,
    CANDIDATE_KEY,
    HR_EMPLOYEE_HIGHLIGHT,
    RECRUITER_HIGHLIGHT,
    CANDIDATE_HIGHLIGHT
} from './../constants.js'
import './style.css'
import {ProcessModal} from './process_modal.jsx'



export class RecruitmentProcessView extends Component {
    highlight = ''
    
    constructor(props) {
        super(props);

        if (this.props.mode ==  HR_EMPLOYEE_KEY) {
            this.highlight = HR_EMPLOYEE_HIGHLIGHT
        } else if (this.props.mode == RECRUITER_KEY) {
            this.highlight = RECRUITER_HIGHLIGHT
        } else if (this.props.mode == CANDIDATE_KEY) {
            this.highlight = CANDIDATE_HIGHLIGHT
        }
        this.state = {
            show: false
        }
    }

    render() {
        let title = <Card.Title>Procesy</Card.Title>
        if (this.props.mode == HR_EMPLOYEE_KEY) {
            title = <div className="components_title_div">
                <Card.Title>Procesy</Card.Title>
                <Button onClick={()=>this.setState({show: true})} variant="outline-success" size="sm">Dodaj</Button>
            </div>
        }
        if (this.props.processes) {
            return (
                <>
                <ProcessModal
                    show={this.state.show}
                    on_hide={()=>this.setState({show: false})}
                    heading={"Dodawanie procesu"}
                    post_submit={this.props.post_add_process?.bind(this)}
                />
                <Card>
                    <Card.Body>
                        {title}
                        <Table hover size="sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Stanowisko</th>
                                    <th>Data</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.props.processes?.map((process, idx) => {
                                        let chosenStyle = (idx == this.props.chosen_process) ? { backgroundColor: this.highlight, cursor: 'pointer' } : { cursor: 'pointer' };
                                        return (
                                            <tr key={idx} style={chosenStyle} onClick={() => {
                                                this.props.on_chosen_process_changed?.(process,  idx);
                                            }}>
                                                <td>{idx + 1}</td>
                                                <SingleProcess data={process} />
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </Table>
                    </Card.Body>
                </Card>
                </>
            )
        } else {
            return null;
        }
    }
}

class SingleProcess extends Component {
    render() {
        return (
            <>
                <td>{this.props.data.job_position_name}</td>
                <td>{this.props.data.deadline}</td>
            </>
        )
    }
}