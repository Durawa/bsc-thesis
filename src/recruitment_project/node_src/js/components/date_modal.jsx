import React, {Component} from 'react'
import {Modal, Form, Button, Col} from 'react-bootstrap'
import TimePicker from 'react-bootstrap-time-picker'

export class DateModal extends Component {
    date;

    constructor(props) {
        super(props)

        this.state = {time: 28800};
    }

    handleTimeChange = (time) => {
        this.setState(() => {
            return {
                time: time
            }
        })
    }

    on_close = () => {
        this.date = null;
        this.props.on_close?.()
    }

    on_save = () => {
        if (this.date) {
            if (this.props.enable_hour == true) {
                this.props.on_save?.(this.date, this.state.time)
            } else {
                this.props.on_save?.(this.date)
            }
            this.props.on_close?.()
        } else {
            alert('Podaj datę!')
        }
    }

    render() {
        let time_picker = null;
        if (this.props.enable_hour == true) {
            time_picker = <Form.Group className="mb-3" as={Col}>
                            <Form.Label>{this.props.hour_label}</Form.Label>
                            <TimePicker onChange={this.handleTimeChange} value={this.state.time} start="8:00" end="16:00" step={30} />
                          </Form.Group>
        }
        return (
            <Modal show={this.props.show} onHide={this.on_close}>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" as={Col}>
                            <Form.Label>{this.props.label}</Form.Label>
                            <Form.Control size="sm" type="date" onChange={(e) => this.date=e.target.value}></Form.Control>
                        </Form.Group> 
                        {time_picker}
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary" onClick={this.on_close}>
                        Anuluj
              </Button>
                    <Button variant="outline-success" onClick={this.on_save}>
                        Zapisz
              </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}