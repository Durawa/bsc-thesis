import React, {Component} from 'react'
import {Form, Button, Modal} from 'react-bootstrap'
import { JobPositionAPI, RecruitmentProcessAPI } from '../API/models_api'

export class ProcessModal extends Component {
    constructor(props){
        super(props)
        this.state = {
            positions: null,
            job_position: null,
            deadline: null
        }
        this.get_data()
    }

    get_data = async () => {
        const positions = await JobPositionAPI.http_get()
        this.setState({
            positions: positions.job_positions,
            job_position: positions.job_positions[0].id
        })
    }

    on_submit = (event) => {
        event.preventDefault()
        RecruitmentProcessAPI.http_post({
            job_position: this.state.job_position,
            deadline: this.state.deadline
        }, () => {
            alert('Poprawnie utworzono proces!')
            this.props.post_submit?.()
            this.props.on_hide?.()
        }, () => {
            alert('Wewnętrzny błąd serwera!')
            this.props.on_hide?.()
        })
    }

    handle_change = (event) => {
        const target = event.target;
        const name = target.name;
        const value = (name=='position') ? parseInt(target.value) : target.value
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.on_hide}>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.heading}</Modal.Title>
                </Modal.Header>
                <Form onSubmit={this.on_submit}>
                    <Modal.Body>
                        <Form.Group className="mb-3">
                            <Form.Label>Stanowisko</Form.Label>
                            <Form.Select onChange={this.handle_change} value={this.state.position} name="position">
                                {
                                    this.state.positions?.map(position => {
                                        return (
                                            <option value={position.id}>{position.experience_stage.name} {position.language.name} {position.position_type.name}</option>
                                        )
                                    })
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Data zakończenia procesu</Form.Label>
                            <Form.Control onChange={this.handle_change} type="date" value={this.state.date} name="deadline" />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="outline-secondary" size="sm">Anuluj</Button>
                        <Button variant="outline-success" type="submit" size="sm">Zapisz</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        )
    }
}