import React, { Component } from 'react'
import { Card, Form, Button, Col, Container, Row, Modal, ButtonGroup } from 'react-bootstrap'
import { OpenQuestionPointsAPI, TestAPI, WrittenTestAPI } from './../API/models_api'
import { RECRUITER_HIGHLIGHT } from './../constants.js'
import './style.css'
import {DateModal} from './date_modal.jsx'
import {to_multiline_paragraph} from './../common_functions.jsx'

export class GenerateOrDisplayTestView extends Component {
    closed = null;
    open = null;

    constructor(props) {
        super(props);
        this.state = {
            test: null,
            accepted: false,
            date: null,
            show_modal: false
        }
    }

    get_data = async () => {
        let test = await TestAPI.getForProcessAndRecruiter(this.props.process, this.props.recruiter.id)
        test = test?.test;
        this.setState(() => {
            return {
                test: test,
                accepted: (test) ? true : false
            }
        })
    }

    componentDidUpdate(prevProps) {
        if (prevProps.process != this.props.process) {
            this.get_data()
        }
    }

    generate_test = async () => {
        if (this.closed == null || this.open == null) {
            alert('Podaj liczbę pytań zamkniętych i otwartych!')
            return;
        }
        const test = await TestAPI.generate(this.props.process, this.closed, this.open, this.props.recruiter.id)
        this.setState(() => {
            return {
                test: test.test
            }
        })
    }

    accept_test = async (date) => {
        await TestAPI.http_post({
            ...this.state.test,
            date: date,
        }, (response) => {
            this.get_data()
        }, (error) => {
            alert(error)
        })
    }

    cancel_test = () => {
        TestAPI.http_delete({id: this.state.test.id}, () => {
            this.get_data()
        }, (error) => {
            alert(error)
        })
    }

    render() {
        const btn_text = (this.state.test) ? 'Wygeneruj ponownie' : 'Wygeneruj test'
        const btn_variant = (this.state.test) ? 'outline-danger' : 'outline-success'
        let gen_btn = <Button onClick={() => this.generate_test()} variant={btn_variant} size="sm">{btn_text}</Button>
        let body = <>
            <Form className="mb-3">
                <Form.Group as={Col}>
                    <Form.Label>Liczba pytań zamkniętych</Form.Label>
                    <Form.Control  size="sm" type="number" onChange={(e) => this.closed = e.target.value}></Form.Control>
                </Form.Group>

                <Form.Group as={Col}>
                    <Form.Label>Liczba pytań otwartych</Form.Label>
                    <Form.Control  size="sm" type="number" onChange={(e) => this.open = e.target.value}></Form.Control>
                </Form.Group>
            </Form>
            {gen_btn}
        </>
        if (this.state.test) {
            if (this.state.accepted == true) {
                if (this.state.test.is_past) {
                    body = <><TestView className="mb-3" test={this.state.test} /><hr/><div className="components_title_div"><p>Test odbył się <span style={{fontWeight: 'bold'}}>{this.state.test.date}</span></p></div></>
                } else {
                    body = <><TestView className="mb-3" test={this.state.test} /><hr/><div className="components_title_div"><p>Test odbędzie się <span style={{fontWeight: 'bold'}}>{this.state.test.date}</span></p><Button onClick={() => this.cancel_test()} variant="outline-danger" size="sm">Odwołaj test</Button></div></>
                }
            } else {
                const accept_btn = <Button onClick={() => this.setState(() => { return { show_modal: true } })} variant="outline-success" size="sm">Zaakceptuj</Button>
                body = <><TestView className="mb-3" test={this.state.test} /><hr/><div className="components_title_div">{gen_btn}{accept_btn}</div></>
            }
        }

        let test_date = null;
        if (this.state.accepted == true) {
            test_date = ` (${this.state.test.date})`
        }

        return (
            <>
            <DateModal
                show={this.state.show_modal}
                title="Akceptacja testu"
                label="Data testu"
                on_close={()=>this.setState(() => {return {show_modal: false}})}
                on_save={this.accept_test.bind(this)}
            />
            <Card>
                <Card.Header style={{ backgroundColor: RECRUITER_HIGHLIGHT }}>Test rekrutacyjny{test_date}</Card.Header>
                <Card.Body>
                    {body}
                </Card.Body>
            </Card>
            </>
        )
    }
}

export class WriteTestView extends Component {

    constructor(props) {
        super(props)
        this.state = {
            test: null,
            written: null
        }
    }

    get_data = async () => {
        let written_test = await WrittenTestAPI.getForProcessAndCandidate(this.props.process, this.props.candidate.id)
        written_test = written_test?.test
        if (!written_test) {
            let test = await TestAPI.getForProcessAndCandidate(this.props.process, this.props.candidate.id)
            test = test?.test;
            this.setState(() => {
                return {
                    test: test,
                    written: null
                }
            })
        } else {
            this.setState(() => {
                return {
                    test: null,
                    written: written_test
                }
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.process != prevProps.process) {
            this.get_data()
        }
    }

    submit_test = (test) => {
        WrittenTestAPI.http_post(test, () => {
            this.get_data()
        }, () => {
            alert('Wewnętrzny błąd serwera!');
        })
    }

    render() {
        let header = null;
        let title = null;
        let test = null;
        if (this.state.test) {
            header = "Test"
            if (this.state.test.info) {
                test = <p>{this.state.test.info}</p>
            } else {
                test = <TestView className="mb-3" mode="write" test={this.state.test} submit_test={this.submit_test.bind(this)} candidate={this.props.candidate} />
            }
        } else if (this.state.written) {
            header = "Statystyki testu"
            title = "Twój test"
            const style = {
                fontWeight: 'bold'
            }
            let open_result = <p>Pytania otwarte oczekują na sprawdzenie. Ogólny wynik testu jest zaniżony.</p>
            if (this.state.written.is_fully_checked) {
                open_result = <>
                    Ilość punktów z pytań otwartych: {this.state.written.open_points}/{this.state.written.max_open_points}<br/>
                    Procentowa ilość punktów z pytań otwartych: {this.state.written.open_points_percent}%<br/><br/>
                </>
            }
            test = <p>
                <p>
                    <span style={style}>Uzyskana ilość punktów: {this.state.written.points}</span><br/>
                    <span style={style}>Procentowa ilość punktów: {this.state.written.points_percent}%</span>
                </p>
                <hr/>
                Ilość punktów z pytań zamkniętych: {this.state.written.closed_points}/{this.state.written.max_closed_points}<br/>
                Procentowa ilość punktów z pytań zamkniętych: {this.state.written.closed_points_percent}%<br/><br/>
                {open_result}
            </p>
        } else {
            header = "Test"
            test = <p>Nie przypisano jeszcze testu rekrutacyjnego.</p>
        }
        return (
            <Card>
                <Card.Header>{header}</Card.Header>
                <Card.Body>
                    <Card.Title>{title}</Card.Title>
                    {test}
                </Card.Body>
            </Card>
        )
    }
}

export class TestView extends Component {
    answers;

    constructor(props) {
        super(props)
        this.answers = this.get_empty_answers()
    }

    get_empty_answers = () => {
        return {
            closed: {},
            open: {}
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.test != this.props.test) {
            this.answers = this.get_empty_answers()
        }
    }

    on_submit = (arg) => {
        arg.preventDefault()
        let test = JSON.parse(JSON.stringify(this.props.test));
        if (this.props.mode == 'write') {
            try {
                for (let i = 0; i < test.closed_test_questions.length; ++i) {
                    const id = test.closed_test_questions[i].id;
                    if (id in this.answers.closed) {
                        test.closed_test_questions[i].given_answer = this.answers.closed[id]
                    } else {
                        throw new Error(`Brak odpowiedzi na pytanie zamknięte nr. ${i+1}.`)
                    }
                }
    
                for (let i = 0; i < test.open_test_questions.length; ++i) {
                    const id = test.open_test_questions[i].id;
                    if (id in this.answers.open && this.answers.open[id] != "") {
                        test.open_test_questions[i].given_answer = this.answers.open[id]
                    } else {
                        throw new Error(`Brak odpowiedzi na pytanie otwarte nr. ${i+1}.`)
                    }
                }
                test.candidate = this.props.candidate
                this.props.submit_test?.(test)
            } catch (error) {
                alert(error)
            }  
        } else if (this.props.mode == 'check') {
            try {
                for (let i = 0; i < arg.target.elements.length-1; ++i) {
                    const points = parseInt(arg.target.elements[i].value)
                    test.open_test_questions[i].checked = true;
                    test.open_test_questions[i].given_points = points;
                }
                test.candidate = this.props.candidate
                this.props.submit_test?.(test)
            } catch (error) {
                alert('Błąd aplikacji. Wartości nie zostały wprowadzone do bazy danych.')
            }
        }
 
    }

    on_option_change = (question, option) => {
        this.answers['closed'][question.id] = option
    }

    on_open_change = (question, value) => {
        this.answers['open'][question.id] = value
    }

    render() {
        let accept_btn = null;
        if (this.props.mode == 'write' || this.props.mode == 'check') {
            accept_btn = <Button style={{float: 'right'}} variant="outline-success" type="submit" size="sm">Zatwierdź</Button>
        }
        return (
            <>
                <Form onSubmit={this.on_submit}>
                <p style={{ textAlign: 'center' }}>Pytania zamknięte</p>
                {
                    this.props.test.closed_test_questions.map((question, idx) => {
                        return (
                            <ClosedQuestionView 
                                mode={this.props.mode} 
                                on_new_option={this.on_option_change.bind(this)}
                                question={question} 
                                pos={idx + 1} 
                            />
                        )
                    })
                }
                <p style={{ textAlign: 'center' }}>Pytania otwarte</p>
                {
                    this.props.test.open_test_questions.map((question, idx) => {
                        return (
                            <OpenQuestionView 
                                mode={this.props.mode} 
                                on_change={this.on_open_change.bind(this)}
                                question={question} 
                                pos={idx + 1} 
                                process={this.props.test?.process}
                            />
                        )
                    })
                }
                {accept_btn}
                </Form>
            </>
        )
    }
}

export class ModalTestView extends Component {
    on_submit = (test) => {
        WrittenTestAPI.http_put(test, () => {
            this.props.on_close_clicked?.()
            alert('Poprawnie zaktualizowano test.')
            this.props.post_action?.()
        }, () => {
            alert('Wewnętrzny błąd serwera!')
        })
    }

    render() {
        return (
            <Modal  show={this.props.show} onHide={() => this.props.on_close_clicked?.()}>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.heading}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <TestView
                        mode={this.props.mode}
                        test={this.props.test}
                        candidate={this.props.candidate}
                        submit_test={this.on_submit}
                    /> 
                </Modal.Body>
            </Modal>
        )
    }
}

export class ClosedQuestionView extends Component {
    value;

    constructor(props){
        super(props)
    }

    set_option = (e) => {
        if (e.target.checked) {
            this.value = e.target.value
        }
        this.props.on_new_option?.(this.props.question, this.value)
    }

    render_write_mode = () => {
        const pos_and_question_text = `${this.props.pos}. ${this.props.question.question_text}`
        return (
            <Container className="mt-3">
                <Row>
                    <p style={{ fontWeight: 'bold' }}>{to_multiline_paragraph(pos_and_question_text)}</p>
                </Row>
                <Row>
                        <Form.Check  size="sm" style={{display: 'flex'}} onChange={this.set_option} name={`group${this.props.pos}`} type="radio" value='a' label={this.props.question.answer_a}/>
                        <Form.Check  size="sm" style={{display: 'flex'}} onChange={this.set_option} name={`group${this.props.pos}`} type="radio" value='b' label={this.props.question.answer_b}/>
                        <Form.Check  size="sm" style={{display: 'flex'}} onChange={this.set_option} name={`group${this.props.pos}`} type="radio" value='c' label={this.props.question.answer_c}/>
                        <Form.Check  size="sm" style={{display: 'flex'}} onChange={this.set_option} name={`group${this.props.pos}`} type="radio" value='d' label={this.props.question.answer_d}/>
                </Row>
            </Container>
        )
    }

    render_view_mode = () => {
        let a = <li>a) {this.props.question.answer_a}</li>
        let b = <li>b) {this.props.question.answer_b}</li>
        let c = <li>c) {this.props.question.answer_c}</li>
        let d = <li>d) {this.props.question.answer_d}</li>

        const correct_style = {
            color: 'green', fontWeight: 'bold'
        }

        const incorrect_style = {
            color: 'red', fontWeight: 'bold'
        }

        if (this.props.question.correct_answer == 'a') {
            a = <li><span style={correct_style}>a) {this.props.question.answer_a}</span></li>
        } else if (this.props.question.correct_answer == 'b') {
            b = <li><span style={correct_style}>b) {this.props.question.answer_b}</span></li>
        } else if (this.props.question.correct_answer == 'c') {
            c = <li><span style={correct_style}>c) {this.props.question.answer_c}</span></li>
        } else if (this.props.question.correct_answer == 'd') {
            d = <li><span style={correct_style}>d) {this.props.question.answer_d}</span></li>
        }

        if (this.props.mode == 'check' || this.props.mode == 'view') {
            let style = null;
            if (this.props.question.answer == 'a') {
                if (this.props.question.correct_answer == 'a') {
                    style = correct_style
                } else {
                    style = incorrect_style
                }
                a = <li><span style={style}>a) {this.props.question.answer_a}</span></li>
            } else if (this.props.question.correct_answer == 'b') {
                if (this.props.question.correct_answer == 'b') {
                    style = correct_style
                } else {
                    style = incorrect_style
                }
                b = <li><span style={style}>b) {this.props.question.answer_b}</span></li>
            } else if (this.props.question.correct_answer == 'c') {
                if (this.props.question.correct_answer == 'c') {
                    style = correct_style
                } else {
                    style = incorrect_style
                }
                c = <li><span style={style}>c) {this.props.question.answer_c}</span></li>
            } else if (this.props.question.correct_answer == 'd') {
                if (this.props.question.correct_answer == 'd') {
                    style = correct_style
                } else {
                    style = incorrect_style
                }
                d = <li><span style={style}>d) {this.props.question.answer_d}</span></li>
            }
        }

        const pos_and_question_text = `${this.props.pos}. ${this.props.question.question_text}`

        return (
            <Container className="mt-3">
                <Row>
                    <p style={{ fontWeight: 'bold' }}>{to_multiline_paragraph(pos_and_question_text)}</p>
                </Row>
                <Row>
                    <ul style={{ listStyle: 'none', marginLeft: '1rem' }}>
                        {a}
                        {b}
                        {c}
                        {d}
                    </ul>
                </Row>
            </Container>
        )
    }

    render() {
        if (this.props.mode == 'write') {
            return this.render_write_mode()
        } else {
            return this.render_view_mode()
        }
    }
}

class OpenQuestionView extends Component {
    value;

    constructor(props) {
        super(props)
        this.state = {
            max_points: null
        }
       // this.get_data()
    }

    get_data = async () => {
        if (this.props.question && this.props.process && this.props.mode) {
            if (this.props.mode=='write') {
                const points = await OpenQuestionPointsAPI.getForQuestionAndExperienceStage(this.props.question.id, this.props.process.job_position.experience_stage.id)
                this.setState(() => {
                    return {
                        max_points: points
                    }
                })
            } else if (this.props.mode=='check') {
                
            }
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.question != this.props.question || prevProps.process != this.props.process) {
          //  this.get_data()
        }
    }

    on_change = (event) => {
        this.value = event.target.value;
        this.props.on_change?.(this.props.question, this.value)
    }

    render() {
        let text_editor = null;
        if (this.props.mode=='write') {
            text_editor = <Form.Group className="mb-3">
                <Form.Label>Odpowiedź</Form.Label>
                <Form.Control  size="sm" onChange={this.on_change} as="textarea" rows={10}></Form.Control>
            </Form.Group>
        } else if (this.props.mode=='check' && this.props.question.max_points) {
            text_editor = <>
            <Container>
                {to_multiline_paragraph(this.props.question.answer)}
            </Container>
            <Form.Group className="mb-3">
                <Form.Label>Punkty</Form.Label>
                <Form.Select  size="sm" aria-label="Points select" defaultValue={null} onChange={(e)=>this.on_select?.(e.target.value)}>
                    {
                        Array.from(Array(this.props.question.max_points+1).keys()).map(points => {
                            return (
                                <option value={parseInt(points)}>{points}</option>
                            )
                        })
                    }
                </Form.Select>
            </Form.Group></>
        } else if (this.props.mode == 'view') {
            const lines = this.props.question.answer.split('\n')
            text_editor = <>
                <Container>
                    <p style={{whiteSpace: 'pre'}}>
                        {
                            lines.map(line => {
                                return (
                                    <>{line}<br/></>
                                )
                            })
                        }
                    </p>
                    <p>Przyznano za to zadanie <span style={{fontWeight: 'bold'}}>{this.props.question.points}</span>/{this.props.question.max_points} punkty.</p>
                </Container>
                </>
        }
        return (
            <Container className="mt-3">
                <Row>
                    <p style={{ fontWeight: 'bold' }}>{this.props.pos}. {this.props.question.question_text}</p>
                </Row>
                <Row>
                    {text_editor}
                </Row>
            </Container>
        )
    }
}

