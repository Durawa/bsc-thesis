import React, {Component} from 'react';
import { Modal, Button } from 'react-bootstrap';

export class QuestionModal extends Component {
    render() {
        return (
            <Modal show={this.props.show} onHide={() => this.props.on_close_clicked?.()}>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.heading}</Modal.Title>
                </Modal.Header>
                <Modal.Body>{this.props.text}</Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary" size="sm" onClick={() => this.props.on_close_clicked?.()}>
                        {this.props.cancel_text}
              </Button>
                    <Button variant="outline-success"size="sm" onClick={() => this.props.on_ok_clicked?.()}>
                        {this.props.ok_text}
              </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}