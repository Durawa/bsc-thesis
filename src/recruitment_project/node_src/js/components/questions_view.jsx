import React, { Component } from 'react'
import { Tabs, Tab, Form, Table, Button, Card, Modal, Container, Row, Col } from 'react-bootstrap'
import { ClosedTestQuestionAPI, JobExperienceStageAPI, JobPositionTypeAPI, OpenTestQuestionAPI, ProgrammingLanguageAPI } from '../API/models_api'
import { to_multiline_paragraph } from './../common_functions.jsx'
import { ClosedQuestionView } from './test_view.jsx'
import './style.css';
import { QuestionModal } from './question_modal.jsx'

export class QuestionsView extends Component {
    render() {
        return (
            <Tabs>
                <Tab eventKey="closed_questions" title="Pytania zamknięte">
                    <ClosedQuestionsViewWithFilter />
                </Tab>
                <Tab eventKey="open_questions" title="Pytania otwarte">
                    <OpenQuestionsViewWithFilter />
                </Tab>
            </Tabs>
        )
    }
}

class ClosedQuestionsViewWithFilter extends Component {
    constructor(props) {
        super(props)
        this.state = {
            experience_stage: null,
            language: null,
            position_type: null,
            filter: false
        }
    }

    on_filter_change = (state) => {
        this.setState(state)
    }

    render() {
        let experience_stage = null
        let language = null
        let position_type = null
        if (this.state.filter) {
            if (this.state.experience_stage && this.state.language && this.state.position_type) {
                experience_stage = this.state.experience_stage
                language = this.state.language
                position_type = this.state.position_type
            }
        }
        return (
            <>
                <TableFilter on_filter_change={this.on_filter_change.bind(this)}/>
                <ClosedQuestionsView 
                    experience_stage={experience_stage}
                    language={language}
                    position_type={position_type}
                />
            </>
        )
    }
}

class OpenQuestionsViewWithFilter extends Component {
    constructor(props) {
        super(props)
        this.state = {
            experience_stage: null,
            language: null,
            position_type: null,
            filter: false
        }
    }

    on_filter_change = (state) => {
        this.setState(state)
    }

    render() {
        let experience_stage = null
        let language = null
        let position_type = null
        if (this.state.filter) {
            if (this.state.experience_stage && this.state.language && this.state.position_type) {
                experience_stage = this.state.experience_stage
                language = this.state.language
                position_type = this.state.position_type
            }
        }
        return (
            <>
                <TableFilter on_filter_change={this.on_filter_change.bind(this)}/>
                <OpenQuestionsView 
                    experience_stage={experience_stage}
                    language={language}
                    position_type={position_type}
                />
            </>
        )
    }
}

class TableFilter extends Component {
    constructor(props) {
        super(props)
        this.state = {
            experience_stages: null,
            experience_stage: null,
            languages: null,
            language: null,
            positions: null,
            position_types: null,
            filter: false
        }
        this.get_data()
    }

    get_data = async () => {
        const experience_stages = await JobExperienceStageAPI.http_get()
        const languages = await ProgrammingLanguageAPI.http_get()
        const positions = await JobPositionTypeAPI.http_get()
        let can_proceed = true;
        if (experience_stages.job_experience_stages.length == 0) {
            alert('Brak poziomów doświadczenia w bazie danych!')
            can_proceed = false;
        }
        if (languages.programming_languages.length == 0) {
            alert('Brak języków programowania w bazie danych!')
            can_proceed = false;
        }
        if (positions.job_position_types.length == 0) {
            alert('Brak rodzajów stanowisk w bazie danych!')
            can_proceed = false;
        }
        if (can_proceed) {
            this.setState({
                experience_stages: experience_stages.job_experience_stages,
                languages: languages.programming_languages,
                positions: positions.job_position_types,
                experience_stage: experience_stages.job_experience_stages?.[0].id,
                language: languages.programming_languages?.[0].id,
                position_type: positions.job_position_types?.[0].id
            })
        } else {
            alert('Aplikacja nie będzie działała poprawnie dopóki nie zostanie uzupełniona baza danych!')
        }
    }

    handle_change = (event) => {
        const target = event.target;
        const name = target.name;
        let value = null
        if (['experience_stage', 'language', 'position_type'].includes(name)) {
            value = parseInt(target.value)
        } else {
            value = target.checked
            let state = JSON.parse(JSON.stringify(this.state))
            state.filter = value
            if (!state.filter) {
                state.experience_stage = null
                state.language = null
                state.position_type = null
            }
            this.props.on_filter_change?.(state)
        }

        if (name != 'filter') {
            if (this.state.filter) {
                let state = JSON.parse(JSON.stringify(this.state))
                state = {[name]: value}
                this.props.on_filter_change?.(state)
            }
        }
         
        this.setState({
            [name]: value
        })

    }

    render() {
        return (
            <Container className="m-3">
                <Row className="mb-3">
                    <Form.Group as={Col} className="mb-3" controlId="filter">
                        <Form.Label>Włącz filtry</Form.Label>
                        <Form.Check  size="sm" name="filter" onChange={this.handle_change} type="checkbox" value={this.state.filter}/>
                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="experienceBox">
                        <Form.Label>Poziom doświadczenia</Form.Label>
                        <Form.Select onChange={this.handle_change} name="experience_stage" size='sm' value={this.state.experience_stage}>
                            {
                                this.state.experience_stages?.map(experience_stage => {
                                    return (
                                        <option value={experience_stage.id}>{experience_stage.name}</option>
                                    )
                                })
                            }
                        </Form.Select>
                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="languageBox">
                        <Form.Label>Język programowania</Form.Label>
                        <Form.Select onChange={this.handle_change} name="language" size='sm' value={this.state.language}>
                            {
                                this.state.languages?.map(language => {
                                    return (
                                        <option value={language.id}>{language.name}</option>
                                    )
                                })
                            }
                        </Form.Select>
                    </Form.Group>

                    <Form.Group as={Col} className="mb-3" controlId="positionBox">
                        <Form.Label>Stanowisko</Form.Label>
                        <Form.Select onChange={this.handle_change} name="position_type" size='sm' value={this.state.position_type}>
                            {
                                this.state.positions?.map(position_type => {
                                    return (
                                        <option value={position_type.id}>{position_type.name}</option>
                                    )
                                })
                            }
                        </Form.Select>
                    </Form.Group>
                </Row>
            </Container>
        )
    }
}

export class ClosedQuestionsView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            questions: null,
            show_edit_modal: false,
            question_to_edit: null,
            show_remove_modal: false,
            question_to_remove: null,
            question_modal_mode: null
        }
        this.get_data();
    }

    get_data = async () => {
        if (this.props.experience_stage && this.props.language && this.props.position_type) {
            const questions = await ClosedTestQuestionAPI.getForExperienceStageAndLanguageAndPositionType(this.props.experience_stage, this.props.language, this.props.position_type)
            this.setState({
                questions: questions.closed_test_questions
            })
        } else {
            const questions = await ClosedTestQuestionAPI.http_get()
            this.setState({
                questions: questions.closed_test_questions
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps != this.props) {
            this.get_data()
        }
    }

    on_edit_question = (question) => {
        this.setState({
            show_edit_modal: true,
            question_to_edit: question,
            question_modal_mode: 'edit'
        })
    }

    on_new_question = () => {
        this.setState({
            show_edit_modal: true,
            question_modal_mode: 'new'
        })
    }

    on_remove_question = (question) => {
        this.setState({
            show_remove_modal: true,
            question_to_remove: question
        })
    }

    get_modal_title = () => {
        if (this.state.question_modal_mode == 'edit') {
            return 'Edycja pytania'
        } else if (this.state.question_modal_mode == 'new') {
            return 'Nowe pytanie'
        }
    }

    render() {
        return (
            <>
                <QuestionModal
                    show={this.state.show_remove_modal}
                    on_close_clicked={() => this.setState({ show_remove_modal: false, question_to_remove: null })}
                    heading={"Usuwanie pytania"}
                    text={"Czy na pewno chcesz usunąć pytanie?"}
                    cancel_text={"Anuluj"}
                    ok_text={"Usuń"}
                    on_ok_clicked={() => {
                        ClosedTestQuestionAPI.http_delete(this.state.question_to_remove, () => {
                            alert('Poprawnie usunięto pytanie')
                            this.get_data()
                            this.setState({
                                show_remove_modal: false,
                                question_to_remove: null
                            })
                        }, () => alert('Wewnętrzny błąd serwera!'))
                    }}
                />
                <ClosedQuestionNewEditModal
                    show={this.state.show_edit_modal}
                    handleClose={() => this.setState({ show_edit_modal: false, question_to_edit: null, question_modal_mode: null })}
                    title={this.get_modal_title()}
                    question={this.state.question_to_edit}
                    mode={this.state.question_modal_mode}
                    post_submit={this.get_data.bind(this)}
                />
                <Card>
                    <Card.Body>
                        <div className="components_title_div mt-3 mb-3">
                            <Card.Title>Wszystkie pytania</Card.Title>
                            <Button variant="outline-success" size="sm" onClick={this.on_new_question}>Nowe pytanie</Button>
                        </div>
                        <Table hover size="sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Pytanie</th>
                                    <th>Poziom</th>
                                    <th>Język</th>
                                    <th>Stanowisko</th>
                                    <th>Data utworzenia</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.questions?.map((question, idx) => {
                                        return (
                                            <QuestionRow
                                                idx={idx}
                                                question={question}
                                                on_edit_question={this.on_edit_question.bind(this)}
                                                on_remove_question={this.on_remove_question.bind(this)}
                                                mode="closed"
                                            />
                                        )
                                    })
                                }
                            </tbody>
                        </Table>
                    </Card.Body>
                </Card>
            </>
        )
    }
}

class OpenQuestionsView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            questions: null,
            show_edit_modal: false,
            question_to_edit: null,
            show_remove_modal: false,
            question_to_remove: null,
            question_modal_mode: null
        }
        this.get_data();
    }

    get_data = async () => {
        if (this.props.experience_stage && this.props.language && this.props.position_type) {
            const questions = await OpenTestQuestionAPI.getForExperienceStageAndLanguageAndPositionType(this.props.experience_stage, this.props.language, this.props.position_type)
            this.setState({
                questions: questions.open_test_questions
            })
        } else {
            const questions = await OpenTestQuestionAPI.http_get()
            this.setState({
                questions: questions.open_test_questions
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps != this.props) {
            this.get_data()
        }
    }

    on_edit_question = (question) => {
        this.setState({
            show_edit_modal: true,
            question_to_edit: question,
            question_modal_mode: 'edit'
        })
    }

    on_new_question = () => {
        this.setState({
            show_edit_modal: true,
            question_modal_mode: 'new'
        })
    }

    on_remove_question = (question) => {
        this.setState({
            show_remove_modal: true,
            question_to_remove: question
        })
    }

    get_modal_title = () => {
        if (this.state.question_modal_mode == 'edit') {
            return 'Edycja pytania'
        } else if (this.state.question_modal_mode == 'new') {
            return 'Nowe pytanie'
        }
    }

    close_new_edit_modal = () => {
        this.setState({ show_edit_modal: false, question_to_edit: null, question_modal_mode: null })
    }

    render() {
        return (
            <>
                <QuestionModal
                    show={this.state.show_remove_modal}
                    on_close_clicked={() => this.setState({ show_remove_modal: false, question_to_remove: null })}
                    heading={"Usuwanie pytania"}
                    text={"Czy na pewno chcesz usunąć pytanie?"}
                    cancel_text={"Anuluj"}
                    ok_text={"Usuń"}
                    on_ok_clicked={() => {
                        OpenTestQuestionAPI.http_delete(this.state.question_to_remove, () => {
                            alert('Poprawnie usunięto pytanie')
                            this.get_data()
                            this.setState({
                                show_remove_modal: false,
                                question_to_remove: null
                            })
                        }, () => alert('Wewnętrzny błąd serwera!'))
                    }}
                />
                <OpenQuestionNewEditModal
                    show={this.state.show_edit_modal}
                    handleClose={this.close_new_edit_modal}
                    title={this.get_modal_title()}
                    question={this.state.question_to_edit}
                    mode={this.state.question_modal_mode}
                    post_submit={this.get_data.bind(this)}
                />
                <Card>
                    <Card.Body>
                        <div className="components_title_div mt-3 mb-3">
                            <Card.Title>Wszystkie pytania</Card.Title>
                            <Button variant="outline-success" size="sm" onClick={this.on_new_question}>Nowe pytanie</Button>
                        </div>
                        <Table hover size="sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Pytanie</th>
                                    <th>Poziom</th>
                                    <th>Język</th>
                                    <th>Stanowisko</th>
                                    <th>Data utworzenia</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.questions?.map((question, idx) => {
                                        return (
                                            <QuestionRow
                                                idx={idx}
                                                question={question}
                                                on_edit_question={this.on_edit_question.bind(this)}
                                                on_remove_question={this.on_remove_question.bind(this)}
                                                mode="open"
                                            />
                                        )
                                    })
                                }
                            </tbody>
                        </Table>
                    </Card.Body>
                </Card>
            </>
        )
    }
}

class QuestionRow extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
    }

    on_edit_question = (e) => {
        e.stopPropagation()
        this.props.on_edit_question?.(this.props.question)
    }

    on_remove_question = (e) => {
        e.stopPropagation()
        this.props.on_remove_question?.(this.props.question)
    }

    handle_row_click = () => {
        if (this.props.mode == 'closed') {
            this.setState({show: true})
        }
    }

    render() {
        return (
            <>
                <ClosedQuestionModal
                    show={this.state.show}
                    handleClose={() => this.setState({ show: false })}
                    question={this.props.question}
                />
                <tr onClick={this.handle_row_click}>
                    <td>{this.props.idx + 1}</td>
                    <td>{to_multiline_paragraph(this.props.question.question_text)}</td>
                    <td>{this.props.question.experience_stage.name}</td>
                    <td>{this.props.question.language.name}</td>
                    <td>{this.props.question.position_type.name}</td>
                    <td>{this.props.question.creation_date}</td>
                    <td><Button onClick={(e) => this.on_edit_question(e)} variant="outline-success" size="sm">Edytuj</Button></td>
                    <td><Button onClick={(e) => this.on_remove_question(e)} variant="outline-danger" size="sm">Usuń</Button></td>
                </tr>
            </>
        )
    }
}

class ClosedQuestionModal extends Component {
    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Szczegóły pytania</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ClosedQuestionView
                        question={this.props.question}
                        pos={'#'}
                    />
                </Modal.Body>

            </Modal>
        )
    }
}

class ClosedQuestionNewEditModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: null,
            experience_stages: null,
            languages: null,
            positions: null,
            experience_stage: null,
            language: null,
            position_type: null,
            question_text: '',
            answer_a: '',
            answer_b: '',
            answer_c: '',
            answer_d: '',
            correct_answer: 'a'
        }
        this.get_data()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.question != this.props.question) {
            if (this.props.question && this.props.mode == 'edit') {
                this.setState({
                    id: this.props.question.id,
                    experience_stage: this.props.question.experience_stage.id,
                    language: this.props.question.language.id,
                    position_type: this.props.question.position_type.id,
                    question_text: this.props.question.question_text,
                    answer_a: this.props.question.answer_a,
                    answer_b: this.props.question.answer_b,
                    answer_c: this.props.question.answer_c,
                    answer_d: this.props.question.answer_d,
                    correct_answer: this.props.question.correct_answer
                })
            }
        }
    }

    get_data = async () => {
        const experience_stages = await JobExperienceStageAPI.http_get()
        const languages = await ProgrammingLanguageAPI.http_get()
        const positions = await JobPositionTypeAPI.http_get()
        let can_proceed = true;
        if (experience_stages.job_experience_stages.length == 0) {
            alert('Brak poziomów doświadczenia w bazie danych!')
            can_proceed = false;
        }
        if (languages.programming_languages.length == 0) {
            alert('Brak języków programowania w bazie danych!')
            can_proceed = false;
        }
        if (positions.job_position_types.length == 0) {
            alert('Brak rodzajów stanowisk w bazie danych!')
            can_proceed = false;
        }
        if (can_proceed) {
            this.setState({
                experience_stages: experience_stages.job_experience_stages,
                languages: languages.programming_languages,
                positions: positions.job_position_types,
                experience_stage: experience_stages.job_experience_stages?.[0].id,
                language: languages.programming_languages?.[0].id,
                position_type: positions.job_position_types?.[0].id
            })
        } else {
            alert('Aplikacja nie będzie działała poprawnie dopóki nie zostanie uzupełniona baza danych!')
        }
    }

    handle_change = (event) => {
        const target = event.target;
        const name = target.name;
        const value = (['experience_stage', 'language', 'position_type'].includes(name)) ? parseInt(target.value) : target.value
        this.setState({
            [name]: value
        })
    }

    on_submit = (event) => {
        event.preventDefault()
        if (this.props.mode == 'edit') {
            ClosedTestQuestionAPI.http_put(this.state, () => {
                alert('Poprawnie dokonano edycji pytania!')
                this.handle_close()
                this.props.post_submit?.()
            }, () => {
                alert('Wewnętrzny błąd serwera!')
                this.handle_close()
            })
        } else if (this.props.mode == 'new') {
            ClosedTestQuestionAPI.http_post(this.state, () => {
                alert('Utworzono nowe pytanie!')
                this.handle_close()
                this.props.post_submit?.()
            }, () => {
                alert('Wewnętrzny błąd serwera!')
                this.handle_close()
            })
        }
    }

    handle_close = () => {
        this.setState({
            id: null,
            experience_stage: this.state.experience_stages?.[0].id,
            language: this.state.languages?.[0].id,
            position_type: this.state.positions?.[0].id,
            question_text: '',
            answer_a: '',
            answer_b: '',
            answer_c: '',
            answer_d: '',
            correct_answer: 'a'
        })
        this.props.handleClose?.()
    }

    render() {
        return (
            <Modal show={this.props.show} onHide={this.handle_close}>
                <Form onSubmit={this.on_submit}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group className="mb-3" controlId="experienceBox">
                            <Form.Label>Poziom doświadczenia</Form.Label>
                            <Form.Select onChange={this.handle_change} name="experience_stage" size='sm' value={this.state.experience_stage}>
                                {
                                    this.state.experience_stages?.map(experience_stage => {
                                        return (
                                            <option value={experience_stage.id}>{experience_stage.name}</option>
                                        )
                                    })
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="languageBox">
                            <Form.Label>Język programowania</Form.Label>
                            <Form.Select onChange={this.handle_change} name="language" size='sm' value={this.state.language}>
                                {
                                    this.state.languages?.map(language => {
                                        return (
                                            <option value={language.id}>{language.name}</option>
                                        )
                                    })
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="positionBox">
                            <Form.Label>Stanowisko</Form.Label>
                            <Form.Select onChange={this.handle_change} name="position_type" size='sm' value={this.state.position_type}>
                                {
                                    this.state.positions?.map(position_type => {
                                        return (
                                            <option value={position_type.id}>{position_type.name}</option>
                                        )
                                    })
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="questionTextArea">
                            <Form.Label>Pytanie</Form.Label>
                            <Form.Control onChange={this.handle_change} name="question_text" size='sm' as="textarea" rows={5} value={this.state.question_text} />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="a_textField">
                            <Form.Label>Odpowiedź A</Form.Label>
                            <Form.Control onChange={this.handle_change} name="answer_a" size='sm' value={this.state.answer_a} />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="b_textField">
                            <Form.Label>Odpowiedź B</Form.Label>
                            <Form.Control onChange={this.handle_change} name="answer_b" size='sm' value={this.state.answer_b} />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="c_textField">
                            <Form.Label>Odpowiedź C</Form.Label>
                            <Form.Control onChange={this.handle_change} name="answer_c" size='sm' value={this.state.answer_c} />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="d_textField">
                            <Form.Label>Odpowiedź D</Form.Label>
                            <Form.Control onChange={this.handle_change} name="answer_d" size='sm' value={this.state.answer_d} />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="correctBox">
                            <Form.Label>Poprawna odpowiedź</Form.Label>
                            <Form.Select onChange={this.handle_change} name="correct_answer" size='sm' value={this.state.correct_answer}>
                                <option value='a'>A</option>
                                <option value='b'>B</option>
                                <option value='c'>C</option>
                                <option value='d'>D</option>
                            </Form.Select>
                        </Form.Group>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handle_close} variant="outline-secondary" size="sm">Anuluj</Button>
                        <Button variant="outline-success" type="submit" size="sm">Zapisz</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        )
    }
}

class OpenQuestionNewEditModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: null,
            experience_stages: null,
            languages: null,
            positions: null,
            experience_stage: null,
            language: null,
            position_type: null,
            question_text: ''
        }
        this.get_data()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.question != this.props.question) {
            if (this.props.question && this.props.mode == 'edit') {
                this.setState({
                    id: this.props.question.id,
                    experience_stage: this.props.question.experience_stage.id,
                    language: this.props.question.language.id,
                    position_type: this.props.question.position_type.id,
                    question_text: this.props.question.question_text
                })
            }
        }
    }

    get_data = async () => {
        const experience_stages = await JobExperienceStageAPI.http_get()
        const languages = await ProgrammingLanguageAPI.http_get()
        const positions = await JobPositionTypeAPI.http_get()
        let can_proceed = true;
        if (experience_stages.job_experience_stages.length == 0) {
            alert('Brak poziomów doświadczenia w bazie danych!')
            can_proceed = false;
        }
        if (languages.programming_languages.length == 0) {
            alert('Brak języków programowania w bazie danych!')
            can_proceed = false;
        }
        if (positions.job_position_types.length == 0) {
            alert('Brak rodzajów stanowisk w bazie danych!')
            can_proceed = false;
        }
        if (can_proceed) {
            this.setState(() => {
                return {
                    experience_stages: experience_stages.job_experience_stages,
                    languages: languages.programming_languages,
                    positions: positions.job_position_types,
                    experience_stage: experience_stages.job_experience_stages?.[0].id,
                    language: languages.programming_languages?.[0].id,
                    position_type: positions.job_position_types?.[0].id
                }
            })
        } else {
            alert('Aplikacja nie będzie działała poprawnie dopóki nie zostanie uzupełniona baza danych!')
        }
    }

    handle_change = (event) => {
        const target = event.target;
        const name = target.name;
        const value = (['experience_stage', 'language', 'position_type'].includes(name)) ? parseInt(target.value) : target.value
        this.setState({
            [name]: value
        })
    }

    on_submit = (event) => {
        event.preventDefault()
        if (this.props.mode == 'edit') {
            OpenTestQuestionAPI.http_put(this.state, () => {
                alert('Poprawnie dokonano edycji pytania!')
                this.handle_close()
                this.props.post_submit?.()
            }, () => {
                alert('Wewnętrzny błąd serwera!')
                this.handle_close()
            })
        } else if (this.props.mode == 'new') {
            OpenTestQuestionAPI.http_post(this.state, () => {
                alert('Utworzono nowe pytanie!')
                this.handle_close()
                this.props.post_submit?.()
            }, () => {
                alert('Wewnętrzny błąd serwera!')
                this.handle_close()
            })
        }
    }

    handle_close = () => {
        this.setState(() => {
            return {
                id: null,
                experience_stage: this.state.experience_stages?.[0].id,
                language: this.state.languages?.[0].id,
                position_type: this.state.positions?.[0].id,
                question_text: ''
            }
        })
        this.props.handleClose?.()
    }

    render() {
        return (
            <Modal show={this.props.show} onHide={this.handle_close}>
                <Form onSubmit={this.on_submit}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group className="mb-3" controlId="experienceBox">
                            <Form.Label>Poziom doświadczenia</Form.Label>
                            <Form.Select onChange={this.handle_change} name="experience_stage" size='sm' value={this.state.experience_stage}>
                                {
                                    this.state.experience_stages?.map(experience_stage => {
                                        return (
                                            <option value={experience_stage.id}>{experience_stage.name}</option>
                                        )
                                    })
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="languageBox">
                            <Form.Label>Język programowania</Form.Label>
                            <Form.Select onChange={this.handle_change} name="language" size='sm' value={this.state.language}>
                                {
                                    this.state.languages?.map(language => {
                                        return (
                                            <option value={language.id}>{language.name}</option>
                                        )
                                    })
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="positionBox">
                            <Form.Label>Stanowisko</Form.Label>
                            <Form.Select onChange={this.handle_change} name="position_type" size='sm' value={this.state.position_type}>
                                {
                                    this.state.positions?.map(position_type => {
                                        return (
                                            <option value={position_type.id}>{position_type.name}</option>
                                        )
                                    })
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="questionTextArea">
                            <Form.Label>Pytanie</Form.Label>
                            <Form.Control onChange={this.handle_change} name="question_text" size='sm' as="textarea" rows={5} value={this.state.question_text} />
                        </Form.Group>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handle_close} variant="outline-secondary" size="sm">Anuluj</Button>
                        <Button variant="outline-success" type="submit" size="sm">Zapisz</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        )
    }
}