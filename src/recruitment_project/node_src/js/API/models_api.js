import { http_get, http_post, http_put, http_delete } from './api.js'

class BaseAPI {
    static async http_get(id = null) {
        if (id == null) {
            return http_get(`${this.model_name()}/get/`);
        }
        return http_get(`${this.model_name()}/get/${id}/`);
    }

    static async http_post(object, on_success = null, on_failure = null) {
        return http_post(`${this.model_name()}/post/`, object, on_success, on_failure);
    }

    static async http_put(object, on_success = null, on_failure = null) {
        return http_put(`${this.model_name()}/put/`, object, on_success, on_failure);
    }

    static async http_delete(object, on_success = null, on_failure = null) {
        return http_delete(`${this.model_name()}/delete/`, object, on_success, on_failure);
    }

    static model_name() {
        throw new Error('You cannot call this function');
    }
}

export class UserAPI extends BaseAPI {
    static model_name() {
        return 'User';
    }

    static async http_post(object, on_success = null, on_failure = null) {
        throw new Error('You cannot call this function');
    }

    static async http_put(object, on_success = null, on_failure = null) {
        throw new Error('You cannot call this function');
    }

    static async http_delete(object, on_success = null, on_failure = null) {
        throw new Error('You cannot call this function');
    }
}

export class AddressAPI extends BaseAPI {
    static model_name() {
        return 'Address';
    }
}

export class HREmployeeAPI extends BaseAPI {
    static model_name() {
        return 'HREmployee';
    }
}

export class RecruiterAPI extends BaseAPI {
    static async get_for_process(id) {
        return http_get(`${this.model_name()}/get/RecruitmentProcess/${id}`);
    }

    static model_name() {
        return 'Recruiter';
    }
}

export class ProjectManagerAPI extends BaseAPI {
    static model_name() {
        return 'ProjectManager';
    }
}

export class CandidateAPI extends BaseAPI {
    static async get_for_process(id) {
        return http_get(`${this.model_name()}/get/RecruitmentProcess/${id}/`);
    }

    static model_name() {
        return 'Candidate';
    }
}

export class JobExperienceStageAPI extends BaseAPI {
    static model_name() {
        return 'JobExperienceStage';
    }
}

export class ProgrammingLanguageAPI extends BaseAPI {
    static model_name() {
        return 'ProgrammingLanguage';
    }
}

export class JobPositionTypeAPI extends BaseAPI {
    static model_name() {
        return 'JobPositionType';
    }
}

export class JobPositionAPI extends BaseAPI {
    static model_name() {
        return 'JobPosition';
    }
}

export class RecruitmentProcessAPI extends BaseAPI {
    static async getForHREmployee(id) {
        return http_get(`${this.model_name()}/get/HREmployee/${id}/`);
    }

    static async getForRecruiter(id) {
        return http_get(`${this.model_name()}/get/Recruiter/${id}/`)
    }

    static async getForCandidate(id) {
        return http_get(`${this.model_name()}/get/Candidate/${id}/`)
    }

    static model_name() {
        return 'RecruitmentProcess';
    }
}

export class RecruiterCandidateBindingAPI extends BaseAPI {
    static async getFoRecruiterAndProcess(recruiter, process) {
        return http_get(`${this.model_name()}/get/Recruiter/${recruiter}/RecruitmentProcess/${process}/`);
    }

    static async getForCandidateAndProcess(candidate, process) {
        return http_get(`${this.model_name()}/get/Candidate/${candidate}/RecruitmentProcess/${process}/`);
    }

    static model_name() {
        return 'RecruiterCandidateBinding';
    }
}

export class ClosedTestQuestionAPI extends BaseAPI {
    static async getForExperienceStageAndLanguageAndPositionType(experience, language, position_type) {
        return http_get(`${this.model_name()}/get/JobExperienceStage/${experience}/ProgrammingLanguage/${language}/JobPositionType/${position_type}`)
    }

    static model_name() {
        return 'ClosedTestQuestion';
    }
}

export class ClosedQuestionPointsAPI extends BaseAPI {
    static model_name() {
        return 'ClosedQuestionPoints';
    }
}

export class OpenTestQuestionAPI extends BaseAPI {
    static async getForExperienceStageAndLanguageAndPositionType(experience, language, position_type) {
        return http_get(`${this.model_name()}/get/JobExperienceStage/${experience}/ProgrammingLanguage/${language}/JobPositionType/${position_type}`)
    }

    static model_name() {
        return 'OpenTestQuestion';
    }
}

export class OpenQuestionPointsAPI extends BaseAPI {
    static async getForQuestionAndExperienceStage(question, experience) {
        return http_get(`${this.model_name()}/get/OpenTestQuestion/${question}/JobExperienceStage/${experience}/`)
    }

    static model_name() {
        return 'OpenQuestionPoints';
    }
}

export class AnsweredOpenQuestionAPI extends BaseAPI {
    static model_name() {
        return 'AnsweredOpenQuestion'
    }
}


export class TestAPI extends BaseAPI {
    static async generate(process, closed, open, recruiter) {
        return http_get(`${this.model_name()}/generate/RecruitmentProcess/${process}/${closed}/${open}/Recruiter/${recruiter}/`)
    }

    static async getForProcessAndRecruiter(process, recruiter){
        return http_get(`${this.model_name()}/get/RecruitmentProcess/${process}/Recruiter/${recruiter}/`)
    }

    static async getForProcessAndCandidate(process, candidate) {
        return http_get(`${this.model_name()}/get/RecruitmentProcess/${process}/Candidate/${candidate}/`)
    }

    static model_name() {
        return 'Test';
    }
}

export class WrittenTestAPI extends BaseAPI {
    static async getForProcessAndCandidate(process, candidate) {
        return http_get(`${this.model_name()}/get/RecruitmentProcess/${process}/Candidate/${candidate}/`)
    }

    static model_name() {
        return 'WrittenTest';
    }
}

export class RecruitmentInterviewAPI extends BaseAPI {
    static async getForProcessAndCandidate(process, candidate) {
        return http_get(`${this.model_name()}/get/RecruitmentProcess/${process}/Candidate/${candidate}/`)
    }

    static model_name() {
        return 'RecruitmentInterview';
    }
}

export class InterviewNotesAPI extends BaseAPI {
    static model_name() {
        return 'InterviewNotes';
    }
}

export class JobOfferAPI extends BaseAPI {
    static model_name() {
        return 'JobOffer';
    }
}