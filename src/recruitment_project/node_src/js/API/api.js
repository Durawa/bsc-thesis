import { getCsrfToken } from './../csrf.js'

export async function http_get(endpoint) {
    const response = await fetch(endpoint);
    return response.json();
}

export async function http_post(endpoint, object, on_success = null, on_failure = null) {
    methodCall('POST', endpoint, object, on_success, on_failure)
}

export async function http_put(endpoint, object, on_success = null, on_failure = null) {
    methodCall('PUT', endpoint, object, on_success, on_failure)
}

export async function http_delete(endpoint, object, on_success = null, on_failure = null) {
    methodCall('DELETE', endpoint, object, on_success, on_failure)
}

async function methodCall(method, endpoint, object, on_success = null, on_failure = null) {
    fetch(endpoint, {
        method: method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRFToken': getCsrfToken()
        },
        body: JSON.stringify(object)
    }).then(response => {
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response;
    }).then(response => {
        on_success?.(response)
    }).catch(error => {
        on_failure?.(error);
    })
}