import React, { Component } from 'react'
import { RecruitmentProcessView } from './components/recruitment_process_view.jsx';
import { CandidatesView } from './components/candidates_view.jsx';
import { RecruitersView } from './components/recruiters_view.jsx';
import { DetailedCandidateView } from './components/detailed_candidate_view.jsx';
import { Container, Row, Col, Card } from 'react-bootstrap'
import { HR_EMPLOYEE_KEY, HR_EMPLOYEE_HIGHLIGHT } from './constants.js'
import { UserAPI, RecruitmentProcessAPI, CandidateAPI, RecruiterAPI, AddressAPI, RecruiterCandidateBindingAPI } from './API/models_api.js'
import {
    get_candidates_for_recruiter_from_process,
    get_processes_for_hremployee,
    get_recruiters_for_process,
    get_candidates_for_process,
    find_element
} from './common_functions.jsx'

const selection_mode = {
    all: 'all',
    of_recruiter: 'of_recruiter'
}


export class HREmployeeView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            processes: null,
            chosen_process: null,
            process_id: null,
            candidates: null,
            chosen_candidate: null,
            candidate_id: null,
            candidates_recruiter: null,
            chosen_candidate_recruiter: null,
            cadndidate_recruiter_id: null,
            recruiters: null,
            chosen_recruiter: null,
            recruiter_id: null,
            recruiter_for_candidate: null
        }
        this.get_data();
    }

    get_data = async () => {
        const user = await UserAPI.http_get()
        const processes_obj = await this.get_processes(user.id)
        const candidates_obj = await this.get_candidates_from_process(processes_obj.process_id);
        const recruiters_obj = await this.get_recruiters_from_process(processes_obj.process_id);
        const candidates_for_recruiter_obj = await get_candidates_for_recruiter_from_process(recruiters_obj.recruiters?.[0].id, processes_obj.process_id)
        this.setState(() => {
            return {
                ...processes_obj, 
                ...candidates_obj,
                ...recruiters_obj,
                ...candidates_for_recruiter_obj
            }
        })

    }

    get_processes = async (user_id) => {
        return get_processes_for_hremployee(user_id)
    }

    get_recruiters_from_process = async (process_id = null) => {
        return get_recruiters_for_process(process_id)
    }

    get_candidates_from_process = async (process_id = null) => {
        return get_candidates_for_process(process_id)
    }


    on_process_changed = async (process, idx) => {
        const new_state = {
            process_id: process.id,
            chosen_process: idx
        };
        const candidates_obj = await this.get_candidates_from_process(process.id);
        const recruiters_obj = await this.get_recruiters_from_process(process.id);
        const recruiter_id = (recruiters_obj.recruiters?.length > 0) ? recruiters_obj.recruiters?.[0].id : null;
        const candidates_for_recruiter_obj = await get_candidates_for_recruiter_from_process(recruiter_id, process.id)
        this.setState(() => {
            return {
                ...new_state,
                ...candidates_obj,
                ...recruiters_obj,
                ...candidates_for_recruiter_obj
            }
        })
    }

    on_recruiter_changed = async (recruiter, idx) => {
        const candidates_obj = await get_candidates_for_recruiter_from_process(recruiter.id, this.state.process_id)
        this.setState(() => {
            return {
                chosen_recruiter: idx,
                recruiter_id: recruiter.id,
                ...candidates_obj
            }
        })
    }

    on_candidate_changed = async (candidate, idx) => {
        const bindings = await RecruiterCandidateBindingAPI.getForCandidateAndProcess(candidate.id, this.state.process_id)
        let recruiter = null;
        if (bindings.recruiter_candidate_bindings.length > 0) {
            recruiter = bindings.recruiter_candidate_bindings[0].recruiter
        }
        this.setState(() => {
            return {
                chosen_candidate: idx,
                candidate_id: candidate.id,
                recruiter_for_candidate: recruiter
            }
        })
    }

    on_candidate_recruiter_changed = (candidate, idx) => {
        this.setState(() => {
            return {
                chosen_candidate_recruiter: idx,
                candidate_recruiter_id: candidate.id
            }
        })
    }

    on_candidate_deleted = async (candidate) => {
        let process = this.get_current_process()
        process.candidates = process.candidates.filter(el => el.id != candidate.id)
        await RecruitmentProcessAPI.http_put(process, async () => {
            let candidates_obj = await this.get_candidates_from_process(process.id)
            candidates_obj = this.get_right_candidate_selection_information(candidates_obj, selection_mode.all)
            let candidates_recruiter_obj = await get_candidates_for_recruiter_from_process(this.state.recruiter_id, process.id)
            candidates_recruiter_obj = this.get_right_candidate_selection_information(candidates_recruiter_obj, selection_mode.of_recruiter)
            this.setState(()=>{
                return {
                    ...candidates_obj,
                    ...candidates_recruiter_obj
                }
            })
        }, () => alert("Wewnętrzny błąd serwera!"))
    }

    get_right_candidate_selection_information = (candidates_obj, mode) => {
        const candidates_key = this.get_candidates_key(mode)
        const chosen_candidate_key = this.get_chosen_candidate_key(mode)
        const candidate_id_key = this.get_candidate_id_key(mode)
        const obj = this.get_chosen_candidate_and_candidates(mode)
        if (candidates_obj[candidates_key].length > 0) {
            candidates_obj[chosen_candidate_key] = (obj.chosen_candidate != obj.candidates.length - 1) ? obj.chosen_candidate : obj.chosen_candidate - 1
            candidates_obj[candidate_id_key] = candidates_obj[candidates_key][candidates_obj[chosen_candidate_key]].id
        }
        return candidates_obj
    }

    get_candidates_key = (mode) => {
        if (mode == selection_mode.all) {
            return 'candidates'
        } else if (mode == selection_mode.of_recruiter) {
            return 'candidates_recruiter'
        } else {
            throw new Error('Invalid mode')
        }
    }

    get_chosen_candidate_key = (mode) => {
        if (mode == selection_mode.all) {
            return 'chosen_candidate';
        } else if (mode == selection_mode.of_recruiter) {
            return 'chosen_candidate_recruiter'
        } else {
            throw new Error('Invalid mode')
        }
    }

    get_candidate_id_key = (mode) => {
        if (mode == selection_mode.all) {
            return 'candidate_id';
        } else if (mode == selection_mode.of_recruiter) {
            return 'candidate_id_recruiter'
        } else {
            throw new Error('Invalid mode')
        }
    }

    get_chosen_candidate_and_candidates = (mode) => {
        let obj = {
            chosen_candidate: null,
            candidates: null
        } 
        if (mode == selection_mode.all) {
            obj.chosen_candidate = this.state.chosen_candidate
            obj.candidates = this.state.candidates
        } else if (mode == selection_mode.of_recruiter) {
            obj.chosen_candidate = this.state.chosen_candidate_recruiter
            obj.candidates = this.state.candidates_recruiter
        } else {
            throw new Error("chosen_candidate is set to invalid value")
        }
        return obj;
    }

    on_candidate_created = async (candidate) => {
        AddressAPI.http_post(candidate, async (response) => {
            const address = await response.json()
            candidate.address = address.id;
            CandidateAPI.http_post(candidate, async (response) => {
                const new_candidate = await response.json()
                let process = this.get_current_process()
                process.candidates.push(new_candidate)
                RecruitmentProcessAPI.http_put(process, async () => {
                    const candidates_obj = await this.get_candidates_from_process(process.id)
                    this.setState(() => {
                        return {
                            ...candidates_obj
                        }
                    })
                })
            })
        });
    }

    assign_to_recruiter = (candidate) => {
        RecruiterCandidateBindingAPI.http_post({
            process: this.state.process_id,
            recruiter: this.state.recruiter_id,
            candidate: candidate.id
        }, async () => {
            const candidates_obj = await get_candidates_for_recruiter_from_process(this.state.recruiter_id, this.state.process_id)
            this.setState(() => {
                return {
                    ...candidates_obj,
                    recruiter_for_candidate: this.state.recruiters.find(el => el.id == this.state.recruiter_id)
                }
            })
        }, () => alert("Wewnętrzny błąd serwera!"))
    }

    remove_candidate_recruiter_binding = (candidate) => {
        RecruiterCandidateBindingAPI.http_delete({
            candidate: candidate.id,
            recruiter: this.state.recruiter_for_candidate.id,
            process: this.state.process_id
        }, async () => {
            const candidates_obj = await get_candidates_for_recruiter_from_process(this.state.recruiter_id, this.state.process_id)
            this.setState(() => {
                return {
                    ...candidates_obj,
                    recruiter_for_candidate: null
                }
            })
        }, () => alert("Wewnętrzny błąd serwera!"))
    }

    on_candidate_add_existing = (candidate) => {
        let process = this.get_current_process()
        process.candidates.push(candidate)
        RecruitmentProcessAPI.http_put(process, async () => {
            const candidates_obj = await this.get_candidates_from_process(process.id)
            this.setState(() => {
                return {
                    ...candidates_obj
                }
            })
        })
    }

    on_candidate_edited = (candidate) => {
        CandidateAPI.http_put(candidate,
            () => this.get_data(),
            () => alert('Wewnętrzny błąd serwera!'))
    }

    get_current_process = () => {
        return find_element(this.state.process_id, this.state.processes.recruitment_processes)
    }

    get_current_candidate = () => {
        return find_element(this.state.candidate_id, this.state.candidates)
    }


    render() {
        return (
            <Container>
                <Row className="mb-3">
                    <Col md>
                        <Container>
                            <Row className="mb-3">
                                <Col md>
                                    <RecruitmentProcessView
                                        mode={HR_EMPLOYEE_KEY}
                                        processes={this.state.processes?.recruitment_processes}
                                        chosen_process={this.state.chosen_process}
                                        on_chosen_process_changed={this.on_process_changed.bind(this)}
                                        post_add_process={this.get_data.bind(this)}
                                    />
                                </Col>
                            </Row>
                            <Row className="mb-3">
                                <Col md>
                                    <RecruitersView
                                        recruiters={this.state.recruiters}
                                        chosen_recruiter={this.state.chosen_recruiter}
                                        on_chosen_recruiter_changed={this.on_recruiter_changed.bind(this)}
                                    />
                                </Col>
                            </Row>
                            <Row className="mb-3">
                                <Col>
                                    <CandidatesView
                                        candidates_all={this.state.candidates}
                                        chosen_candidate_all={this.state.chosen_candidate} 
                                        on_chosen_candidate_all_changed={this.on_candidate_changed.bind(this)}

                                        candidates_recruiter={this.state.candidates_recruiter}
                                        chosen_candidate_recruiter={this.state.chosen_candidate_recruiter}
                                        on_chosen_candidate_recruiter_changed={this.on_candidate_recruiter_changed.bind(this)}

                                        on_candidate_created={this.on_candidate_created.bind(this)}
                                        on_candidate_add_existing={this.on_candidate_add_existing.bind(this)}
                                        highlight={HR_EMPLOYEE_HIGHLIGHT}
                                    />
                                </Col>
                            </Row>
                        </Container>

                    </Col>
                    <Col md>
                        <Row className="mb-3">
                            <Col md>
                                <DetailedCandidateView 
                                    process_id={this.state.process_id}
                                    candidate={this.get_current_candidate()}
                                    recruiter={this.state.recruiter_for_candidate}
                                    on_candidate_edited={this.on_candidate_edited.bind(this)}
                                    on_delete_clicked={this.on_candidate_deleted.bind(this)} 
                                    assign_to_recruiter={this.assign_to_recruiter.bind(this)}
                                    remove_from_recruiter={this.remove_candidate_recruiter_binding.bind(this)}
                                    on_candidate_edited={this.on_candidate_edited.bind(this)}
                                    enable_meetings={false}
                                    highlight={HR_EMPLOYEE_HIGHLIGHT}
                                />
                            </Col>
                        </Row>
                    </Col>
                </Row>.
            </Container>
        )
    }
}