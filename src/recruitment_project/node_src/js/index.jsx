import React from 'react';
import ReactDOM from 'react-dom';
import { 
    HR_EMPLOYEE_KEY, 
    RECRUITER_KEY, 
    PROJECT_MANAGER_KEY, 
    CANDIDATE_KEY 
} from "./constants.js"
import {AddressAPI} from "./API/models_api.js"
import {HREmployeeView} from './hr_employee_view.jsx'
import {RecruiterView} from './recruiter_view.jsx'
import {CandidateView} from './candidate_view.jsx'

class Label extends React.Component {
    constructor(props) {
        super(props)
        this.test()
    }

    test = async () => {
        const ad = await AddressAPI.http_get();
    }

    render() {
        return (
            <p>{this.props.text}</p>
        )
    }
}

let hr_employee = document.getElementById(HR_EMPLOYEE_KEY);
if (hr_employee) {
    ReactDOM.render(<HREmployeeView />, hr_employee);
}
let recruiter = document.getElementById(RECRUITER_KEY);
if (recruiter) {
    ReactDOM.render(<RecruiterView />, recruiter);
}
let project_manager = document.getElementById(PROJECT_MANAGER_KEY);
if (project_manager) {
    ReactDOM.render(<Label text={PROJECT_MANAGER_KEY} />, project_manager);
}
let candidate = document.getElementById(CANDIDATE_KEY);
if (candidate) {
    ReactDOM.render(<CandidateView />, candidate);
}


