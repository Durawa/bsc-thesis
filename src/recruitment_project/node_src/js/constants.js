export const HR_EMPLOYEE_KEY = 'hr_employee';
export const RECRUITER_KEY = 'recruiter';
export const PROJECT_MANAGER_KEY = 'project_manager';
export const CANDIDATE_KEY = 'candidate';

export const HR_EMPLOYEE_HIGHLIGHT = '#d0f2e0';
export const RECRUITER_HIGHLIGHT = '#ced8f0'
export const CANDIDATE_HIGHLIGHT = '#efcef0';

export const BACKEND_ERROR = 'Wewnętrzny błąd serwera!';
export const CRITICAL_ERROR = 'Błąd krytyczny! Skontaktuj się z twórcą aplikacji';