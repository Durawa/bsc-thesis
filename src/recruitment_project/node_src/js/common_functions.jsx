import React from 'react'
import { CandidateAPI, RecruiterAPI, RecruiterCandidateBindingAPI, RecruitmentProcessAPI } from "./API/models_api";


export async function get_candidates_for_recruiter_from_process(recruiter_id, process_id) {
    if (recruiter_id == null) {
        return {
            candidates_recruiter: null,
            chosen_candidate_recruiter: null,
            candidate_recruiter_id: null
        }
    }
    const bindings = await RecruiterCandidateBindingAPI.getFoRecruiterAndProcess(recruiter_id, process_id)
    let chosen_candidate = null;
    let candidate_id = null;
    let candidates = []
    for (let binding of bindings.recruiter_candidate_bindings) {
        candidates.push(binding.candidate)
    }
    if (candidates.length > 0){
        chosen_candidate = 0;
        candidate_id = candidates[0].id
    }
    return {
        candidates_recruiter: candidates,
        chosen_candidate_recruiter: chosen_candidate,
        candidate_recruiter_id: candidate_id
    }
}

export async function get_processes_for_hremployee(user_id) {
    const processes = await RecruitmentProcessAPI.getForHREmployee(user_id);
    return get_processes_object(processes)
}

export async function get_processes_for_recruiter(user_id) {
    const processes = await RecruitmentProcessAPI.getForRecruiter(user_id);
    return get_processes_object(processes)
}

export async function get_processes_for_candidate(user_id) {
    const processes = await RecruitmentProcessAPI.getForCandidate(user_id);
    return get_processes_object(processes);
}

function get_processes_object(processes) {
    let chosen_process = null;
    let process_id = null;
    if (processes.recruitment_processes.length > 0) {
        chosen_process = 0;
        process_id = processes.recruitment_processes[0].id;
    }
    return {
        processes: processes,
        chosen_process: chosen_process,
        process_id: process_id
    }
}

export async function get_recruiter_for_candidate_from_process(candidate, process) {
    if (process == null || candidate == null) {
        return {
            candidate_recruiter_id: null,
            recruiter: null
        }
    }
    const binding = await RecruiterCandidateBindingAPI.getForCandidateAndProcess(candidate, process)
    let recruiter_id = null;
    let recruiter = null;
    if (binding.recruiter_candidate_bindings.length > 0) {
        recruiter_id = binding.recruiter_candidate_bindings[0].recruiter.id
        recruiter = binding.recruiter_candidate_bindings[0].recruiter
    }
    return {
        candidate_recruiter_id: recruiter_id,
        recruiter: recruiter
    }
}

export async function get_recruiters_for_process(process_id = null) {
    if (process_id == null) {
        return {
            recruiters: null,
            chosen_recruiter: null,
            recruiter_id: null
        }
    }
    const recruiters = await RecruiterAPI.get_for_process(process_id);
    let chosen_recruiter = null;
    let recruiter_id = null;
    if (recruiters.recruiters.length > 0) {
        chosen_recruiter = 0;
        recruiter_id = recruiters.recruiters[0].id;
    }
    return {
        recruiters: recruiters.recruiters,
        chosen_recruiter: chosen_recruiter,
        recruiter_id: recruiter_id
    }
}

export async function get_candidates_for_process(process_id = null) {
    if (process_id == null) {
        return {
            candidates: null,
            chosen_candidate: null,
            candidate_id: null
        }
    }
    const candidates = await CandidateAPI.get_for_process(process_id);
    let chosen_candidate = null;
    let candidate_id = null;
    let recruiter_for_candidate = null;
    if (candidates.candidates.length > 0) {
        chosen_candidate = 0;
        candidate_id = candidates.candidates[0].id
        const bindings = await RecruiterCandidateBindingAPI.getForCandidateAndProcess(candidate_id, process_id);
        if (bindings.recruiter_candidate_bindings.length > 0) {
            recruiter_for_candidate = bindings.recruiter_candidate_bindings[0].recruiter
        }
    }
    return {
        candidates: candidates.candidates,
        chosen_candidate: chosen_candidate,
        candidate_id: candidate_id,
        recruiter_for_candidate: recruiter_for_candidate
    }
}

export function find_element(id, iterable) {
    return iterable?.find(el => el.id == id);
}

export function convert_time(time) {
    const minutes_collectively = time / 60;
    const hours = Math.floor(minutes_collectively / 60)
    const minutes = minutes_collectively % 60
    const hours_str = add_zero_padding(hours.toString())
    const minutes_str = add_zero_padding(minutes.toString())
    return `${hours_str}:${minutes_str}`
}

export function add_zero_padding(number) {
   return (number.length == 1) ? `0${number}` : number
}

export function to_multiline_paragraph(text){
    const lines = text.split('\n')
    return (
        <p style={{whiteSpace: 'pre'}}>
            {
                lines.map(line => {
                    return (
                        <>{line}<br /></>
                    )
                })
            }
        </p>
    )
}