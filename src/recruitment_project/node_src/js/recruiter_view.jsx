import React, {Component} from 'react'
import {Container, Row, Col, Tabs, Tab } from 'react-bootstrap'
import { RecruiterAPI, UserAPI } from './API/models_api'
import {
    get_candidates_for_recruiter_from_process,
    get_processes_for_recruiter,
    find_element
} from './common_functions.jsx'
import {RECRUITER_HIGHLIGHT, RECRUITER_KEY} from './constants.js'
import { RecruitmentProcessView } from './components/recruitment_process_view.jsx'
import { CandidatesView } from './components/candidates_view.jsx'
import { DetailedCandidateView } from './components/detailed_candidate_view.jsx';
import { GenerateOrDisplayTestView } from './components/test_view.jsx'
import {QuestionsView} from './components/questions_view.jsx'

export class RecruiterView extends Component {
    constructor(props){
        super(props)

        this.state = {
            processes: null,
            chosen_process: null,
            process_id: null,
            candidates_recruiter: null,
            chosen_candidate_recruiter: null,
            candidate_recruiter_id: null,
            recruiter: null
        }
        this.get_data()
    }
    
    get_data = async () => {
        const user = await UserAPI.http_get()
        const recruiter = await RecruiterAPI.http_get(user.id)
        const processes_obj = await this.get_processes(recruiter.id)
        const candidates_obj = await get_candidates_for_recruiter_from_process(user.id, processes_obj.process_id)
        this.setState(() => {
            return {
                ...processes_obj,
                ...candidates_obj,
                recruiter: recruiter
            }
        })
    }

    get_processes = async (user_id) => {
        return get_processes_for_recruiter(user_id)
    }

    on_process_changed = async (process, idx) => {
        const new_state = {
            process_id: process.id,
            chosen_process: idx
        }
        const user = await UserAPI.http_get()
        const recruiter = await RecruiterAPI.http_get(user.id)
        const candidates_obj = await get_candidates_for_recruiter_from_process(recruiter.id, process.id)
        this.setState(() => {
            return {
                ...new_state,
                ...candidates_obj
            }
        })
    }

    on_candidate_changed = (candidate, idx) => {
        this.setState(() => {
            return {
                chosen_candidate_recruiter: idx,
                candidate_recruiter_id: candidate.id
            }
        })
    }

    get_current_process = () => {
        return find_element(this.state.process_id, this.state.processes.recruitment_processes)
    }

    get_current_candidate = () => {
        return find_element(this.state.candidate_recruiter_id, this.state.candidates_recruiter)
    }

    render() {
        return (
            <Tabs className="mb-3">
                <Tab eventKey="main" title="Panel Główny">
                    <Container>
                        <Row className="mb-3">
                            <Col md>
                                <Container>
                                    <Row className="mb-3">
                                        <Col md>
                                            <RecruitmentProcessView
                                                mode={RECRUITER_KEY}
                                                processes={this.state.processes?.recruitment_processes}
                                                chosen_process={this.state.chosen_process}
                                                on_chosen_process_changed={this.on_process_changed.bind(this)}
                                            />
                                        </Col>
                                    </Row>
                                    <Row className="mb-3">
                                        <Col>
                                            <CandidatesView
                                                candidates_all={this.state.candidates_recruiter}
                                                chosen_candidate_all={this.state.chosen_candidate_recruiter} 
                                                on_chosen_candidate_all_changed={this.on_candidate_changed.bind(this)}
                                                enable_recruiters_view={false}
                                                highlight={RECRUITER_HIGHLIGHT}
                                                show_tests={true}
                                                process={find_element(this.state.process_id, this.state.processes?.recruitment_processes)}
                                            />
                                        </Col>
                                    </Row>
                                    <Row className="mb-3">
                                        <Col md>
                                            <DetailedCandidateView 
                                                process={this.state.process_id}
                                                candidate={this.get_current_candidate()}
                                                recruiter={this.state.recruiter}
                                                enable_deletion={false}
                                                enable_assigning_to_and_removing_from_recruiter={false}
                                                enable_edition={false}
                                                highlight={RECRUITER_HIGHLIGHT}
                                            />
                                        </Col>
                                    </Row>
                                </Container>

                            </Col>
                            <Col md>
                                <Row className="mb-3">
                                    <Col md>
                                        <GenerateOrDisplayTestView 
                                            process={this.state.process_id}
                                            recruiter={this.state.recruiter}
                                        />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                </Tab>
                <Tab eventKey="Questions" title="Pytania testowe">
                    <QuestionsView />
                </Tab>
            </Tabs>
        )
    }
}
